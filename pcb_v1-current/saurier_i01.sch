<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Temp" color="12" fill="1" visible="yes" active="yes"/>
<layer number="101" name="fp1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="fp2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="fp4" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="fp5" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="fp6" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="fp7" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LED-Hole" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="Seriennummer" color="15" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="121" name="tTestdril" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="bTestdril" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="tRemSolderm" color="15" fill="10" visible="no" active="yes"/>
<layer number="126" name="bRemSolderm" color="5" fill="10" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="cabframe">
<packages>
</packages>
<symbols>
<symbol name="A3Q-LOC">
<wire x1="342.9" y1="4.175" x2="374.015" y2="4.175" width="0.1016" layer="94"/>
<wire x1="374.015" y1="4.175" x2="384.175" y2="4.175" width="0.1016" layer="94"/>
<wire x1="384.175" y1="4.175" x2="384.175" y2="9.255" width="0.1016" layer="94"/>
<wire x1="384.175" y1="9.255" x2="384.175" y2="14.335" width="0.1016" layer="94"/>
<wire x1="384.175" y1="14.335" x2="384.175" y2="19.415" width="0.1016" layer="94"/>
<wire x1="384.175" y1="19.415" x2="384.175" y2="24.495" width="0.1016" layer="94"/>
<wire x1="288.925" y1="4.175" x2="288.925" y2="24.495" width="0.6096" layer="94"/>
<wire x1="288.925" y1="24.495" x2="342.9" y2="24.495" width="0.6096" layer="94"/>
<wire x1="342.9" y1="24.495" x2="384.175" y2="24.495" width="0.6096" layer="94"/>
<wire x1="374.015" y1="4.175" x2="374.015" y2="9.255" width="0.1016" layer="94"/>
<wire x1="374.015" y1="9.255" x2="384.175" y2="9.255" width="0.1016" layer="94"/>
<wire x1="374.015" y1="9.255" x2="342.9" y2="9.255" width="0.1016" layer="94"/>
<wire x1="342.9" y1="9.255" x2="342.9" y2="4.175" width="0.6096" layer="94"/>
<wire x1="342.9" y1="9.255" x2="342.9" y2="14.335" width="0.6096" layer="94"/>
<wire x1="342.9" y1="14.335" x2="384.175" y2="14.335" width="0.1016" layer="94"/>
<wire x1="342.9" y1="14.335" x2="342.9" y2="19.415" width="0.6096" layer="94"/>
<wire x1="342.9" y1="19.415" x2="384.175" y2="19.415" width="0.1016" layer="94"/>
<wire x1="342.9" y1="19.415" x2="342.9" y2="24.495" width="0.6096" layer="94"/>
<text x="344.805" y="15.605" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.805" y="10.525" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="358.14" y="5.445" size="2.54" layer="94">&gt;SHEET</text>
<text x="344.551" y="5.318" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="388.62" y2="261.62" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3Q-LOC">
<gates>
<gate name="G$1" symbol="A3Q-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="myatmel">
<packages>
<package name="TQFP32">
<smd name="28" x="0.4" y="4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="27" x="1.2" y="4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="26" x="2" y="4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="25" x="2.8" y="4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="29" x="-0.4" y="4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="30" x="-1.2" y="4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="31" x="-2" y="4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="32" x="-2.8" y="4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="12" x="-0.4" y="-4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="11" x="-1.2" y="-4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="10" x="-2" y="-4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="9" x="-2.8" y="-4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="13" x="0.4" y="-4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="14" x="1.2" y="-4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="15" x="2" y="-4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="16" x="2.8" y="-4.2" dx="0.4" dy="1.5" layer="1"/>
<smd name="21" x="4.2" y="0.4" dx="1.5" dy="0.4" layer="1"/>
<smd name="22" x="4.2" y="1.2" dx="1.5" dy="0.4" layer="1"/>
<smd name="23" x="4.2" y="2" dx="1.5" dy="0.4" layer="1"/>
<smd name="24" x="4.2" y="2.8" dx="1.5" dy="0.4" layer="1"/>
<smd name="20" x="4.2" y="-0.4" dx="1.5" dy="0.4" layer="1"/>
<smd name="19" x="4.2" y="-1.2" dx="1.5" dy="0.4" layer="1"/>
<smd name="18" x="4.2" y="-2" dx="1.5" dy="0.4" layer="1"/>
<smd name="17" x="4.2" y="-2.8" dx="1.5" dy="0.4" layer="1"/>
<smd name="5" x="-4.2" y="-0.4" dx="1.5" dy="0.4" layer="1"/>
<smd name="6" x="-4.2" y="-1.2" dx="1.5" dy="0.4" layer="1"/>
<smd name="7" x="-4.2" y="-2" dx="1.5" dy="0.4" layer="1"/>
<smd name="8" x="-4.2" y="-2.8" dx="1.5" dy="0.4" layer="1"/>
<smd name="4" x="-4.2" y="0.4" dx="1.5" dy="0.4" layer="1"/>
<smd name="3" x="-4.2" y="1.2" dx="1.5" dy="0.4" layer="1"/>
<smd name="2" x="-4.2" y="2" dx="1.5" dy="0.4" layer="1"/>
<smd name="1" x="-4.2" y="2.8" dx="1.5" dy="0.4" layer="1"/>
<wire x1="-3.5" y1="-3.5" x2="3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="3.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2.63" x2="-3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2.63" x2="-2.68" y2="3.45" width="0.127" layer="21"/>
<wire x1="-2.68" y1="3.45" x2="-2.64" y2="3.49" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="-2.63" y2="3.5" width="0.127" layer="21"/>
<wire x1="-2.63" y1="3.5" x2="-2.64" y2="3.49" width="0.127" layer="21"/>
<text x="-3.1" y="5.09" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.08" y="-6.38" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="ML6">
<wire x1="-6.35" y1="3.175" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="3.175" x2="-6.35" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.445" x2="-6.35" y2="4.445" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-4.445" x2="7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="4.445" x2="-7.62" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-6.35" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.445" x2="6.35" y2="4.699" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.699" x2="5.08" y2="4.699" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.445" x2="5.08" y2="4.699" width="0.1524" layer="21"/>
<wire x1="6.35" y1="4.445" x2="7.62" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.635" y1="4.445" x2="5.08" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.699" x2="-6.35" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="4.699" x2="-6.35" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.699" x2="-5.08" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-0.635" y2="4.445" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-4.445" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-3.175" x2="2.032" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="6.604" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="6.604" y1="-3.429" x2="6.604" y2="3.429" width="0.0508" layer="21"/>
<wire x1="6.604" y1="3.429" x2="-6.604" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="3.429" x2="-6.604" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-4.445" x2="-2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="-2.54" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-4.318" x2="-3.81" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-4.445" x2="-3.81" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-4.445" x2="-7.62" y2="-4.445" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="0.9144" diameter="1.778" shape="square"/>
<pad name="2" x="-2.54" y="1.27" drill="0.9144" diameter="1.778"/>
<pad name="3" x="0" y="-1.27" drill="0.9144" diameter="1.778"/>
<pad name="4" x="0" y="1.27" drill="0.9144" diameter="1.778"/>
<pad name="5" x="2.54" y="-1.27" drill="0.9144" diameter="1.778"/>
<pad name="6" x="2.54" y="1.27" drill="0.9144" diameter="1.778"/>
<text x="-7.62" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.08" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-5.08" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<text x="-0.381" y="-4.064" size="1.27" layer="21" ratio="10">6</text>
</package>
</packages>
<symbols>
<symbol name="MEGA328P">
<wire x1="0" y1="0" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="0" x2="27.94" y2="76.2" width="0.254" layer="94"/>
<wire x1="27.94" y1="76.2" x2="0" y2="76.2" width="0.254" layer="94"/>
<wire x1="0" y1="76.2" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="ADC6" x="33.02" y="71.12" length="middle" direction="in" rot="R180"/>
<pin name="ADC7" x="33.02" y="68.58" length="middle" direction="in" rot="R180"/>
<pin name="PB0-PCINT0-CLKO-ICP1" x="33.02" y="63.5" length="middle" direction="in" rot="R180"/>
<pin name="PB1-PCINT1-OC1A" x="33.02" y="60.96" length="middle" direction="in" rot="R180"/>
<pin name="PB2-PCINT2-/SS-OC1B" x="33.02" y="58.42" length="middle" direction="in" rot="R180"/>
<pin name="PB3-PCINT3-OC2A-MOSI" x="33.02" y="55.88" length="middle" direction="in" rot="R180"/>
<pin name="PB4-PCINT4-MISO" x="33.02" y="53.34" length="middle" direction="in" rot="R180"/>
<pin name="PB5-PCINT5-SCK" x="33.02" y="50.8" length="middle" direction="in" rot="R180"/>
<pin name="PB6-XTAL1-TOSC1" x="33.02" y="48.26" length="middle" direction="in" rot="R180"/>
<pin name="PB7-XTAL2-TOSC2" x="33.02" y="45.72" length="middle" direction="in" rot="R180"/>
<pin name="PC0-PCINT8-ADC0" x="33.02" y="40.64" length="middle" direction="in" rot="R180"/>
<pin name="PC1-PCINT9-ADC1" x="33.02" y="38.1" length="middle" direction="in" rot="R180"/>
<pin name="PC2-PCINT10-ADC2" x="33.02" y="35.56" length="middle" direction="in" rot="R180"/>
<pin name="PC3-PCINT11-ADC3" x="33.02" y="33.02" length="middle" direction="in" rot="R180"/>
<pin name="PC4-PCINT12-SDA-ADC4" x="33.02" y="30.48" length="middle" direction="in" rot="R180"/>
<pin name="PC5-PCINT13-SCL-ADC5" x="33.02" y="27.94" length="middle" direction="in" rot="R180"/>
<pin name="PC6-PCINT14-/RESET" x="33.02" y="25.4" length="middle" direction="in" rot="R180"/>
<pin name="PD7-PCINT23-AIN1" x="33.02" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="PD6-PCINT22-OC0A-AIN0" x="33.02" y="5.08" length="middle" direction="in" rot="R180"/>
<pin name="PD5-PCINT21-OC0B-T1" x="33.02" y="7.62" length="middle" direction="in" rot="R180"/>
<pin name="PD4-PCINT20-XCK-T0" x="33.02" y="10.16" length="middle" direction="in" rot="R180"/>
<pin name="PD3-PCINT19-OC2B-INT1" x="33.02" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="PD2-PCINT18-INT0" x="33.02" y="15.24" length="middle" direction="in" rot="R180"/>
<pin name="PD1-PCINT17-TXD" x="33.02" y="17.78" length="middle" direction="in" rot="R180"/>
<pin name="PD0-PCINT16-RXD" x="33.02" y="20.32" length="middle" direction="in" rot="R180"/>
<pin name="AREF" x="33.02" y="73.66" length="middle" direction="in" rot="R180"/>
<text x="0" y="76.708" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="78.74" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SUPPLY">
<pin name="GND" x="0" y="-12.7" length="middle" direction="sup" rot="R90"/>
<pin name="VCC" x="0" y="12.7" length="middle" direction="sup" rot="R270"/>
</symbol>
<symbol name="A-SUPPLY">
<pin name="GND" x="0" y="-12.7" length="middle" direction="sup" rot="R90"/>
<pin name="AVCC" x="0" y="12.7" length="middle" direction="sup" rot="R270"/>
</symbol>
<symbol name="ISP-PROG">
<wire x1="-5.08" y1="10.16" x2="5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<text x="-5.08" y="11.684" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="MISO" x="10.16" y="5.08" length="middle" rot="R180"/>
<pin name="MOSI" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="SCK" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="/RST" x="10.16" y="-2.54" length="middle" rot="R180"/>
<pin name="VCC" x="10.16" y="7.62" length="middle" direction="sup" rot="R180"/>
<pin name="GND" x="10.16" y="-5.08" length="middle" direction="sup" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MEGA328PA" prefix="IC" uservalue="yes">
<gates>
<gate name="G$1" symbol="MEGA328P" x="0" y="0"/>
<gate name="G$3" symbol="SUPPLY" x="-48.26" y="-22.86" addlevel="request"/>
<gate name="G$4" symbol="SUPPLY" x="-43.18" y="-22.86" addlevel="request"/>
<gate name="G$2" symbol="A-SUPPLY" x="-38.1" y="-22.86" addlevel="request"/>
</gates>
<devices>
<device name="" package="TQFP32">
<connects>
<connect gate="G$1" pin="ADC6" pad="19"/>
<connect gate="G$1" pin="ADC7" pad="22"/>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="PB0-PCINT0-CLKO-ICP1" pad="12"/>
<connect gate="G$1" pin="PB1-PCINT1-OC1A" pad="13"/>
<connect gate="G$1" pin="PB2-PCINT2-/SS-OC1B" pad="14"/>
<connect gate="G$1" pin="PB3-PCINT3-OC2A-MOSI" pad="15"/>
<connect gate="G$1" pin="PB4-PCINT4-MISO" pad="16"/>
<connect gate="G$1" pin="PB5-PCINT5-SCK" pad="17"/>
<connect gate="G$1" pin="PB6-XTAL1-TOSC1" pad="7"/>
<connect gate="G$1" pin="PB7-XTAL2-TOSC2" pad="8"/>
<connect gate="G$1" pin="PC0-PCINT8-ADC0" pad="23"/>
<connect gate="G$1" pin="PC1-PCINT9-ADC1" pad="24"/>
<connect gate="G$1" pin="PC2-PCINT10-ADC2" pad="25"/>
<connect gate="G$1" pin="PC3-PCINT11-ADC3" pad="26"/>
<connect gate="G$1" pin="PC4-PCINT12-SDA-ADC4" pad="27"/>
<connect gate="G$1" pin="PC5-PCINT13-SCL-ADC5" pad="28"/>
<connect gate="G$1" pin="PC6-PCINT14-/RESET" pad="29"/>
<connect gate="G$1" pin="PD0-PCINT16-RXD" pad="30"/>
<connect gate="G$1" pin="PD1-PCINT17-TXD" pad="31"/>
<connect gate="G$1" pin="PD2-PCINT18-INT0" pad="32"/>
<connect gate="G$1" pin="PD3-PCINT19-OC2B-INT1" pad="1"/>
<connect gate="G$1" pin="PD4-PCINT20-XCK-T0" pad="2"/>
<connect gate="G$1" pin="PD5-PCINT21-OC0B-T1" pad="9"/>
<connect gate="G$1" pin="PD6-PCINT22-OC0A-AIN0" pad="10"/>
<connect gate="G$1" pin="PD7-PCINT23-AIN1" pad="11"/>
<connect gate="G$2" pin="AVCC" pad="18"/>
<connect gate="G$2" pin="GND" pad="3"/>
<connect gate="G$3" pin="GND" pad="21"/>
<connect gate="G$3" pin="VCC" pad="4"/>
<connect gate="G$4" pin="GND" pad="5"/>
<connect gate="G$4" pin="VCC" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ISP-PROG" prefix="CON" uservalue="yes">
<gates>
<gate name="G$1" symbol="ISP-PROG" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ML6">
<connects>
<connect gate="G$1" pin="/RST" pad="5"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ftdichip">
<description>&lt;b&gt;FTDI (TM) CHIP&lt;/b&gt; Future Technology Devices International Ltd.&lt;p&gt;
http://www.ftdichip.com</description>
<packages>
<package name="SSOP28">
<description>&lt;b&gt;Shrink Small Outline Package&lt;/b&gt; SSOP-28&lt;p&gt;
http://www.ftdichip.com/Documents/DataSheets/DS_FT232R_v104.pdf</description>
<wire x1="-5.1" y1="-2.6" x2="5.1" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-2.6" x2="5.1" y2="2.6" width="0.2032" layer="21"/>
<wire x1="5.1" y1="2.6" x2="-5.1" y2="2.6" width="0.2032" layer="21"/>
<wire x1="-5.1" y1="2.6" x2="-5.1" y2="-2.6" width="0.2032" layer="21"/>
<circle x="-4.2" y="-1.625" radius="0.4422" width="0" layer="21"/>
<smd name="1" x="-4.225" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="2" x="-3.575" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="3" x="-2.925" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="4" x="-2.275" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="5" x="-1.625" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="6" x="-0.975" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="7" x="-0.325" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="8" x="0.325" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="9" x="0.975" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="10" x="1.625" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="11" x="2.275" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="12" x="2.925" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="13" x="3.575" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="14" x="4.225" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="15" x="4.225" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="16" x="3.575" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="17" x="2.925" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="18" x="2.275" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="19" x="1.625" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="20" x="0.975" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="21" x="0.325" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="22" x="-0.325" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="23" x="-0.975" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="24" x="-1.625" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="25" x="-2.275" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="26" x="-2.925" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="27" x="-3.575" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="28" x="-4.225" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<text x="-5.476" y="-2.6299" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-3.8999" y="-0.68" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.4028" y1="-3.937" x2="-4.0472" y2="-2.6416" layer="51"/>
<rectangle x1="-3.7529" y1="-3.937" x2="-3.3973" y2="-2.6416" layer="51"/>
<rectangle x1="-3.1029" y1="-3.937" x2="-2.7473" y2="-2.6416" layer="51"/>
<rectangle x1="-2.4529" y1="-3.937" x2="-2.0973" y2="-2.6416" layer="51"/>
<rectangle x1="-1.8029" y1="-3.937" x2="-1.4473" y2="-2.6416" layer="51"/>
<rectangle x1="-1.1529" y1="-3.937" x2="-0.7973" y2="-2.6416" layer="51"/>
<rectangle x1="-0.5029" y1="-3.937" x2="-0.1473" y2="-2.6416" layer="51"/>
<rectangle x1="0.1473" y1="-3.937" x2="0.5029" y2="-2.6416" layer="51"/>
<rectangle x1="0.7973" y1="-3.937" x2="1.1529" y2="-2.6416" layer="51"/>
<rectangle x1="1.4473" y1="-3.937" x2="1.8029" y2="-2.6416" layer="51"/>
<rectangle x1="2.0973" y1="-3.937" x2="2.4529" y2="-2.6416" layer="51"/>
<rectangle x1="2.7473" y1="-3.937" x2="3.1029" y2="-2.6416" layer="51"/>
<rectangle x1="3.3973" y1="-3.937" x2="3.7529" y2="-2.6416" layer="51"/>
<rectangle x1="4.0472" y1="-3.937" x2="4.4028" y2="-2.6416" layer="51"/>
<rectangle x1="4.0472" y1="2.6416" x2="4.4028" y2="3.937" layer="51"/>
<rectangle x1="3.3973" y1="2.6416" x2="3.7529" y2="3.937" layer="51"/>
<rectangle x1="2.7473" y1="2.6416" x2="3.1029" y2="3.937" layer="51"/>
<rectangle x1="2.0973" y1="2.6416" x2="2.4529" y2="3.937" layer="51"/>
<rectangle x1="1.4473" y1="2.6416" x2="1.8029" y2="3.937" layer="51"/>
<rectangle x1="0.7973" y1="2.6416" x2="1.1529" y2="3.937" layer="51"/>
<rectangle x1="0.1473" y1="2.6416" x2="0.5029" y2="3.937" layer="51"/>
<rectangle x1="-0.5029" y1="2.6416" x2="-0.1473" y2="3.937" layer="51"/>
<rectangle x1="-1.1529" y1="2.6416" x2="-0.7973" y2="3.937" layer="51"/>
<rectangle x1="-1.8029" y1="2.6416" x2="-1.4473" y2="3.937" layer="51"/>
<rectangle x1="-2.4529" y1="2.6416" x2="-2.0973" y2="3.937" layer="51"/>
<rectangle x1="-3.1029" y1="2.6416" x2="-2.7473" y2="3.937" layer="51"/>
<rectangle x1="-3.7529" y1="2.6416" x2="-3.3973" y2="3.937" layer="51"/>
<rectangle x1="-4.4028" y1="2.6416" x2="-4.0472" y2="3.937" layer="51"/>
</package>
<package name="QFN32">
<description>&lt;b&gt;QFN 32&lt;/b&gt;&lt;p&gt;
Source: http://www.ftdichip.com/Documents/DataSheets/DS_FT232R_v104.pdf</description>
<wire x1="-2.45" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="2.45" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="2.45" width="0.1016" layer="51"/>
<wire x1="-2.45" y1="2.05" x2="-2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="2.45" x2="-2.05" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.05" y1="2.45" x2="2.45" y2="2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="2.45" x2="2.45" y2="2.05" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.05" x2="2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="2.45" y1="-2.45" x2="2.05" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.05" y1="-2.45" x2="-2.45" y2="-2.45" width="0.1016" layer="21"/>
<wire x1="-2.45" y1="-2.45" x2="-2.45" y2="-2.05" width="0.1016" layer="21"/>
<circle x="-2.175" y="2.175" radius="0.15" width="0" layer="21"/>
<smd name="EXP" x="0" y="0" dx="3.2" dy="3.2" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="1" x="-2.3" y="1.75" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="2" x="-2.3" y="1.25" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="3" x="-2.3" y="0.75" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="4" x="-2.3" y="0.25" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="5" x="-2.3" y="-0.25" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="6" x="-2.3" y="-0.75" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="7" x="-2.3" y="-1.25" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="8" x="-2.3" y="-1.75" dx="0.6" dy="0.3" layer="1" roundness="30" stop="no" cream="no"/>
<smd name="9" x="-1.75" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="10" x="-1.25" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="11" x="-0.75" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="12" x="-0.25" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="13" x="0.25" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="14" x="0.75" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="15" x="1.25" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="16" x="1.75" y="-2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R90" stop="no" cream="no"/>
<smd name="17" x="2.3" y="-1.75" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="18" x="2.3" y="-1.25" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="19" x="2.3" y="-0.75" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="20" x="2.3" y="-0.25" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="21" x="2.3" y="0.25" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="22" x="2.3" y="0.75" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="23" x="2.3" y="1.25" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="24" x="2.3" y="1.75" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R180" stop="no" cream="no"/>
<smd name="25" x="1.75" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="26" x="1.25" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="27" x="0.75" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="28" x="0.25" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="29" x="-0.25" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="30" x="-0.75" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="31" x="-1.25" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<smd name="32" x="-1.75" y="2.3" dx="0.6" dy="0.3" layer="1" roundness="30" rot="R270" stop="no" cream="no"/>
<text x="-4.05" y="-4.35" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.8" y="3.25" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.3" y1="1.1" x2="0.3" y2="1.4" layer="31"/>
<rectangle x1="-0.3" y1="0.6" x2="0.3" y2="0.9" layer="31"/>
<rectangle x1="-0.3" y1="0.1" x2="0.3" y2="0.4" layer="31"/>
<rectangle x1="-0.3" y1="-0.4" x2="0.3" y2="-0.1" layer="31"/>
<rectangle x1="-0.3" y1="-0.9" x2="0.3" y2="-0.6" layer="31"/>
<rectangle x1="-0.3" y1="-1.4" x2="0.3" y2="-1.1" layer="31"/>
<rectangle x1="-1.3" y1="1.1" x2="-0.7" y2="1.4" layer="31"/>
<rectangle x1="-1.3" y1="0.6" x2="-0.7" y2="0.9" layer="31"/>
<rectangle x1="-1.3" y1="0.1" x2="-0.7" y2="0.4" layer="31"/>
<rectangle x1="-1.3" y1="-0.4" x2="-0.7" y2="-0.1" layer="31"/>
<rectangle x1="-1.3" y1="-0.9" x2="-0.7" y2="-0.6" layer="31"/>
<rectangle x1="-1.3" y1="-1.4" x2="-0.7" y2="-1.1" layer="31"/>
<rectangle x1="0.7" y1="1.1" x2="1.3" y2="1.4" layer="31"/>
<rectangle x1="0.7" y1="0.6" x2="1.3" y2="0.9" layer="31"/>
<rectangle x1="0.7" y1="0.1" x2="1.3" y2="0.4" layer="31"/>
<rectangle x1="0.7" y1="-0.4" x2="1.3" y2="-0.1" layer="31"/>
<rectangle x1="0.7" y1="-0.9" x2="1.3" y2="-0.6" layer="31"/>
<rectangle x1="0.7" y1="-1.4" x2="1.3" y2="-1.1" layer="31"/>
<polygon width="0.5" layer="29">
<vertex x="-1.325" y="1.325"/>
<vertex x="1.325" y="1.325"/>
<vertex x="1.325" y="-1.325"/>
<vertex x="-1.325" y="-1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="1.85"/>
<vertex x="-2.1" y="1.85"/>
<vertex x="-2.05" y="1.8"/>
<vertex x="-2.05" y="1.65"/>
<vertex x="-2.55" y="1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="1.825"/>
<vertex x="-2.125" y="1.825"/>
<vertex x="-2.075" y="1.775"/>
<vertex x="-2.075" y="1.675"/>
<vertex x="-2.525" y="1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="1.35"/>
<vertex x="-2.05" y="1.35"/>
<vertex x="-2.05" y="1.15"/>
<vertex x="-2.55" y="1.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="1.325"/>
<vertex x="-2.075" y="1.325"/>
<vertex x="-2.075" y="1.175"/>
<vertex x="-2.525" y="1.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="0.85"/>
<vertex x="-2.05" y="0.85"/>
<vertex x="-2.05" y="0.65"/>
<vertex x="-2.55" y="0.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="0.825"/>
<vertex x="-2.075" y="0.825"/>
<vertex x="-2.075" y="0.675"/>
<vertex x="-2.525" y="0.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="0.35"/>
<vertex x="-2.05" y="0.35"/>
<vertex x="-2.05" y="0.15"/>
<vertex x="-2.55" y="0.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="0.325"/>
<vertex x="-2.075" y="0.325"/>
<vertex x="-2.075" y="0.175"/>
<vertex x="-2.525" y="0.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-0.15"/>
<vertex x="-2.05" y="-0.15"/>
<vertex x="-2.05" y="-0.35"/>
<vertex x="-2.55" y="-0.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-0.175"/>
<vertex x="-2.075" y="-0.175"/>
<vertex x="-2.075" y="-0.325"/>
<vertex x="-2.525" y="-0.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-0.65"/>
<vertex x="-2.05" y="-0.65"/>
<vertex x="-2.05" y="-0.85"/>
<vertex x="-2.55" y="-0.85"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-0.675"/>
<vertex x="-2.075" y="-0.675"/>
<vertex x="-2.075" y="-0.825"/>
<vertex x="-2.525" y="-0.825"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-1.15"/>
<vertex x="-2.05" y="-1.15"/>
<vertex x="-2.05" y="-1.35"/>
<vertex x="-2.55" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-1.175"/>
<vertex x="-2.075" y="-1.175"/>
<vertex x="-2.075" y="-1.325"/>
<vertex x="-2.525" y="-1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-2.55" y="-1.85"/>
<vertex x="-2.1" y="-1.85"/>
<vertex x="-2.05" y="-1.8"/>
<vertex x="-2.05" y="-1.65"/>
<vertex x="-2.55" y="-1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-2.525" y="-1.825"/>
<vertex x="-2.125" y="-1.825"/>
<vertex x="-2.075" y="-1.775"/>
<vertex x="-2.075" y="-1.675"/>
<vertex x="-2.525" y="-1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.85" y="-2.55"/>
<vertex x="-1.85" y="-2.1"/>
<vertex x="-1.8" y="-2.05"/>
<vertex x="-1.65" y="-2.05"/>
<vertex x="-1.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.825" y="-2.525"/>
<vertex x="-1.825" y="-2.125"/>
<vertex x="-1.775" y="-2.075"/>
<vertex x="-1.675" y="-2.075"/>
<vertex x="-1.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.35" y="-2.55"/>
<vertex x="-1.35" y="-2.05"/>
<vertex x="-1.15" y="-2.05"/>
<vertex x="-1.15" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.325" y="-2.525"/>
<vertex x="-1.325" y="-2.075"/>
<vertex x="-1.175" y="-2.075"/>
<vertex x="-1.175" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.85" y="-2.55"/>
<vertex x="-0.85" y="-2.05"/>
<vertex x="-0.65" y="-2.05"/>
<vertex x="-0.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.825" y="-2.525"/>
<vertex x="-0.825" y="-2.075"/>
<vertex x="-0.675" y="-2.075"/>
<vertex x="-0.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.35" y="-2.55"/>
<vertex x="-0.35" y="-2.05"/>
<vertex x="-0.15" y="-2.05"/>
<vertex x="-0.15" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.325" y="-2.525"/>
<vertex x="-0.325" y="-2.075"/>
<vertex x="-0.175" y="-2.075"/>
<vertex x="-0.175" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.15" y="-2.55"/>
<vertex x="0.15" y="-2.05"/>
<vertex x="0.35" y="-2.05"/>
<vertex x="0.35" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.175" y="-2.525"/>
<vertex x="0.175" y="-2.075"/>
<vertex x="0.325" y="-2.075"/>
<vertex x="0.325" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.65" y="-2.55"/>
<vertex x="0.65" y="-2.05"/>
<vertex x="0.85" y="-2.05"/>
<vertex x="0.85" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.675" y="-2.525"/>
<vertex x="0.675" y="-2.075"/>
<vertex x="0.825" y="-2.075"/>
<vertex x="0.825" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.15" y="-2.55"/>
<vertex x="1.15" y="-2.05"/>
<vertex x="1.35" y="-2.05"/>
<vertex x="1.35" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.175" y="-2.525"/>
<vertex x="1.175" y="-2.075"/>
<vertex x="1.325" y="-2.075"/>
<vertex x="1.325" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.85" y="-2.55"/>
<vertex x="1.85" y="-2.1"/>
<vertex x="1.8" y="-2.05"/>
<vertex x="1.65" y="-2.05"/>
<vertex x="1.65" y="-2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.825" y="-2.525"/>
<vertex x="1.825" y="-2.125"/>
<vertex x="1.775" y="-2.075"/>
<vertex x="1.675" y="-2.075"/>
<vertex x="1.675" y="-2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-1.85"/>
<vertex x="2.1" y="-1.85"/>
<vertex x="2.05" y="-1.8"/>
<vertex x="2.05" y="-1.65"/>
<vertex x="2.55" y="-1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-1.825"/>
<vertex x="2.125" y="-1.825"/>
<vertex x="2.075" y="-1.775"/>
<vertex x="2.075" y="-1.675"/>
<vertex x="2.525" y="-1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-1.35"/>
<vertex x="2.05" y="-1.35"/>
<vertex x="2.05" y="-1.15"/>
<vertex x="2.55" y="-1.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-1.325"/>
<vertex x="2.075" y="-1.325"/>
<vertex x="2.075" y="-1.175"/>
<vertex x="2.525" y="-1.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-0.85"/>
<vertex x="2.05" y="-0.85"/>
<vertex x="2.05" y="-0.65"/>
<vertex x="2.55" y="-0.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-0.825"/>
<vertex x="2.075" y="-0.825"/>
<vertex x="2.075" y="-0.675"/>
<vertex x="2.525" y="-0.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="-0.35"/>
<vertex x="2.05" y="-0.35"/>
<vertex x="2.05" y="-0.15"/>
<vertex x="2.55" y="-0.15"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="-0.325"/>
<vertex x="2.075" y="-0.325"/>
<vertex x="2.075" y="-0.175"/>
<vertex x="2.525" y="-0.175"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="0.15"/>
<vertex x="2.05" y="0.15"/>
<vertex x="2.05" y="0.35"/>
<vertex x="2.55" y="0.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="0.175"/>
<vertex x="2.075" y="0.175"/>
<vertex x="2.075" y="0.325"/>
<vertex x="2.525" y="0.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="0.65"/>
<vertex x="2.05" y="0.65"/>
<vertex x="2.05" y="0.85"/>
<vertex x="2.55" y="0.85"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="0.675"/>
<vertex x="2.075" y="0.675"/>
<vertex x="2.075" y="0.825"/>
<vertex x="2.525" y="0.825"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="1.15"/>
<vertex x="2.05" y="1.15"/>
<vertex x="2.05" y="1.35"/>
<vertex x="2.55" y="1.35"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="1.175"/>
<vertex x="2.075" y="1.175"/>
<vertex x="2.075" y="1.325"/>
<vertex x="2.525" y="1.325"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="2.55" y="1.85"/>
<vertex x="2.1" y="1.85"/>
<vertex x="2.05" y="1.8"/>
<vertex x="2.05" y="1.65"/>
<vertex x="2.55" y="1.65"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="2.525" y="1.825"/>
<vertex x="2.125" y="1.825"/>
<vertex x="2.075" y="1.775"/>
<vertex x="2.075" y="1.675"/>
<vertex x="2.525" y="1.675"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.85" y="2.55"/>
<vertex x="1.85" y="2.1"/>
<vertex x="1.8" y="2.05"/>
<vertex x="1.65" y="2.05"/>
<vertex x="1.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.825" y="2.525"/>
<vertex x="1.825" y="2.125"/>
<vertex x="1.775" y="2.075"/>
<vertex x="1.675" y="2.075"/>
<vertex x="1.675" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="1.35" y="2.55"/>
<vertex x="1.35" y="2.05"/>
<vertex x="1.15" y="2.05"/>
<vertex x="1.15" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="1.325" y="2.525"/>
<vertex x="1.325" y="2.075"/>
<vertex x="1.175" y="2.075"/>
<vertex x="1.175" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.85" y="2.55"/>
<vertex x="0.85" y="2.05"/>
<vertex x="0.65" y="2.05"/>
<vertex x="0.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.825" y="2.525"/>
<vertex x="0.825" y="2.075"/>
<vertex x="0.675" y="2.075"/>
<vertex x="0.675" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="0.35" y="2.55"/>
<vertex x="0.35" y="2.05"/>
<vertex x="0.15" y="2.05"/>
<vertex x="0.15" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="0.325" y="2.525"/>
<vertex x="0.325" y="2.075"/>
<vertex x="0.175" y="2.075"/>
<vertex x="0.175" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.15" y="2.55"/>
<vertex x="-0.15" y="2.05"/>
<vertex x="-0.35" y="2.05"/>
<vertex x="-0.35" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.175" y="2.525"/>
<vertex x="-0.175" y="2.075"/>
<vertex x="-0.325" y="2.075"/>
<vertex x="-0.325" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-0.65" y="2.55"/>
<vertex x="-0.65" y="2.05"/>
<vertex x="-0.85" y="2.05"/>
<vertex x="-0.85" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-0.675" y="2.525"/>
<vertex x="-0.675" y="2.075"/>
<vertex x="-0.825" y="2.075"/>
<vertex x="-0.825" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.15" y="2.55"/>
<vertex x="-1.15" y="2.05"/>
<vertex x="-1.35" y="2.05"/>
<vertex x="-1.35" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.175" y="2.525"/>
<vertex x="-1.175" y="2.075"/>
<vertex x="-1.325" y="2.075"/>
<vertex x="-1.325" y="2.525"/>
</polygon>
<polygon width="0.1016" layer="29">
<vertex x="-1.85" y="2.55"/>
<vertex x="-1.85" y="2.1"/>
<vertex x="-1.8" y="2.05"/>
<vertex x="-1.65" y="2.05"/>
<vertex x="-1.65" y="2.55"/>
</polygon>
<polygon width="0.1016" layer="31">
<vertex x="-1.825" y="2.525"/>
<vertex x="-1.825" y="2.125"/>
<vertex x="-1.775" y="2.075"/>
<vertex x="-1.675" y="2.075"/>
<vertex x="-1.675" y="2.525"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="FT232R">
<wire x1="-10.16" y1="25.4" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="-10.16" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-27.94" x2="-10.16" y2="25.4" width="0.254" layer="94"/>
<text x="-10.16" y="26.67" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="-12.7" y="22.86" length="short" direction="pwr"/>
<pin name="3V3OUT" x="-12.7" y="-5.08" length="short" direction="out"/>
<pin name="USBDP" x="-12.7" y="-10.16" length="short"/>
<pin name="USBDM" x="-12.7" y="-12.7" length="short"/>
<pin name="OSCO" x="-12.7" y="7.62" length="short" direction="out"/>
<pin name="OSCI" x="-12.7" y="10.16" length="short" direction="in"/>
<pin name="GND" x="15.24" y="-20.32" length="short" direction="pwr" rot="R180"/>
<pin name="TXD" x="15.24" y="22.86" length="short" direction="out" rot="R180"/>
<pin name="RXD" x="15.24" y="20.32" length="short" direction="in" rot="R180"/>
<pin name="!RTS" x="15.24" y="17.78" length="short" direction="out" rot="R180"/>
<pin name="!CTS" x="15.24" y="15.24" length="short" direction="in" rot="R180"/>
<pin name="!DTR" x="15.24" y="12.7" length="short" direction="out" rot="R180"/>
<pin name="!DSR" x="15.24" y="10.16" length="short" direction="in" rot="R180"/>
<pin name="!DCD" x="15.24" y="7.62" length="short" direction="in" rot="R180"/>
<pin name="!RI" x="15.24" y="5.08" length="short" direction="in" rot="R180"/>
<pin name="CBUS0" x="15.24" y="0" length="short" rot="R180"/>
<pin name="CBUS1" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="CBUS2" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="CBUS3" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="CBUS4" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="VCCIO" x="-12.7" y="20.32" length="short" direction="pwr"/>
<pin name="!RESET" x="-12.7" y="15.24" length="short" direction="in"/>
<pin name="GND@A" x="-12.7" y="-17.78" length="short" direction="pwr"/>
<pin name="GND@1" x="15.24" y="-22.86" length="short" direction="pwr" rot="R180"/>
<pin name="TEST" x="15.24" y="-15.24" length="short" direction="in" rot="R180"/>
<pin name="GND@2" x="15.24" y="-25.4" length="short" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FT232R" prefix="IC">
<description>Source: http://www.ftdichip.com/Documents/DataSheets/DS_FT232R_v104.pdf</description>
<gates>
<gate name="1" symbol="FT232R" x="0" y="0"/>
</gates>
<devices>
<device name="L" package="SSOP28">
<connects>
<connect gate="1" pin="!CTS" pad="11"/>
<connect gate="1" pin="!DCD" pad="10"/>
<connect gate="1" pin="!DSR" pad="9"/>
<connect gate="1" pin="!DTR" pad="2"/>
<connect gate="1" pin="!RESET" pad="19"/>
<connect gate="1" pin="!RI" pad="6"/>
<connect gate="1" pin="!RTS" pad="3"/>
<connect gate="1" pin="3V3OUT" pad="17"/>
<connect gate="1" pin="CBUS0" pad="23"/>
<connect gate="1" pin="CBUS1" pad="22"/>
<connect gate="1" pin="CBUS2" pad="13"/>
<connect gate="1" pin="CBUS3" pad="14"/>
<connect gate="1" pin="CBUS4" pad="12"/>
<connect gate="1" pin="GND" pad="7"/>
<connect gate="1" pin="GND@1" pad="18"/>
<connect gate="1" pin="GND@2" pad="21"/>
<connect gate="1" pin="GND@A" pad="25"/>
<connect gate="1" pin="OSCI" pad="27"/>
<connect gate="1" pin="OSCO" pad="28"/>
<connect gate="1" pin="RXD" pad="5"/>
<connect gate="1" pin="TEST" pad="26"/>
<connect gate="1" pin="TXD" pad="1"/>
<connect gate="1" pin="USBDM" pad="16"/>
<connect gate="1" pin="USBDP" pad="15"/>
<connect gate="1" pin="VCC" pad="20"/>
<connect gate="1" pin="VCCIO" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="FT232RL" constant="no"/>
<attribute name="OC_FARNELL" value="1146032" constant="no"/>
<attribute name="OC_NEWARK" value="91K9918" constant="no"/>
</technology>
</technologies>
</device>
<device name="Q" package="QFN32">
<connects>
<connect gate="1" pin="!CTS" pad="8"/>
<connect gate="1" pin="!DCD" pad="7"/>
<connect gate="1" pin="!DSR" pad="6"/>
<connect gate="1" pin="!DTR" pad="31"/>
<connect gate="1" pin="!RESET" pad="18"/>
<connect gate="1" pin="!RI" pad="3"/>
<connect gate="1" pin="!RTS" pad="32"/>
<connect gate="1" pin="3V3OUT" pad="16"/>
<connect gate="1" pin="CBUS0" pad="22"/>
<connect gate="1" pin="CBUS1" pad="21"/>
<connect gate="1" pin="CBUS2" pad="10"/>
<connect gate="1" pin="CBUS3" pad="11"/>
<connect gate="1" pin="CBUS4" pad="9"/>
<connect gate="1" pin="GND" pad="4"/>
<connect gate="1" pin="GND@1" pad="17"/>
<connect gate="1" pin="GND@2" pad="20"/>
<connect gate="1" pin="GND@A" pad="24"/>
<connect gate="1" pin="OSCI" pad="27"/>
<connect gate="1" pin="OSCO" pad="28"/>
<connect gate="1" pin="RXD" pad="2"/>
<connect gate="1" pin="TEST" pad="26"/>
<connect gate="1" pin="TXD" pad="30"/>
<connect gate="1" pin="USBDM" pad="15"/>
<connect gate="1" pin="USBDP" pad="14"/>
<connect gate="1" pin="VCC" pad="19"/>
<connect gate="1" pin="VCCIO" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="FT232RQ" constant="no"/>
<attribute name="OC_FARNELL" value="1146033" constant="no"/>
<attribute name="OC_NEWARK" value="91K9919" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="cab2">
<packages>
<package name="USB-MINIB">
<wire x1="-3.7" y1="2.5" x2="-3.7" y2="7.3" width="0.127" layer="21"/>
<wire x1="-3.7" y1="7.3" x2="-3.7" y2="9.35" width="0.127" layer="21"/>
<wire x1="-3.7" y1="9.35" x2="3.7" y2="9.35" width="0.127" layer="21"/>
<wire x1="3.7" y1="9.35" x2="3.7" y2="7.3" width="0.127" layer="21"/>
<wire x1="3.7" y1="7.3" x2="3.7" y2="2.5" width="0.127" layer="21"/>
<wire x1="3.7" y1="2.5" x2="-3.7" y2="2.5" width="0.127" layer="21"/>
<wire x1="-3.7" y1="7.3" x2="3.7" y2="7.3" width="0.127" layer="21"/>
<wire x1="-11.4808" y1="8.6868" x2="-10.1092" y2="7.3152" width="0" layer="48"/>
<wire x1="-10.1092" y1="7.3152" x2="-8.7376" y2="8.6868" width="0" layer="48"/>
<wire x1="-8.7376" y1="8.6868" x2="-7.366" y2="7.3152" width="0" layer="48"/>
<wire x1="-7.366" y1="7.3152" x2="-5.9944" y2="8.6868" width="0" layer="48"/>
<circle x="1.59" y="2.91" radius="0.0223" width="0.6096" layer="21"/>
<smd name="D+" x="0" y="1.1" dx="2.25" dy="0.5" layer="1" rot="R90"/>
<smd name="D-" x="0.8" y="1.1" dx="2.25" dy="0.5" layer="1" rot="R90"/>
<smd name="VCC" x="1.6" y="1.1" dx="2.25" dy="0.5" layer="1" rot="R90"/>
<smd name="ID" x="-0.8" y="1.1" dx="2.25" dy="0.5" layer="1" rot="R90"/>
<smd name="GND" x="-1.6" y="1.1" dx="2.25" dy="0.5" layer="1" rot="R90"/>
<smd name="FGND@4" x="4.93" y="1.15" dx="2.05" dy="3.5" layer="1"/>
<smd name="FGND@2" x="-4.93" y="1.15" dx="2.05" dy="3.5" layer="1"/>
<smd name="FGND@1" x="-4.93" y="5.35" dx="4" dy="2.05" layer="1" rot="R90"/>
<smd name="FGND@3" x="4.93" y="5.35" dx="4" dy="2.05" layer="1" rot="R90"/>
<text x="-11.5316" y="8.89" size="1.27" layer="48">Board </text>
<text x="-10.2616" y="-2.54" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.1016" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.1" y1="6.15" x2="3.1" y2="7.35" layer="41"/>
</package>
<package name="USB-MINIB-DU">
<wire x1="-3.65" y1="-0.03" x2="-3.65" y2="2.02" width="0.127" layer="21"/>
<wire x1="-3.65" y1="2.02" x2="3.65" y2="2.02" width="0.127" layer="21"/>
<wire x1="3.65" y1="2.02" x2="3.65" y2="-0.03" width="0.127" layer="21"/>
<wire x1="-3.65" y1="-0.03" x2="3.65" y2="-0.03" width="0.127" layer="21"/>
<wire x1="-11.4808" y1="1.3568" x2="-10.1092" y2="-0.0148" width="0" layer="48"/>
<wire x1="-10.1092" y1="-0.0148" x2="-8.7376" y2="1.3568" width="0" layer="48"/>
<wire x1="-8.7376" y1="1.3568" x2="-7.366" y2="-0.0148" width="0" layer="48"/>
<wire x1="-7.366" y1="-0.0148" x2="-5.9944" y2="1.3568" width="0" layer="48"/>
<wire x1="-3.65" y1="-4.85" x2="-1.65" y2="-4.85" width="0.127" layer="21"/>
<wire x1="1.65" y1="-4.85" x2="3.65" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-1.65" y1="-4.85" x2="1.65" y2="-4.85" width="0.127" layer="48"/>
<wire x1="-3.65" y1="-4.85" x2="-3.65" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-3.65" y1="-0.05" x2="-3.65" y2="-0.25" width="0.127" layer="21"/>
<wire x1="-4" y1="-1.15" x2="-3.3" y2="-1.15" width="0" layer="20" curve="-180"/>
<wire x1="-3.3" y1="-2.35" x2="-4" y2="-2.35" width="0" layer="20" curve="-180"/>
<wire x1="-3.3" y1="-1.15" x2="-3.3" y2="-2.35" width="0" layer="20"/>
<wire x1="-4" y1="-1.15" x2="-4" y2="-2.35" width="0" layer="20"/>
<wire x1="-3.65" y1="-0.25" x2="-3.65" y2="-3.3" width="0.127" layer="48"/>
<wire x1="3.3" y1="-1.15" x2="4" y2="-1.15" width="0" layer="20" curve="-180"/>
<wire x1="4" y1="-2.35" x2="3.3" y2="-2.35" width="0" layer="20" curve="-180"/>
<wire x1="3.3" y1="-1.15" x2="3.3" y2="-2.35" width="0" layer="20"/>
<wire x1="4" y1="-1.15" x2="4" y2="-2.35" width="0" layer="20"/>
<wire x1="3.65" y1="-4.85" x2="3.65" y2="-3.25" width="0.127" layer="21"/>
<wire x1="3.65" y1="-0.25" x2="3.65" y2="-0.05" width="0.127" layer="21"/>
<wire x1="3.65" y1="-3.25" x2="3.65" y2="-0.25" width="0.127" layer="48"/>
<circle x="1.99" y="-4.32" radius="0.0223" width="0.6096" layer="21"/>
<pad name="D+" x="0" y="-6.8" drill="0.7" diameter="1.1" shape="offset" rot="R270"/>
<pad name="GND" x="-1.6" y="-6.8" drill="0.7" diameter="1.1" shape="offset" rot="R270"/>
<pad name="VCC" x="1.6" y="-6.8" drill="0.7" diameter="1.1" shape="offset" rot="R270"/>
<pad name="D-" x="0.8" y="-5.6" drill="0.7" diameter="1.1" shape="offset" rot="R90"/>
<pad name="ID" x="-0.8" y="-5.6" drill="0.7" diameter="1.1" shape="offset" rot="R90"/>
<smd name="FGND@3" x="-3.65" y="-1.75" dx="1.7" dy="2.7" layer="1" roundness="100" rot="R180"/>
<smd name="FGND@2" x="3.65" y="-1.75" dx="1.7" dy="2.7" layer="1" roundness="100" rot="R180"/>
<smd name="FGND@1" x="-3.65" y="-1.75" dx="1.7" dy="2.7" layer="16" roundness="100"/>
<smd name="FGND@4" x="3.65" y="-1.75" dx="1.7" dy="2.7" layer="16" roundness="100"/>
<text x="-11.5316" y="1.56" size="1.27" layer="48">Board </text>
<text x="-10.4616" y="-10.12" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="0.3484" y="-10.22" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3" y1="-7.5" x2="4.4" y2="-5" layer="41"/>
<rectangle x1="-4.4" y1="-7.5" x2="-3" y2="-5" layer="41"/>
</package>
<package name="USB-MINIB-DU1">
<wire x1="-3.65" y1="-0.03" x2="-3.65" y2="2.02" width="0.127" layer="21"/>
<wire x1="-3.65" y1="2.02" x2="3.65" y2="2.02" width="0.127" layer="21"/>
<wire x1="3.65" y1="2.02" x2="3.65" y2="-0.03" width="0.127" layer="21"/>
<wire x1="-3.65" y1="-0.03" x2="3.65" y2="-0.03" width="0.127" layer="21"/>
<wire x1="-11.4808" y1="1.3568" x2="-10.1092" y2="-0.0148" width="0" layer="48"/>
<wire x1="-10.1092" y1="-0.0148" x2="-8.7376" y2="1.3568" width="0" layer="48"/>
<wire x1="-8.7376" y1="1.3568" x2="-7.366" y2="-0.0148" width="0" layer="48"/>
<wire x1="-7.366" y1="-0.0148" x2="-5.9944" y2="1.3568" width="0" layer="48"/>
<wire x1="-3.65" y1="-4.85" x2="-1.65" y2="-4.85" width="0.127" layer="21"/>
<wire x1="1.65" y1="-4.85" x2="3.65" y2="-4.85" width="0.127" layer="21"/>
<wire x1="-1.65" y1="-4.85" x2="1.65" y2="-4.85" width="0.127" layer="48"/>
<wire x1="-3.65" y1="-4.85" x2="-3.65" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-3.65" y1="-0.05" x2="-3.65" y2="-0.25" width="0.127" layer="21"/>
<wire x1="-3.65" y1="-0.25" x2="-3.65" y2="-3.3" width="0.127" layer="48"/>
<wire x1="3.65" y1="-4.85" x2="3.65" y2="-3.25" width="0.127" layer="21"/>
<wire x1="3.65" y1="-0.25" x2="3.65" y2="-0.05" width="0.127" layer="21"/>
<wire x1="3.65" y1="-3.25" x2="3.65" y2="-0.25" width="0.127" layer="48"/>
<circle x="1.99" y="-4.32" radius="0.0223" width="0.6096" layer="21"/>
<pad name="D+" x="0" y="-6.8" drill="0.7" diameter="1.1" shape="offset" rot="R270"/>
<pad name="GND" x="-1.6" y="-6.8" drill="0.7" diameter="1.1" shape="offset" rot="R270"/>
<pad name="VCC" x="1.6" y="-6.8" drill="0.7" diameter="1.1" shape="offset" rot="R270"/>
<pad name="D-" x="0.8" y="-5.6" drill="0.7" diameter="1.1" shape="offset" rot="R90"/>
<pad name="ID" x="-0.8" y="-5.6" drill="0.7" diameter="1.1" shape="offset" rot="R90"/>
<smd name="FGND@3" x="3.65" y="-1.75" dx="2.6" dy="2.6" layer="1" roundness="100"/>
<smd name="FGND@4" x="3.65" y="-1.75" dx="2.6" dy="2.6" layer="16" roundness="100"/>
<smd name="FGND@2" x="-3.65" y="-1.75" dx="2.6" dy="2.6" layer="1" roundness="100"/>
<smd name="FGND@1" x="-3.65" y="-1.75" dx="2.6" dy="2.6" layer="16" roundness="100"/>
<text x="-11.5316" y="1.56" size="1.27" layer="48">Board </text>
<text x="-10.4616" y="-10.12" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="0.3484" y="-10.22" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-12.25" y="-4.55" size="1.27" layer="250">D=1,78!!!</text>
<rectangle x1="3" y1="-7.5" x2="4.4" y2="-5" layer="41"/>
<rectangle x1="-4.4" y1="-7.5" x2="-3" y2="-5" layer="41"/>
<hole x="-3.65" y="-1.75" drill="1.778"/>
<hole x="3.65" y="-1.75" drill="1.778"/>
</package>
</packages>
<symbols>
<symbol name="FGND">
<pin name="FGND" x="0" y="-7.62" direction="pwr" rot="R90"/>
</symbol>
<symbol name="USB-MINI-B">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<text x="0" y="6.35" size="1.778" layer="96">&gt;VALUE</text>
<text x="-8.89" y="6.35" size="1.6764" layer="95">&gt;NAME</text>
<pin name="VBUS" x="-10.16" y="2.54" length="middle" direction="pwr"/>
<pin name="GND" x="-10.16" y="-7.62" length="middle" direction="pwr"/>
<pin name="D+" x="-10.16" y="-2.54" length="middle"/>
<pin name="D-" x="-10.16" y="0" length="middle"/>
<pin name="ID" x="-10.16" y="-5.08" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB_B_MINI" prefix="CON">
<gates>
<gate name="G$2" symbol="FGND" x="10.16" y="0" addlevel="request"/>
<gate name="G$3" symbol="FGND" x="12.7" y="0" addlevel="request"/>
<gate name="&gt;NAME" symbol="USB-MINI-B" x="0" y="2.54"/>
<gate name="G$1" symbol="FGND" x="15.24" y="0" addlevel="request"/>
<gate name="G$4" symbol="FGND" x="17.78" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="USB-MINIB">
<connects>
<connect gate="&gt;NAME" pin="D+" pad="D+"/>
<connect gate="&gt;NAME" pin="D-" pad="D-"/>
<connect gate="&gt;NAME" pin="GND" pad="GND"/>
<connect gate="&gt;NAME" pin="ID" pad="ID"/>
<connect gate="&gt;NAME" pin="VBUS" pad="VCC"/>
<connect gate="G$1" pin="FGND" pad="FGND@3"/>
<connect gate="G$2" pin="FGND" pad="FGND@1"/>
<connect gate="G$3" pin="FGND" pad="FGND@2"/>
<connect gate="G$4" pin="FGND" pad="FGND@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DU" package="USB-MINIB-DU">
<connects>
<connect gate="&gt;NAME" pin="D+" pad="D+"/>
<connect gate="&gt;NAME" pin="D-" pad="D-"/>
<connect gate="&gt;NAME" pin="GND" pad="GND"/>
<connect gate="&gt;NAME" pin="ID" pad="ID"/>
<connect gate="&gt;NAME" pin="VBUS" pad="VCC"/>
<connect gate="G$1" pin="FGND" pad="FGND@1"/>
<connect gate="G$2" pin="FGND" pad="FGND@2"/>
<connect gate="G$3" pin="FGND" pad="FGND@3"/>
<connect gate="G$4" pin="FGND" pad="FGND@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DU1" package="USB-MINIB-DU1">
<connects>
<connect gate="&gt;NAME" pin="D+" pad="D+"/>
<connect gate="&gt;NAME" pin="D-" pad="D-"/>
<connect gate="&gt;NAME" pin="GND" pad="GND"/>
<connect gate="&gt;NAME" pin="ID" pad="ID"/>
<connect gate="&gt;NAME" pin="VBUS" pad="VCC"/>
<connect gate="G$1" pin="FGND" pad="FGND@1"/>
<connect gate="G$2" pin="FGND" pad="FGND@2"/>
<connect gate="G$3" pin="FGND" pad="FGND@3"/>
<connect gate="G$4" pin="FGND" pad="FGND@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="PE">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.6223" y1="-1.016" x2="0.6223" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-0.3048" y1="-1.524" x2="0.3302" y2="-1.524" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="PE" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+24V">
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PE" prefix="PE">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="M" symbol="PE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MYPASS">
<packages>
<package name="0805">
<wire x1="-0.3556" y1="0.4572" x2="0.3556" y2="0.4572" width="0.127" layer="21"/>
<wire x1="0.3556" y1="-0.4572" x2="-0.3556" y2="-0.4572" width="0.127" layer="21"/>
<wire x1="0.3556" y1="0.4572" x2="0.3556" y2="-0.4572" width="0.127" layer="21"/>
<wire x1="-0.3556" y1="0.4572" x2="-0.3556" y2="-0.4572" width="0.127" layer="21"/>
<smd name="2" x="0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<smd name="1" x="-0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<text x="-0.889" y="-2.159" size="0.8" layer="27">&gt;VALUE</text>
<text x="-0.889" y="1.016" size="0.8" layer="25">&gt;NAME</text>
<rectangle x1="0.4064" y1="-0.6096" x2="0.9144" y2="0.6096" layer="27"/>
<rectangle x1="-0.9144" y1="-0.6096" x2="-0.4064" y2="0.6096" layer="27"/>
<rectangle x1="-0.3" y1="-0.4" x2="0.3" y2="0.4" layer="35"/>
</package>
<package name="MT-CRYSTAL">
<wire x1="-0.55" y1="-0.35" x2="2.75" y2="-0.35" width="0.127" layer="21"/>
<wire x1="2.75" y1="-0.35" x2="2.75" y2="2.15" width="0.127" layer="21"/>
<wire x1="2.75" y1="2.15" x2="-0.55" y2="2.15" width="0.127" layer="21"/>
<wire x1="-0.55" y1="2.15" x2="-0.55" y2="-0.35" width="0.127" layer="21"/>
<smd name="P$1" x="0" y="0" dx="1.3" dy="1" layer="1"/>
<smd name="P$2" x="2.2" y="0" dx="1.3" dy="1" layer="1"/>
<smd name="P$3" x="2.2" y="1.8" dx="1.3" dy="1" layer="1"/>
<smd name="P$4" x="0" y="1.8" dx="1.3" dy="1" layer="1"/>
<text x="-0.7" y="2.4" size="1.016" layer="25">&gt;name</text>
<text x="-0.9" y="-1.6" size="1.016" layer="25">&gt;VALUE</text>
</package>
<package name="ARC241">
<wire x1="-0.3" y1="-0.1" x2="-0.4" y2="-0.1" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-0.1" x2="-0.4" y2="1.5" width="0.127" layer="21"/>
<wire x1="-0.4" y1="1.5" x2="-0.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="2.7" y1="1.5" x2="2.8" y2="1.5" width="0.127" layer="21"/>
<wire x1="2.8" y1="1.5" x2="2.8" y2="-0.1" width="0.127" layer="21"/>
<wire x1="2.8" y1="-0.1" x2="2.7" y2="-0.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="-0.1" x2="1.9" y2="-0.1" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.5" x2="1.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.3" y1="-0.1" x2="1.1" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.5" y1="-0.1" x2="0.3" y2="-0.1" width="0.127" layer="21"/>
<wire x1="0.3" y1="1.5" x2="0.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.1" y1="1.5" x2="1.3" y2="1.5" width="0.127" layer="21"/>
<smd name="1" x="0" y="-0.1" dx="0.45" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="-0.1" dx="0.45" dy="0.8" layer="1"/>
<smd name="3" x="1.6" y="-0.1" dx="0.45" dy="0.8" layer="1"/>
<smd name="4" x="2.4" y="-0.1" dx="0.45" dy="0.8" layer="1"/>
<smd name="5" x="2.4" y="1.5" dx="0.45" dy="0.8" layer="1"/>
<smd name="6" x="1.6" y="1.5" dx="0.45" dy="0.8" layer="1"/>
<smd name="7" x="0.8" y="1.5" dx="0.45" dy="0.8" layer="1"/>
<smd name="8" x="0" y="1.5" dx="0.45" dy="0.8" layer="1"/>
<text x="-0.297" y="-1.84" size="1.27" layer="27">&gt;VALUE</text>
<text x="-0.297" y="1.97" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.2" y1="1.1" x2="0.2" y2="1.7" layer="27"/>
<rectangle x1="0.6" y1="1.1" x2="1" y2="1.7" layer="27"/>
<rectangle x1="1.4" y1="1.1" x2="1.8" y2="1.7" layer="27"/>
<rectangle x1="2.2" y1="1.1" x2="2.6" y2="1.7" layer="27"/>
<rectangle x1="2.2" y1="-0.3" x2="2.6" y2="0.3" layer="27"/>
<rectangle x1="1.4" y1="-0.3" x2="1.8" y2="0.3" layer="27"/>
<rectangle x1="0.6" y1="-0.3" x2="1" y2="0.3" layer="27"/>
<rectangle x1="-0.2" y1="-0.3" x2="0.2" y2="0.3" layer="27"/>
</package>
<package name="1206">
<wire x1="-0.8636" y1="0.635" x2="0.8636" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.8636" y1="-0.635" x2="0.8636" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.8636" y1="0.635" x2="0.8636" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.8636" y1="0.635" x2="-0.8636" y2="-0.635" width="0.127" layer="21"/>
<smd name="2" x="1.4732" y="0" dx="1.143" dy="1.7018" layer="1"/>
<smd name="1" x="-1.4732" y="0" dx="1.143" dy="1.7018" layer="1"/>
<text x="-1.397" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.397" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-1.4224" y1="-0.7874" x2="-0.9144" y2="0.7874" layer="27"/>
<rectangle x1="0.9144" y1="-0.7874" x2="1.4224" y2="0.7874" layer="27"/>
<rectangle x1="-0.7" y1="-0.5" x2="0.7" y2="0.5" layer="35"/>
</package>
<package name="1210">
<wire x1="-0.8636" y1="1.143" x2="0.8636" y2="1.143" width="0.127" layer="21"/>
<wire x1="-0.8636" y1="-1.143" x2="0.8636" y2="-1.143" width="0.127" layer="21"/>
<wire x1="0.8636" y1="1.143" x2="0.8636" y2="-1.143" width="0.127" layer="21"/>
<wire x1="-0.8636" y1="1.143" x2="-0.8636" y2="-1.143" width="0.127" layer="21"/>
<smd name="2" x="1.4732" y="0" dx="1.143" dy="2.7178" layer="1"/>
<smd name="1" x="-1.4732" y="0" dx="1.143" dy="2.7178" layer="1"/>
<text x="-1.397" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.397" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-1.4224" y1="-0.7874" x2="-0.9144" y2="0.7874" layer="27"/>
<rectangle x1="0.9144" y1="-0.7874" x2="1.4224" y2="0.7874" layer="27"/>
<rectangle x1="-0.7" y1="-0.5" x2="0.7" y2="0.5" layer="35"/>
</package>
<package name="SMD">
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="21"/>
<wire x1="4" y1="3" x2="4" y2="2" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-4" y2="2" width="0.127" layer="21"/>
<wire x1="4" y1="-2" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="-3" x2="-4" y2="-2" width="0.127" layer="21"/>
<wire x1="4" y1="2" x2="4" y2="-2" width="0.127" layer="27"/>
<wire x1="-4" y1="2" x2="-4" y2="-2" width="0.127" layer="27"/>
<wire x1="-2.5" y1="0" x2="-2" y2="0" width="0.127" layer="21"/>
<wire x1="-2" y1="0" x2="-1.5" y2="1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="-1" y1="-1" x2="-0.5" y2="1" width="0.127" layer="21"/>
<wire x1="-0.5" y1="1" x2="0" y2="-1" width="0.127" layer="21"/>
<wire x1="0" y1="-1" x2="0.5" y2="1" width="0.127" layer="21"/>
<wire x1="0.5" y1="1" x2="1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="1.5" y2="1" width="0.127" layer="21"/>
<wire x1="1.5" y1="1" x2="2" y2="0" width="0.127" layer="21"/>
<wire x1="2" y1="0" x2="2.5" y2="0" width="0.127" layer="21"/>
<wire x1="1" y1="2" x2="0.5" y2="2" width="0.127" layer="21"/>
<wire x1="0.5" y1="2" x2="-0.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-2" x2="-1" y2="-2" width="0.127" layer="21"/>
<smd name="1" x="-3.7" y="0" dx="2.3" dy="3.1" layer="1"/>
<smd name="2" x="3.7" y="0" dx="2.3" dy="3.1" layer="1"/>
<text x="-3.81" y="3.31" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.81" y="-5.35" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="SMD2">
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="21"/>
<wire x1="4" y1="3" x2="4" y2="2.4" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-4" y2="2.4" width="0.127" layer="21"/>
<wire x1="4" y1="-2.4" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="-3" x2="-4" y2="-2.4" width="0.127" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-2.5" width="0.127" layer="27"/>
<wire x1="-4" y1="2.5" x2="-4" y2="-2.5" width="0.127" layer="27"/>
<wire x1="-2.5" y1="0" x2="-2" y2="0" width="0.127" layer="21"/>
<wire x1="-2" y1="0" x2="-1.5" y2="1" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="-1" y1="-1" x2="-0.5" y2="1" width="0.127" layer="21"/>
<wire x1="-0.5" y1="1" x2="0" y2="-1" width="0.127" layer="21"/>
<wire x1="0" y1="-1" x2="0.5" y2="1" width="0.127" layer="21"/>
<wire x1="0.5" y1="1" x2="1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="1.5" y2="1" width="0.127" layer="21"/>
<wire x1="1.5" y1="1" x2="2" y2="0" width="0.127" layer="21"/>
<wire x1="2" y1="0" x2="2.5" y2="0" width="0.127" layer="21"/>
<wire x1="1" y1="2" x2="0.5" y2="2" width="0.127" layer="21"/>
<wire x1="0.5" y1="2" x2="-0.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-2" x2="-1" y2="-2" width="0.127" layer="21"/>
<smd name="1" x="-4.2" y="0" dx="2.3" dy="4.6" layer="1"/>
<smd name="2" x="4.2" y="0" dx="2.3" dy="4.6" layer="1"/>
<text x="-3.81" y="3.31" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.81" y="-5.35" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="MINISMD">
<wire x1="-2.6" y1="-1.7" x2="-1.1" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-1.7" x2="1.1" y2="-1.7" width="0.127" layer="21"/>
<wire x1="1.1" y1="-1.7" x2="2.6" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2.6" y1="-1.7" x2="2.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="2.6" y1="1.7" x2="1.1" y2="1.7" width="0.127" layer="21"/>
<wire x1="1.1" y1="1.7" x2="-1.1" y2="1.7" width="0.127" layer="21"/>
<wire x1="-1.1" y1="1.7" x2="-2.6" y2="1.7" width="0.127" layer="21"/>
<wire x1="-2.6" y1="1.7" x2="-2.6" y2="-1.7" width="0.127" layer="21"/>
<wire x1="1.1" y1="1.7" x2="1.1" y2="0" width="0.127" layer="21"/>
<wire x1="1.1" y1="0" x2="1.1" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-1.1" y1="1.7" x2="-1.1" y2="0" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0" x2="-1.1" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0" x2="1.1" y2="0" width="0.127" layer="21"/>
<smd name="1" x="-1.85" y="0" dx="1.2" dy="3.1" layer="1"/>
<smd name="2" x="1.85" y="0" dx="1.2" dy="3.1" layer="1"/>
<text x="-2.6" y="1.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.6" y="-3.2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1812">
<wire x1="-1.3462" y1="1.4478" x2="1.3462" y2="1.4478" width="0.127" layer="21"/>
<wire x1="1.3462" y1="-1.4478" x2="-1.3462" y2="-1.4478" width="0.127" layer="21"/>
<wire x1="1.3462" y1="1.4478" x2="1.3462" y2="-1.4478" width="0.127" layer="21"/>
<wire x1="-1.3462" y1="1.4478" x2="-1.3462" y2="-1.4478" width="0.127" layer="21"/>
<smd name="2" x="2.2352" y="0" dx="1.7018" dy="3.302" layer="1" roundness="45"/>
<smd name="1" x="-2.2352" y="0" dx="1.7018" dy="3.302" layer="1" roundness="45"/>
<text x="-2.032" y="-3.683" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.032" y="2.032" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-2.0066" y1="-1.6002" x2="-1.397" y2="1.6002" layer="27"/>
<rectangle x1="1.397" y1="-1.6002" x2="2.0066" y2="1.6002" layer="27"/>
<rectangle x1="-0.5" y1="-0.5" x2="0.5" y2="0.5" layer="35"/>
</package>
<package name="2018">
<wire x1="-1.5462" y1="2.1478" x2="1.5462" y2="2.1478" width="0.127" layer="21"/>
<wire x1="1.5462" y1="-2.1478" x2="-1.5462" y2="-2.1478" width="0.127" layer="21"/>
<wire x1="1.5462" y1="2.1478" x2="1.5462" y2="-2.1478" width="0.127" layer="21"/>
<wire x1="-1.5462" y1="2.1478" x2="-1.5462" y2="-2.1478" width="0.127" layer="21"/>
<smd name="2" x="2.4352" y="0" dx="1.5" dy="4.6" layer="1" roundness="45"/>
<smd name="1" x="-2.4352" y="0" dx="1.5" dy="4.6" layer="1" roundness="45"/>
<text x="-2.032" y="-3.783" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.032" y="2.532" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-0.5" y1="-0.5" x2="0.5" y2="0.5" layer="35"/>
<rectangle x1="-2.3" y1="-2.2" x2="-1.6" y2="2.2" layer="51"/>
<rectangle x1="1.6" y1="-2.2" x2="2.3" y2="2.2" layer="51"/>
</package>
<package name="DO3340">
<wire x1="-1.905" y1="6.35" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-1.905" y2="-6.35" width="0.127" layer="21"/>
<wire x1="1.905" y1="6.35" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="1.905" y2="-6.35" width="0.127" layer="21"/>
<wire x1="1.905" y1="-6.35" x2="1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.905" y1="-3.175" x2="-1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-3.175" x2="-1.905" y2="-6.35" width="0.127" layer="21"/>
<wire x1="1.905" y1="6.35" x2="1.905" y2="3.175" width="0.127" layer="21"/>
<wire x1="1.905" y1="3.175" x2="-1.905" y2="3.175" width="0.127" layer="21"/>
<wire x1="-1.905" y1="3.175" x2="-1.905" y2="6.35" width="0.127" layer="21"/>
<wire x1="-1.905" y1="6.35" x2="1.905" y2="6.35" width="0.127" layer="27"/>
<wire x1="1.905" y1="-6.35" x2="-1.905" y2="-6.35" width="0.127" layer="27"/>
<circle x="0" y="0" radius="4.445" width="0.127" layer="27"/>
<smd name="2" x="0" y="5.1308" dx="2.794" dy="2.921" layer="1"/>
<smd name="1" x="0" y="-5.1308" dx="2.794" dy="2.921" layer="1"/>
<text x="7.62" y="-2.54" size="1.778" layer="27" rot="R90">&gt;VALUE</text>
<text x="-5.715" y="-2.54" size="1.778" layer="25" rot="R90">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="7.62" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="7.62" y1="-1.27" x2="7.62" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="7.62" y2="1.27" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="10.16" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CAP">
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="-0.508" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="QUARZ">
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="1.778" x2="-0.762" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-1.778" x2="-0.762" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="-1.778" x2="-1.778" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="1.778" x2="-1.778" y2="-1.778" width="0.1524" layer="94"/>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="GND">
<pin name="GND" x="0" y="-7.62" direction="pwr" rot="R90"/>
</symbol>
<symbol name="POLYSW">
<wire x1="-5.08" y1="0" x2="-3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="5.08" y2="0" width="0.254" layer="94"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="INDUCTOR">
<text x="-6.35" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-1.27" x2="0" y2="1.27" layer="94"/>
<pin name="1" x="-10.16" y="0" visible="off" length="middle" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="off" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RES0805" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP0805" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MT-CRYSTAL" prefix="Q" uservalue="yes">
<description>3,2 x 2,5mm Reichelt</description>
<gates>
<gate name="G$1" symbol="QUARZ" x="0" y="0"/>
<gate name="G$2" symbol="GND" x="-22.86" y="-7.62"/>
<gate name="G$3" symbol="GND" x="-17.78" y="-7.62"/>
</gates>
<devices>
<device name="" package="MT-CRYSTAL">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$3"/>
<connect gate="G$2" pin="GND" pad="P$2"/>
<connect gate="G$3" pin="GND" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ARC241" prefix="R" uservalue="yes">
<gates>
<gate name="A" symbol="RESISTOR" x="-7.62" y="15.24" swaplevel="1"/>
<gate name="B" symbol="RESISTOR" x="-7.62" y="5.08" swaplevel="1"/>
<gate name="C" symbol="RESISTOR" x="-7.62" y="-5.08" swaplevel="1"/>
<gate name="D" symbol="RESISTOR" x="-7.62" y="-15.24" swaplevel="1"/>
</gates>
<devices>
<device name="" package="ARC241">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP1206" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP1210" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POLYSWITCH" prefix="F" uservalue="yes">
<gates>
<gate name="G$1" symbol="POLYSW" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2" package="SMD2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINISMD" package="MINISMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINISMDC" package="1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2018" package="2018">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DO3340P" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="DO3340">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES1206" prefix="R" uservalue="yes">
<gates>
<gate name="G" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1206">
<connects>
<connect gate="G" pin="1" pad="1"/>
<connect gate="G" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="74xx-eu">
<description>&lt;b&gt;TTL Devices, 74xx Series with European Symbols&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Texas Instruments &lt;i&gt;TTL Data Book&lt;/i&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;Volume 1, 1996.
&lt;li&gt;TTL Data Book, Volume 2 , 1993
&lt;li&gt;National Seminconductor Databook 1990, ALS/LS Logic
&lt;li&gt;ttl 74er digital data dictionary, ECA Electronic + Acustic GmbH, ISBN 3-88109-032-0
&lt;li&gt;http://icmaster.com/ViewCompare.asp
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL14">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="8.89" y1="2.921" x2="-8.89" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-2.921" x2="8.89" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="8.89" y1="2.921" x2="8.89" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="2.921" x2="-8.89" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-2.921" x2="-8.89" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="1.016" x2="-8.89" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-7.62" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="7.62" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="0" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="5.08" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="0" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="-2.54" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-5.08" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-7.62" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-9.271" y="-3.048" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-6.731" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO14">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<wire x1="4.064" y1="1.9558" x2="-4.064" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="4.064" y1="-1.9558" x2="4.445" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.445" y1="1.5748" x2="-4.064" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.064" y1="1.9558" x2="4.445" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.445" y1="-1.5748" x2="-4.064" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.064" y1="-1.9558" x2="4.064" y2="-1.9558" width="0.1524" layer="51"/>
<wire x1="4.445" y1="-1.5748" x2="4.445" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.5748" x2="-4.445" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="0.508" x2="-4.445" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-0.508" x2="-4.445" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="0.508" x2="-4.445" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-4.445" y1="-1.6002" x2="4.445" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-3.81" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-3.81" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-2.54" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.27" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-2.54" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="-1.27" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="0" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="0" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="1.27" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="2.54" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="1.27" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="2.54" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.81" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="3.81" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-3.175" y="-0.762" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.826" y="-1.905" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.254" y1="1.9558" x2="0.254" y2="3.0988" layer="51"/>
<rectangle x1="-4.064" y1="-3.0988" x2="-3.556" y2="-1.9558" layer="51"/>
<rectangle x1="-2.794" y1="-3.0988" x2="-2.286" y2="-1.9558" layer="51"/>
<rectangle x1="-1.524" y1="-3.0734" x2="-1.016" y2="-1.9304" layer="51"/>
<rectangle x1="-0.254" y1="-3.0988" x2="0.254" y2="-1.9558" layer="51"/>
<rectangle x1="-1.524" y1="1.9558" x2="-1.016" y2="3.0988" layer="51"/>
<rectangle x1="-2.794" y1="1.9558" x2="-2.286" y2="3.0988" layer="51"/>
<rectangle x1="-4.064" y1="1.9558" x2="-3.556" y2="3.0988" layer="51"/>
<rectangle x1="1.016" y1="1.9558" x2="1.524" y2="3.0988" layer="51"/>
<rectangle x1="2.286" y1="1.9558" x2="2.794" y2="3.0988" layer="51"/>
<rectangle x1="3.556" y1="1.9558" x2="4.064" y2="3.0988" layer="51"/>
<rectangle x1="1.016" y1="-3.0988" x2="1.524" y2="-1.9558" layer="51"/>
<rectangle x1="2.286" y1="-3.0988" x2="2.794" y2="-1.9558" layer="51"/>
<rectangle x1="3.556" y1="-3.0988" x2="4.064" y2="-1.9558" layer="51"/>
</package>
<package name="LCC20">
<description>&lt;b&gt;Leadless Chip Carrier&lt;/b&gt;&lt;p&gt; Ceramic Package</description>
<wire x1="-0.4001" y1="4.4" x2="-0.87" y2="4.4" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="4.4" x2="-4.4" y2="3.3" width="0.2032" layer="51"/>
<wire x1="-0.4001" y1="4.3985" x2="0.4001" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-1.6701" y1="4.3985" x2="-0.8699" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.3985" y1="2.14" x2="-4.3985" y2="2.94" width="0.2032" layer="51" curve="180"/>
<wire x1="-2.9401" y1="4.4" x2="-3.3" y2="4.4" width="0.2032" layer="51"/>
<wire x1="0.87" y1="4.4" x2="0.4001" y2="4.4" width="0.2032" layer="51"/>
<wire x1="0.87" y1="4.3985" x2="1.67" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.4" y1="3.3" x2="-4.4" y2="2.9401" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="2.14" x2="-4.4" y2="1.6701" width="0.2032" layer="51"/>
<wire x1="-4.3985" y1="0.87" x2="-4.3985" y2="1.67" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.3985" y1="-0.4001" x2="-4.3985" y2="0.4001" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.3985" y1="-1.6701" x2="-4.3985" y2="-0.8699" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.4" y1="0.87" x2="-4.4" y2="0.4001" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-0.4001" x2="-4.4" y2="-0.87" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-2.9401" x2="-4.4" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-4.4" x2="-4.4" y2="-4.4099" width="0.2032" layer="51"/>
<wire x1="2.14" y1="4.3985" x2="2.94" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="2.14" y1="4.4" x2="1.6701" y2="4.4" width="0.2032" layer="51"/>
<wire x1="4.4" y1="4.4" x2="2.9401" y2="4.4" width="0.2032" layer="51"/>
<wire x1="0.4001" y1="-4.4" x2="0.87" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-0.4001" y1="-4.3985" x2="0.4001" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="0.87" y1="-4.3985" x2="1.67" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="2.9401" y1="-4.4" x2="4.4" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-0.87" y1="-4.4" x2="-0.4001" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-1.6701" y1="-4.3985" x2="-0.8699" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="-2.9401" y1="-4.3985" x2="-2.1399" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="-2.14" y1="-4.4" x2="-1.6701" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="-4.4" y1="-4.4" x2="-2.9401" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="4.4" y1="0.4001" x2="4.4" y2="0.87" width="0.2032" layer="51"/>
<wire x1="4.3985" y1="0.4001" x2="4.3985" y2="-0.4001" width="0.2032" layer="51" curve="180"/>
<wire x1="4.3985" y1="1.6701" x2="4.3985" y2="0.8699" width="0.2032" layer="51" curve="180"/>
<wire x1="4.4" y1="2.9401" x2="4.4" y2="4.4" width="0.2032" layer="51"/>
<wire x1="4.4" y1="-0.87" x2="4.4" y2="-0.4001" width="0.2032" layer="51"/>
<wire x1="4.3985" y1="-0.87" x2="4.3985" y2="-1.67" width="0.2032" layer="51" curve="180"/>
<wire x1="4.3985" y1="-2.14" x2="4.3985" y2="-2.94" width="0.2032" layer="51" curve="180"/>
<wire x1="4.4" y1="-2.14" x2="4.4" y2="-1.6701" width="0.2032" layer="51"/>
<wire x1="4.4" y1="-4.4" x2="4.4" y2="-2.9401" width="0.2032" layer="51"/>
<wire x1="-2.9401" y1="4.3985" x2="-2.1399" y2="4.3985" width="0.2032" layer="51" curve="180"/>
<wire x1="-1.6701" y1="4.4" x2="-2.14" y2="4.4" width="0.2032" layer="51"/>
<wire x1="-4.3985" y1="-2.9401" x2="-4.3985" y2="-2.1399" width="0.2032" layer="51" curve="180"/>
<wire x1="-4.4" y1="-1.6701" x2="-4.4" y2="-2.14" width="0.2032" layer="51"/>
<wire x1="1.6701" y1="-4.4" x2="2.14" y2="-4.4" width="0.2032" layer="51"/>
<wire x1="2.14" y1="-4.3985" x2="2.94" y2="-4.3985" width="0.2032" layer="51" curve="-180"/>
<wire x1="4.3985" y1="2.9401" x2="4.3985" y2="2.1399" width="0.2032" layer="51" curve="180"/>
<wire x1="4.4" y1="1.6701" x2="4.4" y2="2.14" width="0.2032" layer="51"/>
<wire x1="-3.3" y1="4.4" x2="-4.4" y2="3.3" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="-3.1941" x2="-4.4" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="-4.4" x2="-3.1941" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="3.1941" y1="-4.4" x2="4.4" y2="-4.4" width="0.2032" layer="21"/>
<wire x1="4.4" y1="-4.4" x2="4.4" y2="-3.1941" width="0.2032" layer="21"/>
<wire x1="4.4" y1="3.1941" x2="4.4" y2="4.4" width="0.2032" layer="21"/>
<wire x1="4.4" y1="4.4" x2="3.1941" y2="4.4" width="0.2032" layer="21"/>
<smd name="2" x="-1.27" y="4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="1" x="0" y="3.8001" dx="0.8" dy="3.4" layer="1"/>
<smd name="3" x="-2.54" y="4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="4" x="-4.5001" y="2.54" dx="2" dy="0.8" layer="1"/>
<smd name="5" x="-4.5001" y="1.27" dx="2" dy="0.8" layer="1"/>
<smd name="6" x="-4.5001" y="0" dx="2" dy="0.8" layer="1"/>
<smd name="7" x="-4.5001" y="-1.27" dx="2" dy="0.8" layer="1"/>
<smd name="8" x="-4.5001" y="-2.54" dx="2" dy="0.8" layer="1"/>
<smd name="9" x="-2.54" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="10" x="-1.27" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="11" x="0" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="12" x="1.27" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="13" x="2.54" y="-4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="14" x="4.5001" y="-2.54" dx="2" dy="0.8" layer="1"/>
<smd name="15" x="4.5001" y="-1.27" dx="2" dy="0.8" layer="1"/>
<smd name="16" x="4.5001" y="0" dx="2" dy="0.8" layer="1"/>
<smd name="17" x="4.5001" y="1.27" dx="2" dy="0.8" layer="1"/>
<smd name="18" x="4.5001" y="2.54" dx="2" dy="0.8" layer="1"/>
<smd name="19" x="2.54" y="4.5001" dx="0.8" dy="2" layer="1"/>
<smd name="20" x="1.27" y="4.5001" dx="0.8" dy="2" layer="1"/>
<text x="-4.0051" y="6.065" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.9751" y="-7.5601" size="1.778" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="7411">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94" curve="-180"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I0" x="-7.62" y="2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
<pin name="I1" x="-7.62" y="0" visible="pad" length="middle" direction="in" swaplevel="1"/>
<pin name="O" x="7.62" y="0" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="I2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
</symbol>
<symbol name="PWRN">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
</symbol>
<symbol name="7410">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.4064" layer="94" curve="-180"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I0" x="-7.62" y="2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
<pin name="I1" x="-7.62" y="0" visible="pad" length="middle" direction="in" swaplevel="1"/>
<pin name="O" x="7.62" y="0" visible="pad" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="I2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="in" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="74*11" prefix="IC">
<description>Triple 3-input &lt;b&gt;AND&lt;/b&gt; gate</description>
<gates>
<gate name="A" symbol="7411" x="15.24" y="5.08" swaplevel="1"/>
<gate name="B" symbol="7411" x="15.24" y="-7.62" swaplevel="1"/>
<gate name="C" symbol="7411" x="40.64" y="5.08" swaplevel="1"/>
<gate name="P" symbol="PWRN" x="-5.08" y="0" addlevel="request"/>
</gates>
<devices>
<device name="N" package="DIL14">
<connects>
<connect gate="A" pin="I0" pad="1"/>
<connect gate="A" pin="I1" pad="2"/>
<connect gate="A" pin="I2" pad="13"/>
<connect gate="A" pin="O" pad="12"/>
<connect gate="B" pin="I0" pad="3"/>
<connect gate="B" pin="I1" pad="4"/>
<connect gate="B" pin="I2" pad="5"/>
<connect gate="B" pin="O" pad="6"/>
<connect gate="C" pin="I0" pad="9"/>
<connect gate="C" pin="I1" pad="10"/>
<connect gate="C" pin="I2" pad="11"/>
<connect gate="C" pin="O" pad="8"/>
<connect gate="P" pin="GND" pad="7"/>
<connect gate="P" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name="AC"/>
<technology name="ACT"/>
<technology name="ALS"/>
<technology name="AS"/>
<technology name="HC"/>
<technology name="HCT"/>
<technology name="LS"/>
<technology name="S"/>
</technologies>
</device>
<device name="D" package="SO14">
<connects>
<connect gate="A" pin="I0" pad="1"/>
<connect gate="A" pin="I1" pad="2"/>
<connect gate="A" pin="I2" pad="13"/>
<connect gate="A" pin="O" pad="12"/>
<connect gate="B" pin="I0" pad="3"/>
<connect gate="B" pin="I1" pad="4"/>
<connect gate="B" pin="I2" pad="5"/>
<connect gate="B" pin="O" pad="6"/>
<connect gate="C" pin="I0" pad="9"/>
<connect gate="C" pin="I1" pad="10"/>
<connect gate="C" pin="I2" pad="11"/>
<connect gate="C" pin="O" pad="8"/>
<connect gate="P" pin="GND" pad="7"/>
<connect gate="P" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name="AC"/>
<technology name="ACT"/>
<technology name="ALS"/>
<technology name="AS"/>
<technology name="HC"/>
<technology name="HCT"/>
<technology name="LS"/>
<technology name="S"/>
</technologies>
</device>
<device name="FK" package="LCC20">
<connects>
<connect gate="A" pin="I0" pad="2"/>
<connect gate="A" pin="I1" pad="3"/>
<connect gate="A" pin="I2" pad="19"/>
<connect gate="A" pin="O" pad="18"/>
<connect gate="B" pin="I0" pad="4"/>
<connect gate="B" pin="I1" pad="6"/>
<connect gate="B" pin="I2" pad="8"/>
<connect gate="B" pin="O" pad="9"/>
<connect gate="C" pin="I0" pad="13"/>
<connect gate="C" pin="I1" pad="14"/>
<connect gate="C" pin="I2" pad="16"/>
<connect gate="C" pin="O" pad="12"/>
<connect gate="P" pin="GND" pad="10"/>
<connect gate="P" pin="VCC" pad="20"/>
</connects>
<technologies>
<technology name="AC"/>
<technology name="ACT"/>
<technology name="ALS"/>
<technology name="AS"/>
<technology name="HC"/>
<technology name="HCT"/>
<technology name="LS"/>
<technology name="S"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="74*10" prefix="IC">
<description>Triple 3-input &lt;b&gt;NAND&lt;/b&gt; gate</description>
<gates>
<gate name="A" symbol="7410" x="15.24" y="7.62" swaplevel="1"/>
<gate name="B" symbol="7410" x="15.24" y="-7.62" swaplevel="1"/>
<gate name="C" symbol="7410" x="38.1" y="7.62" swaplevel="1"/>
<gate name="P" symbol="PWRN" x="-5.08" y="0" addlevel="request"/>
</gates>
<devices>
<device name="N" package="DIL14">
<connects>
<connect gate="A" pin="I0" pad="1"/>
<connect gate="A" pin="I1" pad="2"/>
<connect gate="A" pin="I2" pad="13"/>
<connect gate="A" pin="O" pad="12"/>
<connect gate="B" pin="I0" pad="3"/>
<connect gate="B" pin="I1" pad="4"/>
<connect gate="B" pin="I2" pad="5"/>
<connect gate="B" pin="O" pad="6"/>
<connect gate="C" pin="I0" pad="9"/>
<connect gate="C" pin="I1" pad="10"/>
<connect gate="C" pin="I2" pad="11"/>
<connect gate="C" pin="O" pad="8"/>
<connect gate="P" pin="GND" pad="7"/>
<connect gate="P" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name=""/>
<technology name="AC"/>
<technology name="ACT"/>
<technology name="ALS"/>
<technology name="AS"/>
<technology name="HC"/>
<technology name="HCT"/>
<technology name="LS"/>
<technology name="S"/>
</technologies>
</device>
<device name="D" package="SO14">
<connects>
<connect gate="A" pin="I0" pad="1"/>
<connect gate="A" pin="I1" pad="2"/>
<connect gate="A" pin="I2" pad="13"/>
<connect gate="A" pin="O" pad="12"/>
<connect gate="B" pin="I0" pad="3"/>
<connect gate="B" pin="I1" pad="4"/>
<connect gate="B" pin="I2" pad="5"/>
<connect gate="B" pin="O" pad="6"/>
<connect gate="C" pin="I0" pad="9"/>
<connect gate="C" pin="I1" pad="10"/>
<connect gate="C" pin="I2" pad="11"/>
<connect gate="C" pin="O" pad="8"/>
<connect gate="P" pin="GND" pad="7"/>
<connect gate="P" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name=""/>
<technology name="AC"/>
<technology name="ACT"/>
<technology name="ALS"/>
<technology name="AS"/>
<technology name="HC"/>
<technology name="HCT"/>
<technology name="LS"/>
<technology name="S"/>
</technologies>
</device>
<device name="FK" package="LCC20">
<connects>
<connect gate="A" pin="I0" pad="2"/>
<connect gate="A" pin="I1" pad="3"/>
<connect gate="A" pin="I2" pad="19"/>
<connect gate="A" pin="O" pad="18"/>
<connect gate="B" pin="I0" pad="4"/>
<connect gate="B" pin="I1" pad="6"/>
<connect gate="B" pin="I2" pad="8"/>
<connect gate="B" pin="O" pad="9"/>
<connect gate="C" pin="I0" pad="13"/>
<connect gate="C" pin="I1" pad="14"/>
<connect gate="C" pin="I2" pad="16"/>
<connect gate="C" pin="O" pad="12"/>
<connect gate="P" pin="GND" pad="10"/>
<connect gate="P" pin="VCC" pad="20"/>
</connects>
<technologies>
<technology name=""/>
<technology name="AC"/>
<technology name="ACT"/>
<technology name="ALS"/>
<technology name="AS"/>
<technology name="HC"/>
<technology name="HCT"/>
<technology name="LS"/>
<technology name="S"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CAB">
<description>&lt;b&gt;cab spezial&lt;/b&gt;&lt;p&gt;</description>
<packages>
<package name="107-152">
<wire x1="19.395" y1="9.875" x2="19.395" y2="9.494" width="0.127" layer="21"/>
<wire x1="-19.395" y1="-5.06" x2="-19.097" y2="-5.06" width="0.127" layer="21"/>
<wire x1="19.395" y1="3.83" x2="19.395" y2="-5.06" width="0.127" layer="21"/>
<wire x1="12.537" y1="3.83" x2="12.537" y2="-5.06" width="0.127" layer="21"/>
<wire x1="12.537" y1="-5.06" x2="13.299" y2="-5.06" width="0.127" layer="21"/>
<wire x1="19.395" y1="3.83" x2="13.299" y2="3.83" width="0.127" layer="21"/>
<wire x1="13.299" y1="3.83" x2="13.299" y2="-5.06" width="0.127" layer="21"/>
<wire x1="13.299" y1="3.83" x2="12.537" y2="3.83" width="0.127" layer="21"/>
<wire x1="-12.537" y1="3.83" x2="-12.537" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-12.537" y1="-5.06" x2="12.537" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-12.537" y1="3.83" x2="-13.299" y2="3.83" width="0.127" layer="21"/>
<wire x1="-19.395" y1="-5.06" x2="-19.395" y2="3.83" width="0.127" layer="21"/>
<wire x1="-19.395" y1="3.83" x2="-19.395" y2="7.589" width="0.127" layer="21"/>
<wire x1="-13.299" y1="3.83" x2="-13.299" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-13.299" y1="3.83" x2="-19.395" y2="3.83" width="0.127" layer="21"/>
<wire x1="-13.299" y1="-5.06" x2="-12.537" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-19.395" y1="9.494" x2="19.395" y2="9.494" width="0.127" layer="21"/>
<wire x1="19.395" y1="9.494" x2="19.395" y2="7.589" width="0.127" layer="21"/>
<wire x1="-19.395" y1="9.494" x2="-19.395" y2="9.875" width="0.127" layer="21"/>
<wire x1="-19.395" y1="7.589" x2="19.395" y2="7.589" width="0.127" layer="21"/>
<wire x1="-19.395" y1="7.589" x2="-19.395" y2="9.494" width="0.127" layer="21"/>
<wire x1="19.395" y1="7.589" x2="19.395" y2="3.83" width="0.127" layer="21"/>
<wire x1="19.395" y1="9.875" x2="12.918" y2="9.875" width="0.127" layer="21"/>
<wire x1="12.918" y1="9.875" x2="12.41" y2="10.383" width="0.127" layer="21"/>
<wire x1="12.918" y1="9.875" x2="-12.918" y2="9.875" width="0.127" layer="21"/>
<wire x1="12.41" y1="10.383" x2="12.41" y2="15.444" width="0.127" layer="21"/>
<wire x1="-12.41" y1="15.444" x2="-12.41" y2="10.383" width="0.127" layer="21"/>
<wire x1="-12.41" y1="10.383" x2="-12.918" y2="9.875" width="0.127" layer="21"/>
<wire x1="-12.918" y1="9.875" x2="-19.395" y2="9.875" width="0.127" layer="21"/>
<wire x1="11.902" y1="15.698" x2="11.902" y2="10.383" width="0.127" layer="21"/>
<wire x1="13.299" y1="-5.06" x2="14.525" y2="-5.06" width="0.127" layer="21"/>
<wire x1="14.525" y1="0.02" x2="19.097" y2="0.02" width="0.127" layer="21" curve="-180"/>
<wire x1="19.097" y1="-5.06" x2="19.097" y2="0.02" width="0.127" layer="21"/>
<wire x1="19.097" y1="-5.06" x2="19.395" y2="-5.06" width="0.127" layer="21"/>
<wire x1="14.525" y1="-5.06" x2="14.525" y2="0.02" width="0.127" layer="21"/>
<wire x1="14.525" y1="-5.06" x2="19.097" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-19.097" y1="0.02" x2="-14.525" y2="0.02" width="0.127" layer="21" curve="-180"/>
<wire x1="-14.525" y1="-5.06" x2="-14.525" y2="0.02" width="0.127" layer="21"/>
<wire x1="-14.525" y1="-5.06" x2="-13.299" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-19.097" y1="-5.06" x2="-19.097" y2="0.02" width="0.127" layer="21"/>
<wire x1="-19.097" y1="-5.06" x2="-14.525" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-11.902" y1="15.698" x2="-11.902" y2="10.383" width="0.127" layer="21"/>
<wire x1="11.648" y1="15.952" x2="11.902" y2="15.698" width="0.127" layer="21" curve="-90"/>
<wire x1="-11.902" y1="15.698" x2="-11.648" y2="15.952" width="0.127" layer="21" curve="-90"/>
<wire x1="11.902" y1="15.952" x2="12.41" y2="15.444" width="0.127" layer="21" curve="-90"/>
<wire x1="-12.41" y1="15.444" x2="-11.902" y2="15.952" width="0.127" layer="21" curve="-90"/>
<wire x1="-11.902" y1="15.952" x2="11.902" y2="15.952" width="0.127" layer="21"/>
<wire x1="-9.87" y1="-5.695" x2="-11.14" y2="-6.965" width="0.508" layer="21"/>
<wire x1="-11.14" y1="-6.965" x2="-8.6" y2="-6.965" width="0.508" layer="21"/>
<wire x1="-8.6" y1="-6.965" x2="-9.87" y2="-5.695" width="0.508" layer="21"/>
<wire x1="-10.505" y1="-6.457" x2="-9.235" y2="-6.457" width="0.508" layer="21"/>
<wire x1="-9.87" y1="-5.822" x2="-9.87" y2="-6.203" width="0.508" layer="21"/>
<circle x="16.811" y="-1.45" radius="1.8034" width="0.127" layer="21"/>
<circle x="-16.811" y="-1.45" radius="1.8034" width="0.127" layer="21"/>
<pad name="FGND1" x="16.811" y="-1.45" drill="3.1" diameter="4"/>
<pad name="FGND2" x="-16.811" y="-1.45" drill="3.1" diameter="4"/>
<pad name="12" x="0" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="15" x="8.31" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="14" x="5.54" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="13" x="2.77" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="11" x="-2.77" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="10" x="-5.54" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="9" x="-8.31" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="8" x="9.695" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="7" x="6.925" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="6" x="4.155" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="5" x="1.385" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="4" x="-1.385" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="3" x="-4.155" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="2" x="-6.925" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="1" x="-9.695" y="-2.84" drill="1.016" diameter="1.5748" shape="square"/>
<text x="-11.59" y="-3.155" size="1.27" layer="21">1</text>
<text x="10.955" y="-3.155" size="1.27" layer="21">8</text>
<text x="10.323" y="-0.635" size="1.27" layer="21">15</text>
<text x="-11.732" y="-0.635" size="1.27" layer="21">9</text>
<text x="-2.83" y="-7.6" size="1.778" layer="27">&gt;VALUE</text>
<text x="-20.03" y="-7.6" size="1.778" layer="25">&gt;NAME</text>
</package>
<package name="107-151">
<wire x1="19.395" y1="9.875" x2="19.395" y2="9.494" width="0.127" layer="21"/>
<wire x1="-19.395" y1="-5.06" x2="-19.097" y2="-5.06" width="0.127" layer="21"/>
<wire x1="19.395" y1="3.83" x2="19.395" y2="-5.06" width="0.127" layer="21"/>
<wire x1="12.537" y1="3.83" x2="12.537" y2="-5.06" width="0.127" layer="21"/>
<wire x1="12.537" y1="-5.06" x2="13.299" y2="-5.06" width="0.127" layer="21"/>
<wire x1="19.395" y1="3.83" x2="13.299" y2="3.83" width="0.127" layer="21"/>
<wire x1="13.299" y1="3.83" x2="13.299" y2="-5.06" width="0.127" layer="21"/>
<wire x1="13.299" y1="3.83" x2="12.537" y2="3.83" width="0.127" layer="21"/>
<wire x1="-12.537" y1="3.83" x2="-12.537" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-12.537" y1="-5.06" x2="12.537" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-12.537" y1="3.83" x2="-13.299" y2="3.83" width="0.127" layer="21"/>
<wire x1="-19.395" y1="-5.06" x2="-19.395" y2="3.83" width="0.127" layer="21"/>
<wire x1="-19.395" y1="3.83" x2="-19.395" y2="7.589" width="0.127" layer="21"/>
<wire x1="-13.299" y1="3.83" x2="-13.299" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-13.299" y1="3.83" x2="-19.395" y2="3.83" width="0.127" layer="21"/>
<wire x1="-13.299" y1="-5.06" x2="-12.537" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-19.395" y1="9.494" x2="19.395" y2="9.494" width="0.127" layer="21"/>
<wire x1="19.395" y1="9.494" x2="19.395" y2="7.589" width="0.127" layer="21"/>
<wire x1="-19.395" y1="9.494" x2="-19.395" y2="9.875" width="0.127" layer="21"/>
<wire x1="-19.395" y1="7.589" x2="19.395" y2="7.589" width="0.127" layer="21"/>
<wire x1="-19.395" y1="7.589" x2="-19.395" y2="9.494" width="0.127" layer="21"/>
<wire x1="19.395" y1="7.589" x2="19.395" y2="3.83" width="0.127" layer="21"/>
<wire x1="19.395" y1="9.875" x2="12.918" y2="9.875" width="0.127" layer="21"/>
<wire x1="12.918" y1="9.875" x2="12.41" y2="10.383" width="0.127" layer="21"/>
<wire x1="12.918" y1="9.875" x2="-12.918" y2="9.875" width="0.127" layer="21"/>
<wire x1="12.41" y1="10.383" x2="12.41" y2="15.444" width="0.127" layer="21"/>
<wire x1="-12.41" y1="15.444" x2="-12.41" y2="10.383" width="0.127" layer="21"/>
<wire x1="-12.41" y1="10.383" x2="-12.918" y2="9.875" width="0.127" layer="21"/>
<wire x1="-12.918" y1="9.875" x2="-19.395" y2="9.875" width="0.127" layer="21"/>
<wire x1="11.902" y1="15.698" x2="11.902" y2="10.383" width="0.127" layer="21"/>
<wire x1="13.299" y1="-5.06" x2="14.525" y2="-5.06" width="0.127" layer="21"/>
<wire x1="14.525" y1="0.02" x2="19.097" y2="0.02" width="0.127" layer="21" curve="-180"/>
<wire x1="19.097" y1="-5.06" x2="19.097" y2="0.02" width="0.127" layer="21"/>
<wire x1="19.097" y1="-5.06" x2="19.395" y2="-5.06" width="0.127" layer="21"/>
<wire x1="14.525" y1="-5.06" x2="14.525" y2="0.02" width="0.127" layer="21"/>
<wire x1="14.525" y1="-5.06" x2="19.097" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-19.097" y1="0.02" x2="-14.525" y2="0.02" width="0.127" layer="21" curve="-180"/>
<wire x1="-14.525" y1="-5.06" x2="-14.525" y2="0.02" width="0.127" layer="21"/>
<wire x1="-14.525" y1="-5.06" x2="-13.299" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-19.097" y1="-5.06" x2="-19.097" y2="0.02" width="0.127" layer="21"/>
<wire x1="-19.097" y1="-5.06" x2="-14.525" y2="-5.06" width="0.127" layer="21"/>
<wire x1="-11.902" y1="15.698" x2="-11.902" y2="10.383" width="0.127" layer="21"/>
<wire x1="11.648" y1="15.952" x2="11.902" y2="15.698" width="0.127" layer="21" curve="-90"/>
<wire x1="-11.902" y1="15.698" x2="-11.648" y2="15.952" width="0.127" layer="21" curve="-90"/>
<wire x1="11.902" y1="15.952" x2="12.41" y2="15.444" width="0.127" layer="21" curve="-90"/>
<wire x1="-12.41" y1="15.444" x2="-11.902" y2="15.952" width="0.127" layer="21" curve="-90"/>
<wire x1="-11.902" y1="15.952" x2="11.902" y2="15.952" width="0.127" layer="21"/>
<wire x1="9.815" y1="-5.695" x2="8.545" y2="-6.965" width="0.508" layer="21"/>
<wire x1="8.545" y1="-6.965" x2="11.085" y2="-6.965" width="0.508" layer="21"/>
<wire x1="11.085" y1="-6.965" x2="9.815" y2="-5.695" width="0.508" layer="21"/>
<wire x1="9.18" y1="-6.457" x2="10.45" y2="-6.457" width="0.508" layer="21"/>
<wire x1="9.815" y1="-5.822" x2="9.815" y2="-6.203" width="0.508" layer="21"/>
<circle x="16.811" y="-1.45" radius="1.8034" width="0.127" layer="21"/>
<circle x="-16.811" y="-1.45" radius="1.8034" width="0.127" layer="21"/>
<pad name="FGND1" x="16.811" y="-1.45" drill="3.1" diameter="4"/>
<pad name="FGND2" x="-16.811" y="-1.45" drill="3.1" diameter="4"/>
<pad name="12" x="0" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="9" x="8.31" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="10" x="5.54" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="11" x="2.77" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="13" x="-2.77" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="14" x="-5.54" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="15" x="-8.31" y="0" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="1" x="9.695" y="-2.84" drill="1.016" diameter="1.5748" shape="square"/>
<pad name="2" x="6.925" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="3" x="4.155" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="4" x="1.385" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="5" x="-1.385" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="6" x="-4.155" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="7" x="-6.925" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<pad name="8" x="-9.695" y="-2.84" drill="1.016" diameter="1.5748" shape="octagon"/>
<text x="11.27" y="-3.155" size="1.27" layer="21">1</text>
<text x="-11.905" y="-3.155" size="1.27" layer="21">8</text>
<text x="-11.902" y="-0.635" size="1.27" layer="21">15</text>
<text x="9.858" y="-0.635" size="1.27" layer="21">9</text>
<text x="-2.83" y="-7.6" size="1.778" layer="27">&gt;VALUE</text>
<text x="-20.03" y="-7.6" size="1.778" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="SUBD15">
<wire x1="-4.064" y1="19.6312" x2="-2.5226" y2="20.872" width="0.254" layer="94" curve="-102.324066" cap="flat"/>
<wire x1="-2.5226" y1="20.8718" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="3.0654" y2="19.6494" width="0.254" layer="94"/>
<wire x1="3.0654" y1="19.6495" x2="4.0657" y2="18.4088" width="0.254" layer="94" curve="-77.60071" cap="flat"/>
<wire x1="4.064" y1="-0.6288" x2="4.064" y2="18.4088" width="0.254" layer="94"/>
<wire x1="3.0654" y1="-19.6494" x2="4.064" y2="-18.4088" width="0.254" layer="94" curve="77.657889"/>
<wire x1="-4.064" y1="-19.6312" x2="-4.064" y2="19.6312" width="0.254" layer="94"/>
<wire x1="-2.5226" y1="-20.8718" x2="0" y2="-20.32" width="0.254" layer="94"/>
<wire x1="0" y1="-20.32" x2="3.0654" y2="-19.6494" width="0.254" layer="94"/>
<wire x1="-4.064" y1="-19.6312" x2="-2.5226" y2="-20.8719" width="0.254" layer="94" curve="102.337599" cap="flat"/>
<wire x1="4.064" y1="-0.635" x2="4.064" y2="-18.415" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-17.78" x2="-2.032" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="0.508" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="-2.032" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="0.508" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.032" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="0.508" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.032" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="0.508" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-2.032" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="0.508" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="12.7" x2="-2.032" y2="12.7" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="15.24" x2="0.508" y2="15.24" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="17.78" x2="-2.032" y2="17.78" width="0.1524" layer="94"/>
<circle x="1.27" y="15.24" radius="0.762" width="0.254" layer="94"/>
<circle x="-1.27" y="17.78" radius="0.762" width="0.254" layer="94"/>
<circle x="-1.27" y="12.7" radius="0.762" width="0.254" layer="94"/>
<circle x="1.27" y="10.16" radius="0.762" width="0.254" layer="94"/>
<circle x="-1.27" y="7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="1.27" y="5.08" radius="0.762" width="0.254" layer="94"/>
<circle x="-1.27" y="2.54" radius="0.762" width="0.254" layer="94"/>
<circle x="1.27" y="0" radius="0.762" width="0.254" layer="94"/>
<circle x="-1.27" y="-2.54" radius="0.762" width="0.254" layer="94"/>
<circle x="-1.27" y="-7.62" radius="0.762" width="0.254" layer="94"/>
<circle x="1.27" y="-5.08" radius="0.762" width="0.254" layer="94"/>
<circle x="-1.27" y="-12.7" radius="0.762" width="0.254" layer="94"/>
<circle x="1.27" y="-10.16" radius="0.762" width="0.254" layer="94"/>
<circle x="1.27" y="-15.24" radius="0.762" width="0.254" layer="94"/>
<circle x="-1.27" y="-17.78" radius="0.762" width="0.254" layer="94"/>
<text x="-4.445" y="-23.495" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="21.59" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="17.78" visible="pad" length="middle" direction="pas"/>
<pin name="2" x="-7.62" y="12.7" visible="pad" length="middle" direction="pas"/>
<pin name="3" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas"/>
<pin name="4" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="6" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas"/>
<pin name="7" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas"/>
<pin name="8" x="-7.62" y="-17.78" visible="pad" length="middle" direction="pas"/>
<pin name="9" x="-7.62" y="15.24" visible="pad" length="middle" direction="pas"/>
<pin name="10" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas"/>
<pin name="11" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas"/>
<pin name="12" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="13" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas"/>
<pin name="14" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas"/>
<pin name="15" x="-7.62" y="-15.24" visible="pad" length="middle" direction="pas"/>
</symbol>
<symbol name="FGND">
<pin name="FGND" x="0" y="-7.62" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WUP107-152" prefix="CON">
<gates>
<gate name="G$1" symbol="SUBD15" x="0" y="0"/>
<gate name="G$2" symbol="FGND" x="7.62" y="-10.16"/>
<gate name="G$3" symbol="FGND" x="10.16" y="-10.16"/>
</gates>
<devices>
<device name="" package="107-152">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$2" pin="FGND" pad="FGND1"/>
<connect gate="G$3" pin="FGND" pad="FGND2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WUP107-151" prefix="CON">
<gates>
<gate name="G$1" symbol="SUBD15" x="0" y="0"/>
<gate name="G$2" symbol="FGND" x="-48.26" y="-27.94"/>
<gate name="G$3" symbol="FGND" x="-43.18" y="-27.94"/>
</gates>
<devices>
<device name="" package="107-151">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$2" pin="FGND" pad="FGND1"/>
<connect gate="G$3" pin="FGND" pad="FGND2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MYDISCR">
<packages>
<package name="SMB">
<wire x1="0" y1="-1.8" x2="4.6" y2="-1.8" width="0.127" layer="21"/>
<wire x1="4.6" y1="-1.8" x2="4.6" y2="1.8" width="0.127" layer="27"/>
<wire x1="4.6" y1="1.8" x2="0" y2="1.8" width="0.127" layer="21"/>
<wire x1="0" y1="1.8" x2="0" y2="-1.8" width="0.127" layer="27"/>
<wire x1="0" y1="1.8" x2="0" y2="1.4" width="0.127" layer="21"/>
<wire x1="0" y1="1.4" x2="1.408" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.408" y1="1.4" x2="1.408" y2="-1.4" width="0.127" layer="21"/>
<wire x1="1.408" y1="-1.4" x2="0" y2="-1.4" width="0.127" layer="21"/>
<wire x1="0" y1="-1.4" x2="0" y2="-1.8" width="0.127" layer="21"/>
<wire x1="4.6" y1="-1.8" x2="4.6" y2="-1.4" width="0.127" layer="21"/>
<wire x1="4.6" y1="1.4" x2="4.6" y2="1.8" width="0.127" layer="21"/>
<smd name="K" x="0.2" y="0" dx="1.8" dy="2.5" layer="1"/>
<smd name="A" x="4.4" y="0" dx="1.8" dy="2.5" layer="1"/>
<text x="0" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.1" y="-3.8" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.7" y1="-1.3" x2="0" y2="1.3" layer="27"/>
<rectangle x1="4.6" y1="-1.3" x2="5.3" y2="1.3" layer="27"/>
</package>
<package name="SOT23/32">
<wire x1="-0.8824" y1="0.821" x2="2.0132" y2="0.821" width="0.127" layer="21"/>
<wire x1="2.0132" y1="0.821" x2="2.0132" y2="0.059" width="0.127" layer="21"/>
<wire x1="2.0132" y1="0.059" x2="-0.8824" y2="0.059" width="0.127" layer="21"/>
<wire x1="-0.8824" y1="0.059" x2="-0.8824" y2="0.821" width="0.127" layer="21"/>
<smd name="1" x="1.4306" y="1.456" dx="1" dy="0.9" layer="1"/>
<smd name="2" x="-0.2998" y="1.456" dx="1" dy="0.9" layer="1"/>
<smd name="3" x="0.5654" y="-0.476" dx="0.8" dy="1" layer="1"/>
<text x="-0.857" y="-2.482" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-0.857" y="2.142" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="1.3274" y1="0.8718" x2="1.7084" y2="1.3798" layer="51"/>
<rectangle x1="-0.603" y1="0.8718" x2="-0.222" y2="1.3798" layer="51"/>
<rectangle x1="0.3622" y1="-0.4998" x2="0.7432" y2="0.0082" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SCHOTTKY">
<wire x1="-2.54" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.3716" y2="0" width="0.1524" layer="94"/>
<wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.905" x2="2.286" y2="1.905" width="0.254" layer="94"/>
<wire x1="2.286" y1="1.905" x2="2.286" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.397" y1="-1.905" x2="0.508" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0.508" y1="-1.905" x2="0.508" y2="-1.016" width="0.254" layer="94"/>
<text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MBRS130T3" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BAT54" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23/32">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MYMURATA">
<packages>
<package name="BLA31">
<wire x1="-1.5" y1="-0.8" x2="-1.6" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.8" x2="-1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.8" x2="1.6" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.8" x2="1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="0.9" y1="-0.8" x2="0.7" y2="-0.8" width="0.127" layer="21"/>
<wire x1="0.9" y1="0.8" x2="0.7" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.1" y1="-0.8" x2="-0.1" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-0.7" y1="-0.8" x2="-0.9" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-0.9" y1="0.8" x2="-0.7" y2="0.8" width="0.127" layer="21"/>
<wire x1="-0.1" y1="0.8" x2="0.1" y2="0.8" width="0.127" layer="21"/>
<smd name="1" x="-1.2" y="-0.8" dx="0.45" dy="0.8" layer="1"/>
<smd name="2" x="-0.4" y="-0.8" dx="0.45" dy="0.8" layer="1"/>
<smd name="3" x="0.4" y="-0.8" dx="0.45" dy="0.8" layer="1"/>
<smd name="4" x="1.2" y="-0.8" dx="0.45" dy="0.8" layer="1"/>
<smd name="5" x="1.2" y="0.8" dx="0.45" dy="0.8" layer="1"/>
<smd name="6" x="0.4" y="0.8" dx="0.45" dy="0.8" layer="1"/>
<smd name="7" x="-0.4" y="0.8" dx="0.45" dy="0.8" layer="1"/>
<smd name="8" x="-1.2" y="0.8" dx="0.45" dy="0.8" layer="1"/>
<text x="-1.497" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.497" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-1.4" y1="0.4" x2="-1" y2="1" layer="27"/>
<rectangle x1="-0.6" y1="0.4" x2="-0.2" y2="1" layer="27"/>
<rectangle x1="0.2" y1="0.4" x2="0.6" y2="1" layer="27"/>
<rectangle x1="1" y1="0.4" x2="1.4" y2="1" layer="27"/>
<rectangle x1="1" y1="-1" x2="1.4" y2="-0.4" layer="27"/>
<rectangle x1="0.2" y1="-1" x2="0.6" y2="-0.4" layer="27"/>
<rectangle x1="-0.6" y1="-1" x2="-0.2" y2="-0.4" layer="27"/>
<rectangle x1="-1.4" y1="-1" x2="-1" y2="-0.4" layer="27"/>
</package>
</packages>
<symbols>
<symbol name="L">
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-1.27" x2="2.54" y2="1.27" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BLA31" prefix="L" uservalue="yes">
<gates>
<gate name="A" symbol="L" x="-5.08" y="15.24" swaplevel="1"/>
<gate name="B" symbol="L" x="-5.08" y="5.08" swaplevel="1"/>
<gate name="C" symbol="L" x="-5.08" y="-5.08" swaplevel="1"/>
<gate name="D" symbol="L" x="-5.08" y="-15.24" swaplevel="1"/>
</gates>
<devices>
<device name="" package="BLA31">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MYTI">
<packages>
<package name="SO-8_POWERPAD">
<wire x1="-2.3622" y1="-1.9812" x2="2.3622" y2="-1.9812" width="0.127" layer="21"/>
<wire x1="2.3622" y1="-1.9812" x2="2.3622" y2="1.9812" width="0.127" layer="21"/>
<wire x1="2.3622" y1="1.9812" x2="-2.3622" y2="1.9812" width="0.127" layer="21"/>
<wire x1="-2.3622" y1="1.9812" x2="-2.3622" y2="0.762" width="0.127" layer="21"/>
<wire x1="-2.3622" y1="0.762" x2="-1.6002" y2="0.762" width="0.127" layer="21"/>
<wire x1="-1.6002" y1="0.762" x2="-1.6002" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.6002" y1="-0.762" x2="-2.3622" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-2.3622" y1="-0.762" x2="-2.3622" y2="-1.9812" width="0.127" layer="21"/>
<smd name="1" x="-1.905" y="-2.8194" dx="0.6604" dy="1.6002" layer="1"/>
<smd name="8" x="-1.9304" y="2.8194" dx="0.6604" dy="1.6002" layer="1"/>
<smd name="2" x="-0.635" y="-2.8194" dx="0.6604" dy="1.6002" layer="1"/>
<smd name="3" x="0.635" y="-2.8194" dx="0.6604" dy="1.6002" layer="1"/>
<smd name="7" x="-0.6604" y="2.8194" dx="0.6604" dy="1.6002" layer="1"/>
<smd name="6" x="0.6096" y="2.8194" dx="0.6604" dy="1.6002" layer="1"/>
<smd name="4" x="1.905" y="-2.8194" dx="0.6604" dy="1.6002" layer="1"/>
<smd name="5" x="1.8796" y="2.8194" dx="0.6604" dy="1.6002" layer="1"/>
<text x="4.064" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-2.794" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-2.159" y1="-2.8956" x2="-1.651" y2="-2.032" layer="27"/>
<rectangle x1="-0.889" y1="-2.8956" x2="-0.381" y2="-2.032" layer="27"/>
<rectangle x1="0.381" y1="-2.8956" x2="0.889" y2="-2.032" layer="27"/>
<rectangle x1="1.651" y1="-2.8956" x2="2.159" y2="-2.032" layer="27"/>
<rectangle x1="1.651" y1="2.032" x2="2.159" y2="2.8956" layer="27"/>
<rectangle x1="0.381" y1="2.032" x2="0.889" y2="2.8956" layer="27"/>
<rectangle x1="-0.889" y1="2.032" x2="-0.381" y2="2.8956" layer="27"/>
<rectangle x1="-2.159" y1="2.032" x2="-1.651" y2="2.8956" layer="27"/>
<smd name="9" x="0" y="0" dx="3" dy="2" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="TPS54540">
<wire x1="0" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="15.24" y2="27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="27.94" x2="0" y2="27.94" width="0.254" layer="94"/>
<wire x1="0" y1="27.94" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="VIN" x="-5.08" y="25.4" length="middle" direction="sup"/>
<pin name="GND" x="7.62" y="-5.08" length="middle" direction="sup" rot="R90"/>
<pin name="BOOT" x="20.32" y="25.4" length="middle" direction="sup" rot="R180"/>
<pin name="SW" x="20.32" y="17.78" length="middle" direction="sup" rot="R180"/>
<pin name="FB" x="20.32" y="10.16" length="middle" direction="sup" rot="R180"/>
<pin name="EN" x="-5.08" y="20.32" length="middle" direction="sup"/>
<pin name="COMP" x="-5.08" y="15.24" length="middle" direction="sup"/>
<pin name="RT" x="-5.08" y="10.16" length="middle" direction="sup"/>
<text x="0" y="30.48" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="28.448" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TPS54540" prefix="IC">
<gates>
<gate name="G$1" symbol="TPS54540" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO-8_POWERPAD">
<connects>
<connect gate="G$1" pin="BOOT" pad="1"/>
<connect gate="G$1" pin="COMP" pad="6"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND" pad="7 9"/>
<connect gate="G$1" pin="RT" pad="4"/>
<connect gate="G$1" pin="SW" pad="8"/>
<connect gate="G$1" pin="VIN" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MYACL">
<packages>
<package name="SSOP48DL">
<wire x1="-7.6962" y1="-4.1656" x2="8.1788" y2="-4.1656" width="0.127" layer="21"/>
<wire x1="8.1788" y1="-4.1656" x2="8.1788" y2="3.3274" width="0.127" layer="21"/>
<wire x1="8.1788" y1="3.3274" x2="-7.6962" y2="3.3274" width="0.127" layer="21"/>
<wire x1="-7.6962" y1="-4.1656" x2="-7.6962" y2="3.3274" width="0.127" layer="21"/>
<wire x1="-7.4422" y1="-3.9116" x2="7.9248" y2="-3.9116" width="0.127" layer="21"/>
<wire x1="7.9248" y1="3.0734" x2="7.9248" y2="-3.9116" width="0.127" layer="21"/>
<wire x1="7.9248" y1="3.0734" x2="-7.4422" y2="3.0734" width="0.127" layer="21"/>
<wire x1="-7.4422" y1="-3.9116" x2="-7.4422" y2="3.0734" width="0.127" layer="21"/>
<circle x="-6.1722" y="-2.7686" radius="0.8128" width="0.127" layer="21"/>
<smd name="1" x="-7.0612" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="2" x="-6.4262" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="3" x="-5.7912" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="4" x="-5.1562" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="5" x="-4.5212" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="6" x="-3.8862" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="7" x="-3.2512" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="8" x="-2.6162" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="9" x="-1.9812" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="10" x="-1.3462" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="11" x="-0.7112" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="12" x="-0.0762" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="13" x="0.5588" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="14" x="1.1938" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="15" x="1.8288" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="16" x="2.4638" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="17" x="3.0988" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="18" x="3.7338" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="19" x="4.3688" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="20" x="5.0038" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="21" x="5.6388" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="22" x="6.2738" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="23" x="6.9088" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="24" x="7.5438" y="-4.9022" dx="0.3048" dy="1.397" layer="1"/>
<smd name="25" x="7.5438" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="26" x="6.9088" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="27" x="6.2738" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="28" x="5.6388" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="29" x="5.0038" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="30" x="4.3688" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="31" x="3.7338" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="32" x="3.0988" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="33" x="2.4638" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="34" x="1.8288" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="35" x="1.1938" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="36" x="0.5588" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="37" x="-0.0762" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="38" x="-0.7112" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="39" x="-1.3462" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="40" x="-1.9812" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="41" x="-2.6162" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="42" x="-3.2512" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="43" x="-3.8862" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="44" x="-4.5212" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="45" x="-5.1562" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="46" x="-5.7912" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="47" x="-6.4262" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<smd name="48" x="-7.0612" y="4.064" dx="0.3048" dy="1.397" layer="1"/>
<text x="-2.9972" y="0.508" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.9718" y="-1.6764" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.1628" y1="3.3782" x2="-6.9596" y2="4.2164" layer="27"/>
<rectangle x1="-6.5278" y1="3.3782" x2="-6.3246" y2="4.2164" layer="27"/>
<rectangle x1="-5.8928" y1="3.3782" x2="-5.6896" y2="4.2164" layer="27"/>
<rectangle x1="-5.2578" y1="3.3782" x2="-5.0546" y2="4.2164" layer="27"/>
<rectangle x1="-4.6228" y1="3.3782" x2="-4.4196" y2="4.2164" layer="27"/>
<rectangle x1="-3.9878" y1="3.3782" x2="-3.7846" y2="4.2164" layer="27"/>
<rectangle x1="-3.3528" y1="3.3782" x2="-3.1496" y2="4.2164" layer="27"/>
<rectangle x1="-2.7178" y1="3.3782" x2="-2.5146" y2="4.2164" layer="27"/>
<rectangle x1="-2.0828" y1="3.3782" x2="-1.8796" y2="4.2164" layer="27"/>
<rectangle x1="-1.4478" y1="3.3782" x2="-1.2446" y2="4.2164" layer="27"/>
<rectangle x1="-0.8128" y1="3.3782" x2="-0.6096" y2="4.2164" layer="27"/>
<rectangle x1="-0.1778" y1="3.3782" x2="0.0254" y2="4.2164" layer="27"/>
<rectangle x1="0.4572" y1="3.3782" x2="0.6604" y2="4.2164" layer="27"/>
<rectangle x1="1.0922" y1="3.3782" x2="1.2954" y2="4.2164" layer="27"/>
<rectangle x1="1.7272" y1="3.3782" x2="1.9304" y2="4.2164" layer="27"/>
<rectangle x1="2.3622" y1="3.3782" x2="2.5654" y2="4.2164" layer="27"/>
<rectangle x1="2.9972" y1="3.3782" x2="3.2004" y2="4.2164" layer="27"/>
<rectangle x1="3.6322" y1="3.3782" x2="3.8354" y2="4.2164" layer="27"/>
<rectangle x1="4.2672" y1="3.3782" x2="4.4704" y2="4.2164" layer="27"/>
<rectangle x1="4.9022" y1="3.3782" x2="5.1054" y2="4.2164" layer="27"/>
<rectangle x1="5.5372" y1="3.3782" x2="5.7404" y2="4.2164" layer="27"/>
<rectangle x1="6.1722" y1="3.3782" x2="6.3754" y2="4.2164" layer="27"/>
<rectangle x1="6.8072" y1="3.3782" x2="7.0104" y2="4.2164" layer="27"/>
<rectangle x1="7.4422" y1="3.3782" x2="7.6454" y2="4.2164" layer="27"/>
<rectangle x1="7.4422" y1="-5.0546" x2="7.6454" y2="-4.2164" layer="27"/>
<rectangle x1="6.8072" y1="-5.0546" x2="7.0104" y2="-4.2164" layer="27"/>
<rectangle x1="6.1722" y1="-5.0546" x2="6.3754" y2="-4.2164" layer="27"/>
<rectangle x1="5.5372" y1="-5.0546" x2="5.7404" y2="-4.2164" layer="27"/>
<rectangle x1="4.9022" y1="-5.0546" x2="5.1054" y2="-4.2164" layer="27"/>
<rectangle x1="4.2672" y1="-5.0546" x2="4.4704" y2="-4.2164" layer="27"/>
<rectangle x1="4.9022" y1="-5.0546" x2="5.1054" y2="-4.2164" layer="27"/>
<rectangle x1="3.6322" y1="-5.0546" x2="3.8354" y2="-4.2164" layer="27"/>
<rectangle x1="2.9972" y1="-5.0546" x2="3.2004" y2="-4.2164" layer="27"/>
<rectangle x1="2.3622" y1="-5.0546" x2="2.5654" y2="-4.2164" layer="27"/>
<rectangle x1="1.7272" y1="-5.0546" x2="1.9304" y2="-4.2164" layer="27"/>
<rectangle x1="1.0922" y1="-5.0546" x2="1.2954" y2="-4.2164" layer="27"/>
<rectangle x1="0.4572" y1="-5.0546" x2="0.6604" y2="-4.2164" layer="27"/>
<rectangle x1="0.4572" y1="-5.0546" x2="0.6604" y2="-4.2164" layer="27"/>
<rectangle x1="-0.1778" y1="-5.0546" x2="0.0254" y2="-4.2164" layer="27"/>
<rectangle x1="-0.8128" y1="-5.0546" x2="-0.6096" y2="-4.2164" layer="27"/>
<rectangle x1="-1.4478" y1="-5.0546" x2="-1.2446" y2="-4.2164" layer="27"/>
<rectangle x1="-1.4478" y1="-5.0546" x2="-1.2446" y2="-4.2164" layer="27"/>
<rectangle x1="-2.0828" y1="-5.0546" x2="-1.8796" y2="-4.2164" layer="27"/>
<rectangle x1="-2.7178" y1="-5.0546" x2="-2.5146" y2="-4.2164" layer="27"/>
<rectangle x1="-3.3528" y1="-5.0546" x2="-3.1496" y2="-4.2164" layer="27"/>
<rectangle x1="-3.3528" y1="-5.0546" x2="-3.1496" y2="-4.2164" layer="27"/>
<rectangle x1="-3.9878" y1="-5.0546" x2="-3.7846" y2="-4.2164" layer="27"/>
<rectangle x1="-4.6228" y1="-5.0546" x2="-4.4196" y2="-4.2164" layer="27"/>
<rectangle x1="-5.2578" y1="-5.0546" x2="-5.0546" y2="-4.2164" layer="27"/>
<rectangle x1="-5.8928" y1="-5.0546" x2="-5.6896" y2="-4.2164" layer="27"/>
<rectangle x1="-5.8928" y1="-5.0546" x2="-5.6896" y2="-4.2164" layer="27"/>
<rectangle x1="-6.5278" y1="-5.0546" x2="-6.3246" y2="-4.2164" layer="27"/>
<rectangle x1="-7.1628" y1="-5.0546" x2="-6.9596" y2="-4.2164" layer="27"/>
</package>
</packages>
<symbols>
<symbol name="7416245">
<wire x1="-10.16" y1="27.94" x2="-10.16" y2="-27.94" width="0.254" layer="94"/>
<wire x1="7.62" y1="-27.94" x2="-10.16" y2="-27.94" width="0.254" layer="94"/>
<wire x1="7.62" y1="-27.94" x2="7.62" y2="27.94" width="0.254" layer="94"/>
<wire x1="-10.16" y1="27.94" x2="7.62" y2="27.94" width="0.254" layer="94"/>
<text x="-10.16" y="30.48" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1G" x="-15.24" y="-17.78" length="middle" direction="in" function="dot"/>
<pin name="1DIR" x="-15.24" y="-20.32" length="middle" direction="in" function="dot"/>
<pin name="2G" x="-15.24" y="-22.86" length="middle" direction="in" function="dot"/>
<pin name="2DIR" x="-15.24" y="-25.4" length="middle" direction="in" function="dot"/>
<pin name="1B1" x="12.7" y="25.4" length="middle" rot="R180"/>
<pin name="1B2" x="12.7" y="22.86" length="middle" rot="R180"/>
<pin name="1B3" x="12.7" y="20.32" length="middle" rot="R180"/>
<pin name="1B4" x="12.7" y="17.78" length="middle" rot="R180"/>
<pin name="1B5" x="12.7" y="15.24" length="middle" rot="R180"/>
<pin name="1B6" x="12.7" y="12.7" length="middle" rot="R180"/>
<pin name="1B7" x="12.7" y="10.16" length="middle" rot="R180"/>
<pin name="1B8" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="2B1" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="2B2" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="2B3" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="2B4" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="2B5" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="2B6" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="2B7" x="12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="2B8" x="12.7" y="-12.7" length="middle" rot="R180"/>
<pin name="1A1" x="-15.24" y="25.4" length="middle"/>
<pin name="1A2" x="-15.24" y="22.86" length="middle"/>
<pin name="1A3" x="-15.24" y="20.32" length="middle"/>
<pin name="1A4" x="-15.24" y="17.78" length="middle"/>
<pin name="1A5" x="-15.24" y="15.24" length="middle"/>
<pin name="1A6" x="-15.24" y="12.7" length="middle"/>
<pin name="1A7" x="-15.24" y="10.16" length="middle"/>
<pin name="1A8" x="-15.24" y="7.62" length="middle"/>
<pin name="2A1" x="-15.24" y="5.08" length="middle"/>
<pin name="2A2" x="-15.24" y="2.54" length="middle"/>
<pin name="2A3" x="-15.24" y="0" length="middle"/>
<pin name="2A4" x="-15.24" y="-2.54" length="middle"/>
<pin name="2A5" x="-15.24" y="-5.08" length="middle"/>
<pin name="2A6" x="-15.24" y="-7.62" length="middle"/>
<pin name="2A7" x="-15.24" y="-10.16" length="middle"/>
<pin name="2A8" x="-15.24" y="-12.7" length="middle"/>
</symbol>
<symbol name="4PWR8GND">
<text x="0" y="-1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="-19.05" y="-12.7" size="1.27" layer="95">GND</text>
<text x="-13.97" y="-12.7" size="1.27" layer="95">GND</text>
<text x="-8.89" y="-12.7" size="1.27" layer="95">GND</text>
<text x="-3.81" y="-12.7" size="1.27" layer="95">GND</text>
<text x="-8.89" y="12.7" size="1.27" layer="95">VCC</text>
<text x="-3.81" y="12.7" size="1.27" layer="95">VCC</text>
<text x="1.27" y="12.7" size="1.27" layer="95">VCC</text>
<text x="6.35" y="12.7" size="1.27" layer="95">VCC</text>
<text x="1.27" y="-12.7" size="1.27" layer="95">GND</text>
<text x="6.35" y="-12.7" size="1.27" layer="95">GND</text>
<text x="11.43" y="-12.7" size="1.27" layer="95">GND</text>
<text x="16.51" y="-12.7" size="1.27" layer="95">GND</text>
<pin name="VCC@1" x="-7.62" y="10.16" visible="pad" direction="pwr" rot="R270"/>
<pin name="VCC@2" x="-2.54" y="10.16" visible="pad" direction="pwr" rot="R270"/>
<pin name="GND@4" x="-2.54" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
<pin name="GND@3" x="-7.62" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
<pin name="GND@2" x="-12.7" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
<pin name="GND@1" x="-17.78" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
<pin name="VCC@3" x="2.54" y="10.16" visible="pad" direction="pwr" rot="R270"/>
<pin name="VCC@4" x="7.62" y="10.16" visible="pad" direction="pwr" rot="R270"/>
<pin name="GND@5" x="2.54" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
<pin name="GND@6" x="7.62" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
<pin name="GND@7" x="12.7" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
<pin name="GND@8" x="17.78" y="-10.16" visible="pad" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="74AC16245" prefix="IC" uservalue="yes">
<gates>
<gate name="G$1" symbol="7416245" x="0" y="-2.54"/>
<gate name="P" symbol="4PWR8GND" x="-43.18" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="SSOP48DL">
<connects>
<connect gate="G$1" pin="1A1" pad="47"/>
<connect gate="G$1" pin="1A2" pad="46"/>
<connect gate="G$1" pin="1A3" pad="44"/>
<connect gate="G$1" pin="1A4" pad="43"/>
<connect gate="G$1" pin="1A5" pad="41"/>
<connect gate="G$1" pin="1A6" pad="40"/>
<connect gate="G$1" pin="1A7" pad="38"/>
<connect gate="G$1" pin="1A8" pad="37"/>
<connect gate="G$1" pin="1B1" pad="2"/>
<connect gate="G$1" pin="1B2" pad="3"/>
<connect gate="G$1" pin="1B3" pad="5"/>
<connect gate="G$1" pin="1B4" pad="6"/>
<connect gate="G$1" pin="1B5" pad="8"/>
<connect gate="G$1" pin="1B6" pad="9"/>
<connect gate="G$1" pin="1B7" pad="11"/>
<connect gate="G$1" pin="1B8" pad="12"/>
<connect gate="G$1" pin="1DIR" pad="1"/>
<connect gate="G$1" pin="1G" pad="48"/>
<connect gate="G$1" pin="2A1" pad="36"/>
<connect gate="G$1" pin="2A2" pad="35"/>
<connect gate="G$1" pin="2A3" pad="33"/>
<connect gate="G$1" pin="2A4" pad="32"/>
<connect gate="G$1" pin="2A5" pad="30"/>
<connect gate="G$1" pin="2A6" pad="29"/>
<connect gate="G$1" pin="2A7" pad="27"/>
<connect gate="G$1" pin="2A8" pad="26"/>
<connect gate="G$1" pin="2B1" pad="13"/>
<connect gate="G$1" pin="2B2" pad="14"/>
<connect gate="G$1" pin="2B3" pad="16"/>
<connect gate="G$1" pin="2B4" pad="17"/>
<connect gate="G$1" pin="2B5" pad="19"/>
<connect gate="G$1" pin="2B6" pad="20"/>
<connect gate="G$1" pin="2B7" pad="22"/>
<connect gate="G$1" pin="2B8" pad="23"/>
<connect gate="G$1" pin="2DIR" pad="24"/>
<connect gate="G$1" pin="2G" pad="25"/>
<connect gate="P" pin="GND@1" pad="4"/>
<connect gate="P" pin="GND@2" pad="10"/>
<connect gate="P" pin="GND@3" pad="15"/>
<connect gate="P" pin="GND@4" pad="21"/>
<connect gate="P" pin="GND@5" pad="28"/>
<connect gate="P" pin="GND@6" pad="34"/>
<connect gate="P" pin="GND@7" pad="39"/>
<connect gate="P" pin="GND@8" pad="45"/>
<connect gate="P" pin="VCC@1" pad="7"/>
<connect gate="P" pin="VCC@2" pad="18"/>
<connect gate="P" pin="VCC@3" pad="31"/>
<connect gate="P" pin="VCC@4" pad="42"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jumper">
<description>&lt;b&gt;Jumpers&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SJ_2W">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="3.175" y1="-1.524" x2="-3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.524" x2="3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.429" y1="1.27" x2="-3.175" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.429" y1="-1.27" x2="-3.175" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="3.175" y1="-1.524" x2="3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="3.429" y1="-1.27" x2="3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.524" x2="3.175" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="0" x2="-3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0" y1="0.762" x2="0" y2="1.397" width="0.1524" layer="51"/>
<wire x1="0" y1="-1.397" x2="0" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="2.032" y1="0.127" x2="2.032" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="-2.032" y1="-0.127" x2="-2.032" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-2.54" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="0" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="3" x="2.54" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="-3.429" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.1001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-0.762" x2="0.508" y2="0.762" layer="51"/>
</package>
<package name="SJ_2">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="2.159" y1="-1.016" x2="-2.159" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="0.762" x2="-2.159" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="-0.762" x2="-2.159" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.016" x2="2.413" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-0.762" x2="-2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.016" x2="2.159" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0" x2="-2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="0" y1="0.762" x2="0" y2="1.016" width="0.1524" layer="51"/>
<wire x1="0" y1="-1.016" x2="0" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.016" y1="0.127" x2="1.016" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="-1.016" y1="-0.127" x2="-1.016" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-1.524" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="3" x="1.524" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-2.413" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.1001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-0.762" x2="0.508" y2="0.762" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SJ_2">
<wire x1="-0.635" y1="-1.397" x2="0.635" y2="-1.397" width="1.27" layer="94" curve="180" cap="flat"/>
<wire x1="-0.635" y1="1.397" x2="0.635" y2="1.397" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="1.27" y1="-0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<text x="2.54" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.635" x2="1.27" y2="0.635" layer="94"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="2" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SJ2W" prefix="SJ" uservalue="yes">
<description>SMD solder &lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="SJ_2" x="0" y="0"/>
</gates>
<devices>
<device name="W" package="SJ_2W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SJ_2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="led">
<description>&lt;b&gt;LEDs&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;br&gt;
Extended by Federico Battaglin &lt;author&gt;&amp;lt;federico.rd@fdpinternational.com&amp;gt;&lt;/author&gt; with DUOLED</description>
<packages>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KA-3528ASYC">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.1016" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.1016" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
</package>
<package name="SML0805">
<description>&lt;b&gt;SML0805-2CW-TR (0805 PROFILE)&lt;/b&gt; COOL WHITE&lt;p&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<wire x1="-0.95" y1="-0.55" x2="0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.55" x2="0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.55" x2="-0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.55" x2="-0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="-0.175" y1="-0.025" x2="0" y2="0.15" width="0.0634" layer="21"/>
<wire x1="0" y1="0.15" x2="0.15" y2="0" width="0.0634" layer="21"/>
<wire x1="0.15" y1="0" x2="-0.025" y2="-0.175" width="0.0634" layer="21"/>
<wire x1="-0.025" y1="-0.175" x2="-0.175" y2="-0.025" width="0.0634" layer="21"/>
<circle x="-0.275" y="0.4" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SML1206">
<description>&lt;b&gt;SML10XXKH-TR (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;SML10R3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10E3KH-TR&lt;/td&gt;&lt;td&gt;SUPER REDSUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10O3KH-TR&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PY3KH-TR&lt;/td&gt;&lt;td&gt;PURE YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10OY3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10AG3KH-TR&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10BG3KH-TR&lt;/td&gt;&lt;td&gt;BLUE GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PB1KH-TR&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10CW1KH-TR&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<wire x1="-1.5" y1="0.5" x2="-1.5" y2="-0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="1.5" y1="-0.5" x2="1.5" y2="0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<circle x="-0.725" y="0.525" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="0.4" x2="-1.15" y2="0.8" layer="51"/>
<rectangle x1="-1.6" y1="-0.8" x2="-1.15" y2="-0.4" layer="51"/>
<rectangle x1="-1.175" y1="-0.6" x2="-1" y2="-0.275" layer="51"/>
<rectangle x1="1.15" y1="-0.8" x2="1.6" y2="-0.4" layer="51" rot="R180"/>
<rectangle x1="1.15" y1="0.4" x2="1.6" y2="0.8" layer="51" rot="R180"/>
<rectangle x1="1" y1="0.275" x2="1.175" y2="0.6" layer="51" rot="R180"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
</package>
<package name="SML0603">
<description>&lt;b&gt;SML0603-XXX (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;AG3K&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;B1K&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R1K&lt;/td&gt;&lt;td&gt;SUPER RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R3K&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3K&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3KH&lt;/td&gt;&lt;td&gt;SOFT ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3KH&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3K&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;2CW&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.3" x2="-0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="0.45" y1="0.3" x2="0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.35" x2="0.2" y2="0.35" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="0.2" y2="-0.35" width="0.1016" layer="21"/>
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.175" x2="0" y2="0.4" layer="51"/>
<rectangle x1="-0.25" y1="0.175" x2="0" y2="0.4" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0805" package="SML0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML1206" package="SML1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0603" package="SML0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="myconn">
<packages>
<package name="AKL-169">
<wire x1="-2.5" y1="-8.9" x2="-2.5" y2="2.3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.3" x2="9.6" y2="2.3" width="0.127" layer="21"/>
<wire x1="9.6" y1="2.3" x2="9.6" y2="-8.9" width="0.127" layer="21"/>
<wire x1="9.6" y1="-8.9" x2="-2.5" y2="-8.9" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.5" diameter="2.54" shape="square"/>
<pad name="P$2" x="3.5" y="0" drill="1.5" diameter="2.54"/>
<pad name="P$3" x="7" y="0" drill="1.5" diameter="2.54"/>
<text x="-2.4" y="3" size="1.27" layer="21">&gt;NAME</text>
<text x="-3.3" y="-8.7" size="1.27" layer="21" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="3POL">
<wire x1="-5.08" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="15.24" width="0.254" layer="94"/>
<text x="-5.08" y="17.78" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3" x="5.08" y="2.54" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="5.08" y="7.62" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="1" x="5.08" y="12.7" visible="pin" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AKL-182" prefix="CON">
<description>AKL 182-3 Reichelt</description>
<gates>
<gate name="G$1" symbol="3POL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AKL-169">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="myON">
<packages>
<package name="TSOP-6">
<wire x1="-1.5" y1="-0.8" x2="-1.1" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.8" x2="1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.8" x2="-1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.8" x2="-1.5" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.4" x2="-1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.4" x2="-1.1" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.4" x2="-1.1" y2="-0.8" width="0.127" layer="21"/>
<smd name="1" x="-0.9" y="-1.3" dx="0.5" dy="0.9" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.5" dy="0.9" layer="1"/>
<smd name="3" x="0.9" y="-1.3" dx="0.5" dy="0.9" layer="1"/>
<smd name="4" x="0.9" y="1.3" dx="0.5" dy="0.9" layer="1"/>
<smd name="5" x="0" y="1.3" dx="0.5" dy="0.9" layer="1"/>
<smd name="6" x="-0.9" y="1.3" dx="0.5" dy="0.9" layer="1"/>
<text x="-1.7" y="-1.8" size="1.778" layer="25" rot="R90">&gt;NAME</text>
<text x="3.5" y="-1.8" size="1.778" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="MMQA">
<wire x1="0" y1="0" x2="15.24" y2="0" width="0.254" layer="94"/>
<wire x1="15.24" y1="0" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="C2" x="-5.08" y="2.54" length="middle"/>
<pin name="A@1" x="-5.08" y="7.62" length="middle"/>
<pin name="C1" x="-5.08" y="12.7" length="middle"/>
<pin name="C3" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="A@2" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="C4" x="20.32" y="12.7" length="middle" rot="R180"/>
<text x="0" y="17.78" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="15.494" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MMQA">
<gates>
<gate name="G$1" symbol="MMQA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSOP-6">
<connects>
<connect gate="G$1" pin="A@1" pad="2"/>
<connect gate="G$1" pin="A@2" pad="5"/>
<connect gate="G$1" pin="C1" pad="1"/>
<connect gate="G$1" pin="C2" pad="3"/>
<connect gate="G$1" pin="C3" pad="4"/>
<connect gate="G$1" pin="C4" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="mypassive-bleifrei">
<packages>
<package name="0603REFLOW">
<description>Bauform 0603 für Reflow</description>
<wire x1="-1.3" y1="0.5" x2="1.3" y2="0.5" width="0.12" layer="21"/>
<wire x1="1.3" y1="0.5" x2="1.3" y2="-0.5" width="0.12" layer="21"/>
<wire x1="1.3" y1="-0.5" x2="-1.3" y2="-0.5" width="0.12" layer="21"/>
<wire x1="-1.3" y1="-0.5" x2="-1.3" y2="0.5" width="0.12" layer="21"/>
<smd name="1" x="-0.8" y="0" dx="0.8" dy="0.75" layer="1" roundness="45"/>
<smd name="2" x="0.8" y="0" dx="0.8" dy="0.75" layer="1" roundness="45"/>
<text x="-1.3" y="0.9" size="0.8" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.27" y="-1.905" size="0.8" layer="27" ratio="15">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.3" x2="0.2" y2="0.3" layer="35"/>
</package>
<package name="0603WAVE">
<description>Bauform 0603 mit größeren Pads</description>
<wire x1="-1.7" y1="0.5" x2="1.7" y2="0.5" width="0.12" layer="21"/>
<wire x1="1.7" y1="0.5" x2="1.7" y2="-0.5" width="0.12" layer="21"/>
<wire x1="1.7" y1="-0.5" x2="-1.7" y2="-0.5" width="0.12" layer="21"/>
<wire x1="-1.7" y1="-0.5" x2="-1.7" y2="0.5" width="0.12" layer="21"/>
<smd name="1" x="-1" y="0" dx="1.2" dy="0.75" layer="1" roundness="45"/>
<smd name="2" x="1" y="0" dx="1.2" dy="0.75" layer="1" roundness="45"/>
<text x="-1.3" y="0.9" size="1" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="-0.2" y1="-0.3" x2="0.2" y2="0.3" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CAP">
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="-0.508" y2="-1.524" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.524" x2="0.508" y2="-1.524" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RES0603" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="R" package="0603REFLOW">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="W" package="0603WAVE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP0603" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="R" package="0603REFLOW">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="W" package="0603WAVE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="cab1">
<packages>
<package name="SO4">
<wire x1="-0.635" y1="0.889" x2="-0.635" y2="1.524" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.524" x2="-0.635" y2="5.461" width="0.127" layer="21"/>
<wire x1="-0.635" y1="5.461" x2="3.175" y2="5.461" width="0.127" layer="21"/>
<wire x1="3.175" y1="5.461" x2="3.175" y2="1.524" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.524" x2="3.175" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.635" y1="0.889" x2="3.175" y2="0.889" width="0.127" layer="21"/>
<wire x1="-0.635" y1="1.524" x2="3.175" y2="1.524" width="0.127" layer="21"/>
<circle x="0" y="2.032" radius="0.254" width="0.127" layer="21"/>
<smd name="1" x="0" y="0" dx="1.6764" dy="0.8128" layer="1" rot="R90"/>
<smd name="4" x="0" y="6.35" dx="1.6764" dy="0.8128" layer="1" rot="R90"/>
<smd name="3" x="2.54" y="6.35" dx="1.6764" dy="0.8128" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="0" dx="1.6764" dy="0.8128" layer="1" rot="R90"/>
<text x="-1.27" y="0" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="0" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PC817">
<wire x1="0.127" y1="-1.143" x2="1.524" y2="0.254" width="0.1524" layer="94"/>
<wire x1="1.524" y1="0.254" x2="0.635" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="0.635" y1="-0.127" x2="1.143" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.143" y1="-0.635" x2="1.524" y2="0.254" width="0.1524" layer="94"/>
<wire x1="1.397" y1="1.397" x2="0.508" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.016" x2="1.016" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.016" y1="0.508" x2="1.397" y2="1.397" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.397" y2="1.397" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="-1.905" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.27" x2="-3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="-4.445" y1="5.08" x2="6.985" y2="5.08" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-5.08" x2="6.985" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-1.905" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="5.08" x2="-4.445" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.27" x2="-1.905" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.985" y1="5.08" x2="6.985" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.905" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="4.826" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="4.318" y1="-1.016" x2="4.826" y2="-2.286" width="0.1524" layer="94"/>
<wire x1="4.826" y1="-2.286" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="4.826" y1="-2.286" x2="3.556" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-1.778" x2="4.318" y2="-1.016" width="0.1524" layer="94"/>
<text x="-4.445" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.445" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="2.159" y1="-2.54" x2="2.921" y2="2.54" layer="94"/>
<pin name="A" x="-7.62" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="C" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="EMIT" x="10.16" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="COL" x="10.16" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PC357">
<gates>
<gate name="G$1" symbol="PC817" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO4">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="COL" pad="4"/>
<connect gate="G$1" pin="EMIT" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARTIKEL" value="5810913" constant="no"/>
<attribute name="HERSTELLER" value="SHARP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="cabframe" deviceset="A3Q-LOC" device=""/>
<part name="IC1" library="myatmel" deviceset="MEGA328PA" device="" value="Mega328?"/>
<part name="U$2" library="cabframe" deviceset="A3Q-LOC" device=""/>
<part name="IC2" library="ftdichip" deviceset="FT232R" device="L"/>
<part name="CON1" library="cab2" deviceset="USB_B_MINI" device="DU1"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="R1" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="R2" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="10k"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="C1" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="CON2" library="myatmel" deviceset="ISP-PROG" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
<part name="Q1" library="MYPASS" deviceset="MT-CRYSTAL" device="" value="16MHz"/>
<part name="C3" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="12p"/>
<part name="C4" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="12p"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="IC3" library="74xx-eu" deviceset="74*11" device="D" technology="HCT"/>
<part name="IC4" library="74xx-eu" deviceset="74*10" device="D" technology="HCT"/>
<part name="C5" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C6" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C7" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C8" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C9" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="P+4" library="supply1" deviceset="+5V" device=""/>
<part name="C10" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C11" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="R3" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="10k"/>
<part name="P+5" library="supply1" deviceset="+5V" device=""/>
<part name="PE1" library="supply1" deviceset="PE" device=""/>
<part name="PE2" library="supply1" deviceset="PE" device=""/>
<part name="CON4" library="CAB" deviceset="WUP107-152" device=""/>
<part name="L1" library="MYMURATA" deviceset="BLA31" device="" value="BLA31AG102"/>
<part name="L2" library="MYMURATA" deviceset="BLA31" device="" value="BLA31AG102"/>
<part name="L3" library="MYMURATA" deviceset="BLA31" device="" value="BLA31AG102"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="CON3" library="CAB" deviceset="WUP107-151" device=""/>
<part name="C13" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C14" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C15" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C16" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C17" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C18" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C19" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C22" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C23" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C24" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="PE4" library="supply1" deviceset="PE" device=""/>
<part name="L4" library="MYMURATA" deviceset="BLA31" device="" value="BLA31AG102"/>
<part name="L5" library="MYMURATA" deviceset="BLA31" device="" value="BLA31AG102"/>
<part name="L6" library="MYMURATA" deviceset="BLA31" device="" value="BLA31AG102"/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="P+6" library="supply1" deviceset="+5V" device=""/>
<part name="C25" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C26" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C27" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C28" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C29" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C30" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C31" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C32" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C33" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C34" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C35" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="R4" library="MYPASS" deviceset="ARC241" device="" value="33R"/>
<part name="R5" library="MYPASS" deviceset="ARC241" device="" value="33R"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="P+13" library="supply1" deviceset="+5V" device=""/>
<part name="IC11" library="MYTI" deviceset="TPS54540" device=""/>
<part name="D23" library="MYDISCR" deviceset="MBRS130T3" device="" value="MBRS130T3"/>
<part name="C39" library="MYPASS" deviceset="CAP1206" device="" value="4,7µ 50V">
<attribute name="ARTIKEL" value="5918230"/>
</part>
<part name="C41" library="MYPASS" deviceset="CAP1206" device="" value="4,7µ 50V">
<attribute name="ARTIKEL" value="5918230"/>
</part>
<part name="GND56" library="supply1" deviceset="GND" device=""/>
<part name="P+28" library="supply1" deviceset="+24V" device=""/>
<part name="R21" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="243k 1%"/>
<part name="R23" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="10k"/>
<part name="C37" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="18n"/>
<part name="GND60" library="supply1" deviceset="GND" device=""/>
<part name="C44" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="L8" library="MYPASS" deviceset="DO3340P" device="" value="WE 744770122"/>
<part name="C45" library="MYPASS" deviceset="CAP1210" device="" value="22µ 35V">
<attribute name="ARTIKEL" value="5918231"/>
</part>
<part name="C88" library="MYPASS" deviceset="CAP1210" device="" value="22µ 35V">
<attribute name="ARTIKEL" value="5918231"/>
</part>
<part name="GND64" library="supply1" deviceset="GND" device=""/>
<part name="R25" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="68k 1%"/>
<part name="R98" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="13k 1%"/>
<part name="GND69" library="supply1" deviceset="GND" device=""/>
<part name="GND72" library="supply1" deviceset="GND" device=""/>
<part name="C90" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="10p"/>
<part name="IC12" library="MYACL" deviceset="74AC16245" device="" value="74AHCT16245"/>
<part name="IC13" library="MYACL" deviceset="74AC16245" device="" value="74ahct16245"/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="P+14" library="supply1" deviceset="+5V" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="P+15" library="supply1" deviceset="+5V" device=""/>
<part name="C36" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C38" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C40" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C42" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C43" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C46" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C47" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C48" library="mypassive-bleifrei" deviceset="CAP0603" device="R" value="100n"/>
<part name="C49" library="MYPASS" deviceset="CAP0805" device="" value="100n"/>
<part name="C50" library="MYPASS" deviceset="CAP0805" device="" value="100n"/>
<part name="PE5" library="supply1" deviceset="PE" device=""/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="SJ1" library="jumper" deviceset="SJ2W" device=""/>
<part name="P+16" library="supply1" deviceset="+5V" device=""/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="F1" library="MYPASS" deviceset="POLYSWITCH" device="MINISMDC" value="500mA"/>
<part name="R10" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R11" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R12" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R13" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R14" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R15" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R16" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R17" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R18" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R19" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R20" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="LED1" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED2" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED3" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED4" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED5" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED6" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED7" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED8" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED9" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED10" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED11" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="R22" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R24" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R26" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R27" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R28" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R29" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R30" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R31" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R32" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="R33" library="MYPASS" deviceset="RES0805" device="" value="1k5"/>
<part name="LED12" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED13" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED14" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED15" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED16" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED17" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED18" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED19" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED20" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="LED21" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="D1" library="MYDISCR" deviceset="MBRS130T3" device="" value="MBRS130T3"/>
<part name="P+17" library="supply1" deviceset="+24V" device=""/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="PE6" library="supply1" deviceset="PE" device=""/>
<part name="R34" library="MYPASS" deviceset="RES0805" device="" value="10k"/>
<part name="LED22" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="CON5" library="myconn" deviceset="AKL-182" device=""/>
<part name="U$3" library="myON" deviceset="MMQA" device="" value="MMQA5V6T1G"/>
<part name="U$4" library="myON" deviceset="MMQA" device="" value="MMQA5V6T1G"/>
<part name="U$5" library="myON" deviceset="MMQA" device="" value="MMQA5V6T1G"/>
<part name="U$6" library="myON" deviceset="MMQA" device="" value="MMQA5V6T1G"/>
<part name="U$7" library="myON" deviceset="MMQA" device="" value="MMQA5V6T1G"/>
<part name="U$8" library="myON" deviceset="MMQA" device="" value="MMQA5V6T1G"/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="R35" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="33R"/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="R6" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="R7" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="R8" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="R9" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="R36" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="R37" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="R38" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="R39" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="R40" library="mypassive-bleifrei" deviceset="RES0603" device="R" value="4k7"/>
<part name="C2" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="C12" library="mypassive-bleifrei" deviceset="CAP0603" device="W" value="1n"/>
<part name="PE3" library="supply1" deviceset="PE" device=""/>
<part name="OC23" library="cab1" deviceset="PC357" device=""/>
<part name="R41" library="MYPASS" deviceset="RES1206" device="" value="2k2"/>
<part name="D2" library="MYDISCR" deviceset="BAT54" device="" value="BAT54"/>
<part name="LED23" library="led" deviceset="LED" device="CHIP-LED0805"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="0" y="0"/>
<instance part="IC1" gate="G$1" x="139.7" y="157.48"/>
<instance part="IC2" gate="1" x="88.9" y="210.82"/>
<instance part="CON1" gate="&gt;NAME" x="15.24" y="200.66" rot="MR0"/>
<instance part="GND2" gate="1" x="27.94" y="187.96"/>
<instance part="GND3" gate="1" x="73.66" y="187.96"/>
<instance part="P+2" gate="1" x="73.66" y="238.76"/>
<instance part="GND4" gate="1" x="106.68" y="180.34"/>
<instance part="R1" gate="G$1" x="30.48" y="203.2"/>
<instance part="R2" gate="G$1" x="40.64" y="203.2"/>
<instance part="GND5" gate="1" x="48.26" y="187.96"/>
<instance part="C1" gate="G$1" x="68.58" y="205.74" rot="R180"/>
<instance part="GND6" gate="1" x="63.5" y="203.2"/>
<instance part="CON2" gate="G$1" x="93.98" y="152.4"/>
<instance part="GND7" gate="1" x="106.68" y="142.24"/>
<instance part="P+3" gate="1" x="106.68" y="165.1"/>
<instance part="Q1" gate="G$1" x="208.28" y="233.68" rot="R90"/>
<instance part="Q1" gate="G$2" x="17.78" y="25.4"/>
<instance part="Q1" gate="G$3" x="20.32" y="25.4"/>
<instance part="C3" gate="G$1" x="218.44" y="228.6" rot="R180"/>
<instance part="C4" gate="G$1" x="218.44" y="236.22" rot="R180"/>
<instance part="GND8" gate="1" x="226.06" y="223.52"/>
<instance part="IC3" gate="A" x="218.44" y="213.36"/>
<instance part="IC3" gate="B" x="218.44" y="200.66"/>
<instance part="IC3" gate="C" x="218.44" y="182.88"/>
<instance part="IC4" gate="A" x="243.84" y="200.66"/>
<instance part="IC4" gate="B" x="261.62" y="200.66"/>
<instance part="IC4" gate="C" x="218.44" y="167.64" rot="R180"/>
<instance part="IC3" gate="P" x="25.4" y="25.4" smashed="yes">
<attribute name="NAME" x="24.765" y="24.765" size="1.778" layer="95"/>
</instance>
<instance part="IC4" gate="P" x="33.02" y="25.4" smashed="yes">
<attribute name="NAME" x="32.385" y="20.701" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="IC1" gate="G$3" x="40.64" y="25.4" smashed="yes"/>
<instance part="IC1" gate="G$4" x="48.26" y="25.4" smashed="yes"/>
<instance part="IC1" gate="G$2" x="55.88" y="25.4" smashed="yes"/>
<instance part="C5" gate="G$1" x="25.4" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="24.892" y="20.828" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="24.384" y="24.638" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="33.02" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="32.512" y="26.924" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="36.83" y="18.542" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C7" gate="G$1" x="40.64" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="43.688" y="26.67" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="43.18" y="18.542" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="48.26" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="51.054" y="26.924" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="50.8" y="18.796" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C9" gate="G$1" x="55.88" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="58.928" y="26.67" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="58.166" y="18.796" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND9" gate="1" x="25.4" y="12.7"/>
<instance part="P+4" gate="1" x="25.4" y="38.1"/>
<instance part="C10" gate="G$1" x="63.5" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="65.786" y="26.416" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="65.532" y="18.796" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="71.12" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="73.406" y="26.67" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="73.406" y="17.78" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="101.6" y="172.72" rot="R180"/>
<instance part="P+5" gate="1" x="96.52" y="177.8"/>
<instance part="PE1" gate="M" x="104.14" y="10.16"/>
<instance part="PE2" gate="M" x="363.22" y="127"/>
<instance part="CON4" gate="G$2" x="104.14" y="25.4"/>
<instance part="CON4" gate="G$3" x="109.22" y="25.4"/>
<instance part="CON3" gate="G$2" x="114.3" y="25.4"/>
<instance part="CON3" gate="G$3" x="119.38" y="25.4"/>
<instance part="P+13" gate="1" x="121.92" y="96.52" smashed="yes">
<attribute name="VALUE" x="119.38" y="91.44" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IC11" gate="G$1" x="55.88" y="73.66" smashed="yes">
<attribute name="NAME" x="55.88" y="104.14" size="1.778" layer="95"/>
<attribute name="VALUE" x="55.88" y="102.108" size="1.778" layer="96"/>
</instance>
<instance part="D23" gate="G$1" x="86.36" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="83.7184" y="84.0486" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="88.9254" y="80.6958" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C39" gate="G$1" x="27.94" y="93.98" smashed="yes" rot="R90">
<attribute name="ARTIKEL" x="27.94" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="NAME" x="25.4" y="91.44" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="31.75" y="91.44" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C41" gate="G$1" x="35.56" y="93.98" smashed="yes" rot="R90">
<attribute name="ARTIKEL" x="35.56" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="NAME" x="35.56" y="100.33" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="39.37" y="91.44" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND56" gate="1" x="27.94" y="83.82" smashed="yes">
<attribute name="VALUE" x="25.4" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="P+28" gate="1" x="27.94" y="104.14" smashed="yes">
<attribute name="VALUE" x="25.4" y="99.06" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R21" gate="G$1" x="48.26" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="45.72" y="78.74" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="52.07" y="73.66" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R23" gate="G$1" x="38.1" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="73.66" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="41.91" y="76.2" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C37" gate="G$1" x="38.1" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="81.28" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="41.91" y="81.28" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND60" gate="1" x="38.1" y="66.04" smashed="yes">
<attribute name="VALUE" x="35.56" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="C44" gate="G$1" x="81.28" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="83.82" y="96.52" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="83.82" y="102.87" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L8" gate="G$1" x="93.98" y="91.44" smashed="yes">
<attribute name="NAME" x="90.17" y="93.98" size="1.778" layer="95"/>
<attribute name="VALUE" x="89.916" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="C45" gate="G$1" x="101.6" y="86.36" smashed="yes" rot="R90">
<attribute name="ARTIKEL" x="101.6" y="86.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="NAME" x="100.33" y="84.582" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="99.06" y="87.376" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C88" gate="G$1" x="109.22" y="86.36" smashed="yes" rot="R90">
<attribute name="ARTIKEL" x="109.22" y="86.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="NAME" x="107.696" y="84.328" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="106.172" y="93.218" size="1.778" layer="96"/>
</instance>
<instance part="GND64" gate="1" x="101.6" y="78.74" smashed="yes">
<attribute name="VALUE" x="99.06" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="121.92" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="123.698" y="90.932" size="1.778" layer="95"/>
<attribute name="VALUE" x="123.952" y="93.218" size="1.778" layer="96"/>
</instance>
<instance part="R98" gate="G$1" x="121.92" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="115.57" y="83.312" size="1.778" layer="95"/>
<attribute name="VALUE" x="120.65" y="77.978" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND69" gate="1" x="63.5" y="66.04" smashed="yes">
<attribute name="VALUE" x="60.96" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="GND72" gate="1" x="121.92" y="68.58" smashed="yes">
<attribute name="VALUE" x="119.38" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="C90" gate="G$1" x="129.54" y="76.2" smashed="yes" rot="R270">
<attribute name="NAME" x="124.206" y="72.136" size="1.778" layer="95"/>
<attribute name="VALUE" x="124.714" y="77.978" size="1.778" layer="96"/>
</instance>
<instance part="C49" gate="G$1" x="142.24" y="22.86" rot="R180"/>
<instance part="C50" gate="G$1" x="142.24" y="30.48" rot="R180"/>
<instance part="PE5" gate="M" x="149.86" y="17.78"/>
<instance part="GND22" gate="1" x="134.62" y="17.78"/>
<instance part="D1" gate="G$1" x="40.64" y="142.24"/>
<instance part="P+17" gate="1" x="48.26" y="147.32"/>
<instance part="GND26" gate="1" x="38.1" y="134.62"/>
<instance part="PE6" gate="M" x="35.56" y="127"/>
<instance part="R34" gate="G$1" x="60.96" y="132.08" rot="MR90"/>
<instance part="LED22" gate="G$1" x="60.96" y="129.54" rot="MR0"/>
<instance part="GND27" gate="1" x="60.96" y="121.92"/>
<instance part="CON5" gate="G$1" x="25.4" y="129.54"/>
</instances>
<busses>
<bus name="BUS:RXD,TXD,MISO-DIRY,MOSI-DIRX,SCK-DIRZ,/RST,X1,X2,Y1,Y2,Z1,Z2,STEPX,STEPY,STEPZ,DIRX,DIRY,DIRZ,DOOR1,DOOR2,LASER1,LASER2,CHILLER,LASERDIS,DOOR,AIR,AUX1,XT1,XT2,STEPEN">
<segment>
<wire x1="195.58" y1="160.02" x2="195.58" y2="241.3" width="0.762" layer="92"/>
<wire x1="195.58" y1="241.3" x2="193.04" y2="243.84" width="0.762" layer="92"/>
<wire x1="193.04" y1="243.84" x2="127" y2="243.84" width="0.762" layer="92"/>
<wire x1="127" y1="243.84" x2="124.46" y2="241.3" width="0.762" layer="92"/>
<wire x1="124.46" y1="241.3" x2="124.46" y2="152.4" width="0.762" layer="92"/>
<wire x1="195.58" y1="241.3" x2="198.12" y2="243.84" width="0.762" layer="92"/>
<wire x1="198.12" y1="243.84" x2="210.82" y2="243.84" width="0.762" layer="92"/>
<label x="205.486" y="244.602" size="1.778" layer="95"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="CON1" gate="&gt;NAME" pin="GND"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="25.4" y1="193.04" x2="27.94" y2="193.04" width="0.1524" layer="91"/>
<wire x1="27.94" y1="193.04" x2="27.94" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="1" pin="GND@A"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="76.2" y1="193.04" x2="73.66" y2="193.04" width="0.1524" layer="91"/>
<wire x1="73.66" y1="193.04" x2="73.66" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="1" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="104.14" y1="190.5" x2="106.68" y2="190.5" width="0.1524" layer="91"/>
<wire x1="106.68" y1="190.5" x2="106.68" y2="187.96" width="0.1524" layer="91"/>
<pinref part="IC2" gate="1" pin="GND@1"/>
<wire x1="106.68" y1="187.96" x2="106.68" y2="185.42" width="0.1524" layer="91"/>
<wire x1="106.68" y1="185.42" x2="106.68" y2="182.88" width="0.1524" layer="91"/>
<wire x1="104.14" y1="187.96" x2="106.68" y2="187.96" width="0.1524" layer="91"/>
<junction x="106.68" y="187.96"/>
<pinref part="IC2" gate="1" pin="GND@2"/>
<wire x1="104.14" y1="185.42" x2="106.68" y2="185.42" width="0.1524" layer="91"/>
<junction x="106.68" y="185.42"/>
<pinref part="IC2" gate="1" pin="TEST"/>
<wire x1="104.14" y1="195.58" x2="106.68" y2="195.58" width="0.1524" layer="91"/>
<wire x1="106.68" y1="195.58" x2="106.68" y2="190.5" width="0.1524" layer="91"/>
<junction x="106.68" y="190.5"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="45.72" y1="203.2" x2="48.26" y2="203.2" width="0.1524" layer="91"/>
<wire x1="48.26" y1="203.2" x2="48.26" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="106.68" y1="144.78" x2="106.68" y2="147.32" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="147.32" x2="104.14" y2="147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="223.52" y1="236.22" x2="226.06" y2="236.22" width="0.1524" layer="91"/>
<wire x1="226.06" y1="236.22" x2="226.06" y2="228.6" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="226.06" y1="228.6" x2="226.06" y2="226.06" width="0.1524" layer="91"/>
<wire x1="223.52" y1="228.6" x2="226.06" y2="228.6" width="0.1524" layer="91"/>
<junction x="226.06" y="228.6"/>
</segment>
<segment>
<pinref part="IC3" gate="P" pin="GND"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="25.4" y1="17.78" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="25.4" y1="15.24" x2="25.4" y2="17.78" width="0.1524" layer="91"/>
<junction x="25.4" y="17.78"/>
<pinref part="C6" gate="G$1" pin="1"/>
<pinref part="IC4" gate="P" pin="GND"/>
<wire x1="33.02" y1="20.32" x2="33.02" y2="17.78" width="0.1524" layer="91"/>
<wire x1="25.4" y1="17.78" x2="33.02" y2="17.78" width="0.1524" layer="91"/>
<junction x="33.02" y="17.78"/>
<wire x1="33.02" y1="17.78" x2="33.02" y2="12.7" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$3" pin="GND"/>
<wire x1="40.64" y1="20.32" x2="40.64" y2="12.7" width="0.1524" layer="91"/>
<wire x1="33.02" y1="12.7" x2="40.64" y2="12.7" width="0.1524" layer="91"/>
<junction x="40.64" y="12.7"/>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$4" pin="GND"/>
<wire x1="48.26" y1="20.32" x2="48.26" y2="12.7" width="0.1524" layer="91"/>
<wire x1="40.64" y1="12.7" x2="48.26" y2="12.7" width="0.1524" layer="91"/>
<junction x="48.26" y="12.7"/>
<pinref part="IC1" gate="G$2" pin="GND"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="55.88" y1="12.7" x2="55.88" y2="20.32" width="0.1524" layer="91"/>
<wire x1="48.26" y1="12.7" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<junction x="55.88" y="12.7"/>
<pinref part="Q1" gate="G$3" pin="GND"/>
<wire x1="25.4" y1="17.78" x2="20.32" y2="17.78" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$2" pin="GND"/>
<wire x1="20.32" y1="17.78" x2="17.78" y2="17.78" width="0.1524" layer="91"/>
<junction x="20.32" y="17.78"/>
<wire x1="55.88" y1="12.7" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
<wire x1="63.5" y1="20.32" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="63.5" y1="12.7" x2="71.12" y2="12.7" width="0.1524" layer="91"/>
<wire x1="71.12" y1="12.7" x2="71.12" y2="20.32" width="0.1524" layer="91"/>
<junction x="63.5" y="12.7"/>
<pinref part="C10" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND56" gate="1" pin="GND"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="27.94" y1="86.36" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="27.94" y1="88.9" x2="35.56" y2="88.9" width="0.1524" layer="91"/>
<junction x="27.94" y="88.9"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="38.1" y1="71.12" x2="48.26" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND60" gate="1" pin="GND"/>
<junction x="38.1" y="71.12"/>
<wire x1="38.1" y1="68.58" x2="38.1" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D23" gate="G$1" pin="A"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="86.36" y1="81.28" x2="101.6" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C88" gate="G$1" pin="1"/>
<wire x1="101.6" y1="81.28" x2="109.22" y2="81.28" width="0.1524" layer="91"/>
<junction x="101.6" y="81.28"/>
<pinref part="GND64" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC11" gate="G$1" pin="GND"/>
<pinref part="GND69" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R98" gate="G$1" pin="1"/>
<pinref part="GND72" gate="1" pin="GND"/>
<pinref part="C90" gate="G$1" pin="2"/>
<wire x1="121.92" y1="71.12" x2="129.54" y2="71.12" width="0.1524" layer="91"/>
<junction x="121.92" y="71.12"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="137.16" y1="30.48" x2="134.62" y2="30.48" width="0.1524" layer="91"/>
<wire x1="134.62" y1="30.48" x2="134.62" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="134.62" y1="22.86" x2="134.62" y2="20.32" width="0.1524" layer="91"/>
<wire x1="137.16" y1="22.86" x2="134.62" y2="22.86" width="0.1524" layer="91"/>
<junction x="134.62" y="22.86"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="30.48" y1="137.16" x2="38.1" y2="137.16" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="LED22" gate="G$1" pin="C"/>
<pinref part="GND27" gate="1" pin="GND"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<pinref part="CON1" gate="&gt;NAME" pin="D-"/>
<wire x1="25.4" y1="200.66" x2="33.02" y2="200.66" width="0.1524" layer="91"/>
<wire x1="33.02" y1="200.66" x2="35.56" y2="198.12" width="0.1524" layer="91"/>
<pinref part="IC2" gate="1" pin="USBDM"/>
<wire x1="35.56" y1="198.12" x2="76.2" y2="198.12" width="0.1524" layer="91"/>
<label x="66.04" y="198.374" size="1.4224" layer="95"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<pinref part="CON1" gate="&gt;NAME" pin="D+"/>
<wire x1="25.4" y1="198.12" x2="33.02" y2="198.12" width="0.1524" layer="91"/>
<wire x1="33.02" y1="198.12" x2="35.56" y2="200.66" width="0.1524" layer="91"/>
<pinref part="IC2" gate="1" pin="USBDP"/>
<wire x1="35.56" y1="200.66" x2="76.2" y2="200.66" width="0.1524" layer="91"/>
<label x="66.04" y="200.914" size="1.4224" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="CON1" gate="&gt;NAME" pin="VBUS"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="IC2" gate="1" pin="!RESET"/>
<wire x1="76.2" y1="226.06" x2="35.56" y2="226.06" width="0.1524" layer="91"/>
<wire x1="35.56" y1="226.06" x2="35.56" y2="203.2" width="0.1524" layer="91"/>
<junction x="35.56" y="203.2"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="IC2" gate="1" pin="VCCIO"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="76.2" y1="231.14" x2="73.66" y2="231.14" width="0.1524" layer="91"/>
<wire x1="73.66" y1="231.14" x2="73.66" y2="233.68" width="0.1524" layer="91"/>
<pinref part="IC2" gate="1" pin="VCC"/>
<wire x1="73.66" y1="233.68" x2="73.66" y2="236.22" width="0.1524" layer="91"/>
<wire x1="76.2" y1="233.68" x2="73.66" y2="233.68" width="0.1524" layer="91"/>
<junction x="73.66" y="233.68"/>
</segment>
<segment>
<pinref part="CON2" gate="G$1" pin="VCC"/>
<wire x1="104.14" y1="160.02" x2="106.68" y2="160.02" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="106.68" y1="160.02" x2="106.68" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="P" pin="VCC"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="25.4" y1="33.02" x2="25.4" y2="30.48" width="0.1524" layer="91"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="25.4" y1="35.56" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<junction x="25.4" y="33.02"/>
<pinref part="IC4" gate="P" pin="VCC"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="33.02" y1="33.02" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<wire x1="25.4" y1="33.02" x2="33.02" y2="33.02" width="0.1524" layer="91"/>
<junction x="33.02" y="33.02"/>
<wire x1="33.02" y1="33.02" x2="33.02" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="IC1" gate="G$3" pin="VCC"/>
<wire x1="40.64" y1="30.48" x2="40.64" y2="38.1" width="0.1524" layer="91"/>
<wire x1="33.02" y1="38.1" x2="40.64" y2="38.1" width="0.1524" layer="91"/>
<junction x="40.64" y="38.1"/>
<pinref part="IC1" gate="G$4" pin="VCC"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="48.26" y1="38.1" x2="48.26" y2="30.48" width="0.1524" layer="91"/>
<wire x1="40.64" y1="38.1" x2="48.26" y2="38.1" width="0.1524" layer="91"/>
<junction x="48.26" y="38.1"/>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="IC1" gate="G$2" pin="AVCC"/>
<wire x1="55.88" y1="30.48" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<wire x1="48.26" y1="38.1" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<junction x="55.88" y="38.1"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="55.88" y1="38.1" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<wire x1="63.5" y1="38.1" x2="63.5" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="63.5" y1="38.1" x2="71.12" y2="38.1" width="0.1524" layer="91"/>
<wire x1="71.12" y1="38.1" x2="71.12" y2="30.48" width="0.1524" layer="91"/>
<junction x="63.5" y="38.1"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="96.52" y1="172.72" x2="96.52" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="L8" gate="G$1" pin="2"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="99.06" y1="91.44" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<pinref part="C88" gate="G$1" pin="2"/>
<wire x1="101.6" y1="91.44" x2="109.22" y2="91.44" width="0.1524" layer="91"/>
<junction x="101.6" y="91.44"/>
<pinref part="R25" gate="G$1" pin="2"/>
<junction x="109.22" y="91.44"/>
<wire x1="109.22" y1="91.44" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<pinref part="P+13" gate="1" pin="+5V"/>
<wire x1="121.92" y1="93.98" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<junction x="121.92" y="91.44"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC2" gate="1" pin="3V3OUT"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="76.2" y1="205.74" x2="73.66" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="IC2" gate="1" pin="TXD"/>
<wire x1="104.14" y1="233.68" x2="121.92" y2="233.68" width="0.1524" layer="91"/>
<wire x1="121.92" y1="233.68" x2="124.46" y2="236.22" width="0.1524" layer="91"/>
<label x="106.68" y="233.934" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PD0-PCINT16-RXD"/>
<wire x1="172.72" y1="177.8" x2="193.04" y2="177.8" width="0.1524" layer="91"/>
<wire x1="193.04" y1="177.8" x2="195.58" y2="180.34" width="0.1524" layer="91"/>
<label x="175.26" y="178.054" size="1.4224" layer="95"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="IC2" gate="1" pin="RXD"/>
<wire x1="104.14" y1="231.14" x2="121.92" y2="231.14" width="0.1524" layer="91"/>
<wire x1="121.92" y1="231.14" x2="124.46" y2="233.68" width="0.1524" layer="91"/>
<label x="106.68" y="231.394" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PD1-PCINT17-TXD"/>
<wire x1="172.72" y1="175.26" x2="193.04" y2="175.26" width="0.1524" layer="91"/>
<wire x1="193.04" y1="175.26" x2="195.58" y2="177.8" width="0.1524" layer="91"/>
<label x="175.26" y="175.514" size="1.4224" layer="95"/>
</segment>
</net>
<net name="/RST" class="0">
<segment>
<pinref part="CON2" gate="G$1" pin="/RST"/>
<wire x1="104.14" y1="149.86" x2="121.92" y2="149.86" width="0.1524" layer="91"/>
<wire x1="121.92" y1="149.86" x2="124.46" y2="152.4" width="0.1524" layer="91"/>
<label x="109.22" y="150.114" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PC6-PCINT14-/RESET"/>
<wire x1="172.72" y1="182.88" x2="193.04" y2="182.88" width="0.1524" layer="91"/>
<wire x1="193.04" y1="182.88" x2="195.58" y2="185.42" width="0.1524" layer="91"/>
<label x="175.26" y="183.134" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="106.68" y1="172.72" x2="121.92" y2="172.72" width="0.1524" layer="91"/>
<wire x1="121.92" y1="172.72" x2="124.46" y2="175.26" width="0.1524" layer="91"/>
<label x="114.3" y="172.974" size="1.4224" layer="95"/>
</segment>
</net>
<net name="XT1" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="2"/>
<wire x1="198.12" y1="236.22" x2="208.28" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="208.28" y1="236.22" x2="213.36" y2="236.22" width="0.1524" layer="91"/>
<junction x="208.28" y="236.22"/>
<label x="175.26" y="205.994" size="1.4224" layer="95"/>
<wire x1="198.12" y1="236.22" x2="195.58" y2="238.76" width="0.1524" layer="91"/>
<label x="198.882" y="236.474" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PB6-XTAL1-TOSC1"/>
<wire x1="172.72" y1="205.74" x2="193.04" y2="205.74" width="0.1524" layer="91"/>
<wire x1="193.04" y1="205.74" x2="195.58" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="XT2" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="1"/>
<wire x1="198.12" y1="228.6" x2="208.28" y2="228.6" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="208.28" y1="228.6" x2="213.36" y2="228.6" width="0.1524" layer="91"/>
<junction x="208.28" y="228.6"/>
<label x="175.26" y="203.454" size="1.4224" layer="95"/>
<wire x1="198.12" y1="228.6" x2="195.58" y2="231.14" width="0.1524" layer="91"/>
<label x="198.882" y="228.854" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PB7-XTAL2-TOSC2"/>
<wire x1="172.72" y1="203.2" x2="193.04" y2="203.2" width="0.1524" layer="91"/>
<wire x1="193.04" y1="203.2" x2="195.58" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PC0-PCINT8-ADC0"/>
<wire x1="172.72" y1="198.12" x2="193.04" y2="198.12" width="0.1524" layer="91"/>
<wire x1="193.04" y1="198.12" x2="195.58" y2="200.66" width="0.1524" layer="91"/>
<label x="175.26" y="198.374" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="I0"/>
<wire x1="210.82" y1="215.9" x2="198.12" y2="215.9" width="0.1524" layer="91"/>
<wire x1="198.12" y1="215.9" x2="195.58" y2="218.44" width="0.1524" layer="91"/>
<label x="199.136" y="216.154" size="1.4224" layer="95"/>
</segment>
</net>
<net name="X2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PC1-PCINT9-ADC1"/>
<wire x1="172.72" y1="195.58" x2="193.04" y2="195.58" width="0.1524" layer="91"/>
<wire x1="193.04" y1="195.58" x2="195.58" y2="198.12" width="0.1524" layer="91"/>
<label x="175.26" y="195.834" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="I1"/>
<wire x1="210.82" y1="213.36" x2="198.12" y2="213.36" width="0.1524" layer="91"/>
<wire x1="198.12" y1="213.36" x2="195.58" y2="215.9" width="0.1524" layer="91"/>
<label x="199.136" y="213.614" size="1.4224" layer="95"/>
</segment>
</net>
<net name="Y1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PC2-PCINT10-ADC2"/>
<wire x1="172.72" y1="193.04" x2="193.04" y2="193.04" width="0.1524" layer="91"/>
<wire x1="193.04" y1="193.04" x2="195.58" y2="195.58" width="0.1524" layer="91"/>
<label x="175.26" y="193.294" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="I2"/>
<wire x1="210.82" y1="210.82" x2="198.12" y2="210.82" width="0.1524" layer="91"/>
<wire x1="198.12" y1="210.82" x2="195.58" y2="213.36" width="0.1524" layer="91"/>
<label x="199.136" y="211.074" size="1.4224" layer="95"/>
</segment>
</net>
<net name="Y2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PC3-PCINT11-ADC3"/>
<wire x1="172.72" y1="190.5" x2="193.04" y2="190.5" width="0.1524" layer="91"/>
<wire x1="193.04" y1="190.5" x2="195.58" y2="193.04" width="0.1524" layer="91"/>
<label x="175.26" y="190.754" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="B" pin="I0"/>
<wire x1="210.82" y1="203.2" x2="198.12" y2="203.2" width="0.1524" layer="91"/>
<wire x1="198.12" y1="203.2" x2="195.58" y2="205.74" width="0.1524" layer="91"/>
<label x="199.136" y="203.454" size="1.4224" layer="95"/>
</segment>
</net>
<net name="Z1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PC4-PCINT12-SDA-ADC4"/>
<wire x1="172.72" y1="187.96" x2="193.04" y2="187.96" width="0.1524" layer="91"/>
<wire x1="193.04" y1="187.96" x2="195.58" y2="190.5" width="0.1524" layer="91"/>
<label x="175.26" y="188.214" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="B" pin="I1"/>
<wire x1="210.82" y1="200.66" x2="198.12" y2="200.66" width="0.1524" layer="91"/>
<wire x1="198.12" y1="200.66" x2="195.58" y2="203.2" width="0.1524" layer="91"/>
<label x="199.136" y="200.914" size="1.4224" layer="95"/>
</segment>
</net>
<net name="Z2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PC5-PCINT13-SCL-ADC5"/>
<wire x1="172.72" y1="185.42" x2="193.04" y2="185.42" width="0.1524" layer="91"/>
<wire x1="193.04" y1="185.42" x2="195.58" y2="187.96" width="0.1524" layer="91"/>
<label x="175.26" y="185.674" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC3" gate="B" pin="I2"/>
<wire x1="210.82" y1="198.12" x2="198.12" y2="198.12" width="0.1524" layer="91"/>
<wire x1="198.12" y1="198.12" x2="195.58" y2="200.66" width="0.1524" layer="91"/>
<label x="199.136" y="198.374" size="1.4224" layer="95"/>
</segment>
</net>
<net name="LASER1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PD5-PCINT21-OC0B-T1"/>
<wire x1="172.72" y1="165.1" x2="193.04" y2="165.1" width="0.1524" layer="91"/>
<wire x1="193.04" y1="165.1" x2="195.58" y2="167.64" width="0.1524" layer="91"/>
<label x="175.26" y="165.354" size="1.4224" layer="95"/>
</segment>
</net>
<net name="LASER2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PD6-PCINT22-OC0A-AIN0"/>
<wire x1="172.72" y1="162.56" x2="193.04" y2="162.56" width="0.1524" layer="91"/>
<wire x1="193.04" y1="162.56" x2="195.58" y2="165.1" width="0.1524" layer="91"/>
<label x="175.26" y="162.814" size="1.4224" layer="95"/>
</segment>
</net>
<net name="STEPX" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB0-PCINT0-CLKO-ICP1"/>
<wire x1="172.72" y1="220.98" x2="193.04" y2="220.98" width="0.1524" layer="91"/>
<wire x1="193.04" y1="220.98" x2="195.58" y2="223.52" width="0.1524" layer="91"/>
<label x="175.26" y="221.234" size="1.4224" layer="95"/>
</segment>
</net>
<net name="MOSI-DIRX" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB3-PCINT3-OC2A-MOSI"/>
<wire x1="172.72" y1="213.36" x2="193.04" y2="213.36" width="0.1524" layer="91"/>
<wire x1="193.04" y1="213.36" x2="195.58" y2="215.9" width="0.1524" layer="91"/>
<label x="175.26" y="213.614" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="CON2" gate="G$1" pin="MOSI"/>
<wire x1="104.14" y1="154.94" x2="121.92" y2="154.94" width="0.1524" layer="91"/>
<wire x1="121.92" y1="154.94" x2="124.46" y2="157.48" width="0.1524" layer="91"/>
<label x="109.22" y="155.194" size="1.4224" layer="95"/>
</segment>
</net>
<net name="DOOR1" class="0">
<segment>
<pinref part="IC3" gate="C" pin="I0"/>
<wire x1="210.82" y1="185.42" x2="198.12" y2="185.42" width="0.1524" layer="91"/>
<wire x1="198.12" y1="185.42" x2="195.58" y2="187.96" width="0.1524" layer="91"/>
<label x="199.136" y="185.674" size="1.4224" layer="95"/>
</segment>
</net>
<net name="DOOR2" class="0">
<segment>
<pinref part="IC3" gate="C" pin="I1"/>
<wire x1="210.82" y1="182.88" x2="198.12" y2="182.88" width="0.1524" layer="91"/>
<wire x1="198.12" y1="182.88" x2="195.58" y2="185.42" width="0.1524" layer="91"/>
<pinref part="IC3" gate="C" pin="I2"/>
<wire x1="210.82" y1="182.88" x2="210.82" y2="180.34" width="0.1524" layer="91"/>
<junction x="210.82" y="182.88"/>
<label x="199.136" y="183.134" size="1.4224" layer="95"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC4" gate="A" pin="I1"/>
<pinref part="IC3" gate="B" pin="O"/>
<wire x1="236.22" y1="200.66" x2="226.06" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="IC4" gate="A" pin="I0"/>
<wire x1="236.22" y1="203.2" x2="231.14" y2="203.2" width="0.1524" layer="91"/>
<wire x1="231.14" y1="203.2" x2="231.14" y2="213.36" width="0.1524" layer="91"/>
<pinref part="IC3" gate="A" pin="O"/>
<wire x1="231.14" y1="213.36" x2="226.06" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DOOR" class="0">
<segment>
<pinref part="IC4" gate="A" pin="I2"/>
<wire x1="236.22" y1="198.12" x2="231.14" y2="198.12" width="0.1524" layer="91"/>
<wire x1="231.14" y1="198.12" x2="231.14" y2="190.5" width="0.1524" layer="91"/>
<pinref part="IC3" gate="C" pin="O"/>
<wire x1="231.14" y1="190.5" x2="231.14" y2="182.88" width="0.1524" layer="91"/>
<wire x1="231.14" y1="182.88" x2="226.06" y2="182.88" width="0.1524" layer="91"/>
<wire x1="231.14" y1="190.5" x2="198.12" y2="190.5" width="0.1524" layer="91"/>
<wire x1="198.12" y1="190.5" x2="195.58" y2="193.04" width="0.1524" layer="91"/>
<junction x="231.14" y="190.5"/>
<label x="199.39" y="190.754" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PD2-PCINT18-INT0"/>
<wire x1="172.72" y1="172.72" x2="193.04" y2="172.72" width="0.1524" layer="91"/>
<wire x1="193.04" y1="172.72" x2="195.58" y2="175.26" width="0.1524" layer="91"/>
<label x="175.26" y="172.974" size="1.4224" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC4" gate="A" pin="O"/>
<pinref part="IC4" gate="B" pin="I1"/>
<wire x1="251.46" y1="200.66" x2="254" y2="200.66" width="0.1524" layer="91"/>
<pinref part="IC4" gate="B" pin="I0"/>
<wire x1="254" y1="203.2" x2="254" y2="200.66" width="0.1524" layer="91"/>
<junction x="254" y="200.66"/>
<pinref part="IC4" gate="B" pin="I2"/>
<wire x1="254" y1="198.12" x2="254" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC4" gate="B" pin="O"/>
<wire x1="269.24" y1="200.66" x2="271.78" y2="200.66" width="0.1524" layer="91"/>
<wire x1="271.78" y1="200.66" x2="271.78" y2="165.1" width="0.1524" layer="91"/>
<pinref part="IC4" gate="C" pin="I0"/>
<wire x1="226.06" y1="165.1" x2="271.78" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CHILLER" class="0">
<segment>
<wire x1="195.58" y1="177.8" x2="198.12" y2="175.26" width="0.1524" layer="91"/>
<wire x1="198.12" y1="175.26" x2="228.6" y2="175.26" width="0.1524" layer="91"/>
<wire x1="228.6" y1="175.26" x2="228.6" y2="170.18" width="0.1524" layer="91"/>
<pinref part="IC4" gate="C" pin="I1"/>
<wire x1="228.6" y1="170.18" x2="228.6" y2="167.64" width="0.1524" layer="91"/>
<wire x1="228.6" y1="167.64" x2="226.06" y2="167.64" width="0.1524" layer="91"/>
<pinref part="IC4" gate="C" pin="I2"/>
<wire x1="226.06" y1="170.18" x2="228.6" y2="170.18" width="0.1524" layer="91"/>
<junction x="228.6" y="170.18"/>
<label x="199.136" y="175.514" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PD3-PCINT19-OC2B-INT1"/>
<wire x1="172.72" y1="170.18" x2="193.04" y2="170.18" width="0.1524" layer="91"/>
<wire x1="193.04" y1="170.18" x2="195.58" y2="172.72" width="0.1524" layer="91"/>
<label x="175.26" y="170.434" size="1.4224" layer="95"/>
</segment>
</net>
<net name="LASERDIS" class="0">
<segment>
<pinref part="IC4" gate="C" pin="O"/>
<wire x1="210.82" y1="167.64" x2="198.12" y2="167.64" width="0.1524" layer="91"/>
<wire x1="198.12" y1="167.64" x2="195.58" y2="170.18" width="0.1524" layer="91"/>
<label x="199.136" y="167.894" size="1.4224" layer="95"/>
</segment>
</net>
<net name="STEPY" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB1-PCINT1-OC1A"/>
<wire x1="172.72" y1="218.44" x2="193.04" y2="218.44" width="0.1524" layer="91"/>
<wire x1="193.04" y1="218.44" x2="195.58" y2="220.98" width="0.1524" layer="91"/>
<label x="175.26" y="218.694" size="1.4224" layer="95"/>
</segment>
</net>
<net name="MISO-DIRY" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB4-PCINT4-MISO"/>
<wire x1="172.72" y1="210.82" x2="193.04" y2="210.82" width="0.1524" layer="91"/>
<wire x1="193.04" y1="210.82" x2="195.58" y2="213.36" width="0.1524" layer="91"/>
<label x="175.26" y="211.074" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="CON2" gate="G$1" pin="MISO"/>
<wire x1="104.14" y1="157.48" x2="121.92" y2="157.48" width="0.1524" layer="91"/>
<wire x1="121.92" y1="157.48" x2="124.46" y2="160.02" width="0.1524" layer="91"/>
<label x="109.22" y="157.734" size="1.4224" layer="95"/>
</segment>
</net>
<net name="SCK-DIRZ" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB5-PCINT5-SCK"/>
<wire x1="172.72" y1="208.28" x2="193.04" y2="208.28" width="0.1524" layer="91"/>
<wire x1="193.04" y1="208.28" x2="195.58" y2="210.82" width="0.1524" layer="91"/>
<label x="175.26" y="208.534" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="CON2" gate="G$1" pin="SCK"/>
<wire x1="104.14" y1="152.4" x2="121.92" y2="152.4" width="0.1524" layer="91"/>
<wire x1="121.92" y1="152.4" x2="124.46" y2="154.94" width="0.1524" layer="91"/>
<label x="109.22" y="152.654" size="1.4224" layer="95"/>
</segment>
</net>
<net name="STEPZ" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB2-PCINT2-/SS-OC1B"/>
<wire x1="172.72" y1="215.9" x2="193.04" y2="215.9" width="0.1524" layer="91"/>
<wire x1="193.04" y1="215.9" x2="195.58" y2="218.44" width="0.1524" layer="91"/>
<label x="175.26" y="216.154" size="1.4224" layer="95"/>
</segment>
</net>
<net name="AIR" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PD4-PCINT20-XCK-T0"/>
<wire x1="172.72" y1="167.64" x2="193.04" y2="167.64" width="0.1524" layer="91"/>
<wire x1="193.04" y1="167.64" x2="195.58" y2="170.18" width="0.1524" layer="91"/>
<label x="175.26" y="167.894" size="1.4224" layer="95"/>
</segment>
</net>
<net name="AUX1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PD7-PCINT23-AIN1"/>
<wire x1="172.72" y1="160.02" x2="193.04" y2="160.02" width="0.1524" layer="91"/>
<wire x1="193.04" y1="160.02" x2="195.58" y2="162.56" width="0.1524" layer="91"/>
<label x="175.26" y="160.274" size="1.4224" layer="95"/>
</segment>
</net>
<net name="PE" class="0">
<segment>
<pinref part="PE1" gate="M" pin="PE"/>
<pinref part="CON4" gate="G$2" pin="FGND"/>
<wire x1="104.14" y1="12.7" x2="104.14" y2="17.78" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$3" pin="FGND"/>
<wire x1="104.14" y1="17.78" x2="109.22" y2="17.78" width="0.1524" layer="91"/>
<junction x="104.14" y="17.78"/>
<pinref part="CON3" gate="G$2" pin="FGND"/>
<wire x1="109.22" y1="17.78" x2="114.3" y2="17.78" width="0.1524" layer="91"/>
<junction x="109.22" y="17.78"/>
<pinref part="CON3" gate="G$3" pin="FGND"/>
<wire x1="114.3" y1="17.78" x2="119.38" y2="17.78" width="0.1524" layer="91"/>
<junction x="114.3" y="17.78"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="1"/>
<pinref part="PE5" gate="M" pin="PE"/>
<wire x1="147.32" y1="30.48" x2="149.86" y2="30.48" width="0.1524" layer="91"/>
<wire x1="149.86" y1="30.48" x2="149.86" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="149.86" y1="22.86" x2="149.86" y2="20.32" width="0.1524" layer="91"/>
<wire x1="147.32" y1="22.86" x2="149.86" y2="22.86" width="0.1524" layer="91"/>
<junction x="149.86" y="22.86"/>
</segment>
<segment>
<wire x1="30.48" y1="132.08" x2="35.56" y2="132.08" width="0.1524" layer="91"/>
<pinref part="PE6" gate="M" pin="PE"/>
<wire x1="35.56" y1="132.08" x2="35.56" y2="129.54" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="3"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="VIN"/>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="50.8" y1="99.06" x2="35.56" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="35.56" y1="99.06" x2="27.94" y2="99.06" width="0.1524" layer="91"/>
<junction x="35.56" y="99.06"/>
<pinref part="P+28" gate="1" pin="+24V"/>
<wire x1="27.94" y1="101.6" x2="27.94" y2="99.06" width="0.1524" layer="91"/>
<junction x="27.94" y="99.06"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="K"/>
<wire x1="45.72" y1="142.24" x2="48.26" y2="142.24" width="0.1524" layer="91"/>
<pinref part="P+17" gate="1" pin="+24V"/>
<wire x1="48.26" y1="142.24" x2="48.26" y2="144.78" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="48.26" y1="142.24" x2="60.96" y2="142.24" width="0.1524" layer="91"/>
<junction x="48.26" y="142.24"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="C37" gate="G$1" pin="1"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="38.1" y1="78.74" x2="38.1" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="COMP" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="COMP"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="50.8" y1="88.9" x2="38.1" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RT" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="RT"/>
<wire x1="50.8" y1="83.82" x2="48.26" y2="83.82" width="0.1524" layer="91"/>
<wire x1="48.26" y1="83.82" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="BOOT" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="BOOT"/>
<pinref part="C44" gate="G$1" pin="2"/>
</segment>
</net>
<net name="SW" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="SW"/>
<pinref part="D23" gate="G$1" pin="K"/>
<wire x1="76.2" y1="91.44" x2="86.36" y2="91.44" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="1"/>
<wire x1="86.36" y1="91.44" x2="83.82" y2="91.44" width="0.1524" layer="91"/>
<junction x="86.36" y="91.44"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="86.36" y1="99.06" x2="86.36" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FB" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="R98" gate="G$1" pin="2"/>
<wire x1="121.92" y1="81.28" x2="114.3" y2="81.28" width="0.1524" layer="91"/>
<junction x="121.92" y="81.28"/>
<wire x1="114.3" y1="81.28" x2="114.3" y2="73.66" width="0.1524" layer="91"/>
<wire x1="114.3" y1="73.66" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
<wire x1="81.28" y1="73.66" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="FB"/>
<wire x1="81.28" y1="83.82" x2="76.2" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C90" gate="G$1" pin="1"/>
<wire x1="121.92" y1="81.28" x2="129.54" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="30.48" y1="142.24" x2="35.56" y2="142.24" width="0.1524" layer="91"/>
<pinref part="CON5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<pinref part="LED22" gate="G$1" pin="A"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="0" y="0"/>
<instance part="L1" gate="D" x="254" y="246.38"/>
<instance part="L1" gate="C" x="254" y="243.84"/>
<instance part="L1" gate="B" x="254" y="241.3"/>
<instance part="L1" gate="A" x="254" y="238.76"/>
<instance part="L2" gate="D" x="254" y="236.22"/>
<instance part="L2" gate="C" x="254" y="233.68"/>
<instance part="L2" gate="B" x="254" y="231.14"/>
<instance part="L2" gate="A" x="254" y="228.6"/>
<instance part="L3" gate="D" x="254" y="226.06"/>
<instance part="GND10" gate="1" x="347.98" y="205.74"/>
<instance part="L3" gate="C" x="254" y="223.52"/>
<instance part="L3" gate="B" x="254" y="220.98"/>
<instance part="L3" gate="A" x="254" y="218.44"/>
<instance part="GND11" gate="1" x="231.14" y="185.42"/>
<instance part="GND12" gate="1" x="198.12" y="185.42"/>
<instance part="CON3" gate="G$1" x="355.6" y="114.3"/>
<instance part="C13" gate="G$1" x="350.52" y="198.12" rot="MR0"/>
<instance part="C14" gate="G$1" x="358.14" y="193.04" rot="MR0"/>
<instance part="C15" gate="G$1" x="350.52" y="187.96" rot="MR0"/>
<instance part="C16" gate="G$1" x="358.14" y="182.88" rot="MR0"/>
<instance part="C17" gate="G$1" x="350.52" y="177.8" rot="MR0"/>
<instance part="C18" gate="G$1" x="358.14" y="172.72" rot="MR0"/>
<instance part="C19" gate="G$1" x="350.52" y="167.64" rot="MR0"/>
<instance part="C22" gate="G$1" x="358.14" y="162.56" rot="MR0"/>
<instance part="C23" gate="G$1" x="350.52" y="157.48" rot="MR0"/>
<instance part="C24" gate="G$1" x="358.14" y="152.4" rot="MR0"/>
<instance part="PE4" gate="M" x="365.76" y="137.16" rot="MR0"/>
<instance part="L4" gate="D" x="279.4" y="132.08" smashed="yes">
<attribute name="NAME" x="277.114" y="131.318" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="128.27" size="1.778" layer="96"/>
</instance>
<instance part="L4" gate="C" x="279.4" y="129.54" smashed="yes">
<attribute name="NAME" x="277.114" y="128.778" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="125.73" size="1.778" layer="96"/>
</instance>
<instance part="L4" gate="B" x="279.4" y="127" smashed="yes">
<attribute name="NAME" x="277.114" y="126.238" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="123.19" size="1.778" layer="96"/>
</instance>
<instance part="L4" gate="A" x="279.4" y="124.46" smashed="yes">
<attribute name="NAME" x="277.114" y="123.698" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="120.65" size="1.778" layer="96"/>
</instance>
<instance part="L5" gate="D" x="279.4" y="121.92" smashed="yes">
<attribute name="NAME" x="277.114" y="121.158" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="118.11" size="1.778" layer="96"/>
</instance>
<instance part="L5" gate="C" x="279.4" y="119.38" smashed="yes">
<attribute name="NAME" x="276.86" y="118.618" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="115.57" size="1.778" layer="96"/>
</instance>
<instance part="L5" gate="B" x="279.4" y="116.84" smashed="yes">
<attribute name="NAME" x="277.114" y="116.078" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="113.03" size="1.778" layer="96"/>
</instance>
<instance part="L5" gate="A" x="279.4" y="114.3" smashed="yes">
<attribute name="NAME" x="277.114" y="113.538" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="110.49" size="1.778" layer="96"/>
</instance>
<instance part="L6" gate="D" x="279.4" y="111.76" smashed="yes">
<attribute name="NAME" x="277.114" y="110.998" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="107.95" size="1.778" layer="96"/>
</instance>
<instance part="L6" gate="C" x="279.4" y="109.22" smashed="yes">
<attribute name="NAME" x="277.114" y="108.458" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="105.41" size="1.778" layer="96"/>
</instance>
<instance part="L6" gate="B" x="279.4" y="106.68" smashed="yes">
<attribute name="NAME" x="277.114" y="105.918" size="1.778" layer="95"/>
<attribute name="VALUE" x="276.86" y="102.87" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="345.44" y="91.44"/>
<instance part="P+6" gate="1" x="254" y="139.7"/>
<instance part="L6" gate="A" x="294.64" y="101.6" smashed="yes">
<attribute name="NAME" x="292.354" y="100.838" size="1.778" layer="95"/>
<attribute name="VALUE" x="292.1" y="97.79" size="1.778" layer="96"/>
</instance>
<instance part="C25" gate="G$1" x="350.52" y="76.2" rot="MR0"/>
<instance part="C26" gate="G$1" x="358.14" y="71.12" rot="MR0"/>
<instance part="C27" gate="G$1" x="350.52" y="66.04" rot="MR0"/>
<instance part="C28" gate="G$1" x="358.14" y="60.96" rot="MR0"/>
<instance part="C29" gate="G$1" x="350.52" y="55.88" rot="MR0"/>
<instance part="C30" gate="G$1" x="358.14" y="50.8" rot="MR0"/>
<instance part="C31" gate="G$1" x="350.52" y="45.72" rot="MR0"/>
<instance part="C32" gate="G$1" x="358.14" y="40.64" rot="MR0"/>
<instance part="C33" gate="G$1" x="350.52" y="35.56" rot="MR0"/>
<instance part="C34" gate="G$1" x="350.52" y="147.32" rot="MR0"/>
<instance part="C35" gate="G$1" x="358.14" y="142.24" rot="MR0"/>
<instance part="R4" gate="D" x="119.38" y="127" smashed="yes">
<attribute name="NAME" x="121.92" y="125.984" size="1.778" layer="95"/>
<attribute name="VALUE" x="117.348" y="127.254" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="C" x="127" y="124.46" smashed="yes">
<attribute name="NAME" x="129.54" y="123.698" size="1.778" layer="95"/>
<attribute name="VALUE" x="135.636" y="124.714" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="B" x="119.38" y="121.92" smashed="yes">
<attribute name="NAME" x="121.92" y="120.904" size="1.778" layer="95"/>
<attribute name="VALUE" x="116.84" y="122.174" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="A" x="127" y="119.38" smashed="yes">
<attribute name="NAME" x="129.794" y="118.618" size="1.778" layer="95"/>
<attribute name="VALUE" x="135.382" y="119.634" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="D" x="119.38" y="116.84" smashed="yes">
<attribute name="NAME" x="122.174" y="115.824" size="1.778" layer="95"/>
<attribute name="VALUE" x="116.84" y="117.094" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="C" x="127" y="114.3" smashed="yes">
<attribute name="NAME" x="129.794" y="113.284" size="1.778" layer="95"/>
<attribute name="VALUE" x="135.382" y="114.554" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="B" x="119.38" y="111.76" smashed="yes">
<attribute name="NAME" x="121.92" y="110.744" size="1.778" layer="95"/>
<attribute name="VALUE" x="116.84" y="112.014" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="A" x="127" y="109.22" smashed="yes">
<attribute name="NAME" x="129.54" y="108.204" size="1.778" layer="95"/>
<attribute name="VALUE" x="135.382" y="109.474" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="236.22" y="78.74"/>
<instance part="GND16" gate="1" x="203.2" y="78.74"/>
<instance part="GND17" gate="1" x="170.18" y="78.74"/>
<instance part="IC12" gate="G$1" x="111.76" y="220.98"/>
<instance part="IC13" gate="G$1" x="99.06" y="101.6"/>
<instance part="GND18" gate="1" x="114.3" y="83.82"/>
<instance part="GND19" gate="1" x="93.98" y="190.5"/>
<instance part="GND20" gate="1" x="81.28" y="71.12"/>
<instance part="P+14" gate="1" x="88.9" y="205.74"/>
<instance part="IC12" gate="P" x="210.82" y="25.4" smashed="yes">
<attribute name="NAME" x="210.82" y="24.13" size="1.778" layer="95"/>
</instance>
<instance part="IC13" gate="P" x="256.54" y="25.4" smashed="yes">
<attribute name="NAME" x="256.54" y="24.13" size="1.778" layer="95"/>
</instance>
<instance part="GND21" gate="1" x="193.04" y="7.62"/>
<instance part="P+15" gate="1" x="203.2" y="43.18"/>
<instance part="C36" gate="G$1" x="203.2" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="205.232" y="26.67" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="205.232" y="19.05" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C38" gate="G$1" x="208.28" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="210.566" y="26.416" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="210.058" y="18.542" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C40" gate="G$1" x="213.36" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="215.646" y="26.416" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="215.138" y="18.288" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C42" gate="G$1" x="218.44" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="220.472" y="26.67" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="220.472" y="18.034" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C43" gate="G$1" x="248.92" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="250.952" y="26.67" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="250.952" y="18.542" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C46" gate="G$1" x="254" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="256.54" y="26.924" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="256.286" y="18.288" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C47" gate="G$1" x="259.08" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="261.112" y="26.67" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="260.858" y="19.05" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C48" gate="G$1" x="264.16" y="25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="266.192" y="26.924" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="266.192" y="18.542" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SJ1" gate="G$1" x="269.24" y="88.9"/>
<instance part="P+16" gate="1" x="269.24" y="96.52"/>
<instance part="GND23" gate="1" x="269.24" y="81.28"/>
<instance part="F1" gate="G$1" x="264.16" y="134.62"/>
<instance part="R10" gate="G$1" x="35.56" y="66.04"/>
<instance part="R11" gate="G$1" x="35.56" y="60.96"/>
<instance part="R12" gate="G$1" x="35.56" y="55.88"/>
<instance part="R13" gate="G$1" x="35.56" y="50.8"/>
<instance part="R14" gate="G$1" x="35.56" y="45.72"/>
<instance part="R15" gate="G$1" x="35.56" y="40.64"/>
<instance part="R16" gate="G$1" x="35.56" y="35.56"/>
<instance part="R17" gate="G$1" x="35.56" y="30.48"/>
<instance part="R18" gate="G$1" x="35.56" y="25.4"/>
<instance part="R19" gate="G$1" x="35.56" y="20.32"/>
<instance part="R20" gate="G$1" x="35.56" y="15.24"/>
<instance part="LED1" gate="G$1" x="33.02" y="66.04" rot="R270"/>
<instance part="LED2" gate="G$1" x="33.02" y="60.96" rot="R270"/>
<instance part="LED3" gate="G$1" x="33.02" y="55.88" rot="R270"/>
<instance part="LED4" gate="G$1" x="33.02" y="50.8" rot="R270"/>
<instance part="LED5" gate="G$1" x="33.02" y="45.72" rot="R270"/>
<instance part="LED6" gate="G$1" x="33.02" y="40.64" rot="R270"/>
<instance part="LED7" gate="G$1" x="33.02" y="35.56" rot="R270"/>
<instance part="LED8" gate="G$1" x="33.02" y="30.48" rot="R270"/>
<instance part="LED9" gate="G$1" x="33.02" y="25.4" rot="R270"/>
<instance part="LED10" gate="G$1" x="33.02" y="20.32" rot="R270"/>
<instance part="LED11" gate="G$1" x="33.02" y="15.24" rot="R270"/>
<instance part="GND24" gate="1" x="25.4" y="10.16"/>
<instance part="R22" gate="G$1" x="99.06" y="66.04" rot="MR0"/>
<instance part="R24" gate="G$1" x="99.06" y="60.96" rot="MR0"/>
<instance part="R26" gate="G$1" x="99.06" y="55.88" rot="MR0"/>
<instance part="R27" gate="G$1" x="99.06" y="50.8" rot="MR0"/>
<instance part="R28" gate="G$1" x="99.06" y="45.72" rot="MR0"/>
<instance part="R29" gate="G$1" x="99.06" y="40.64" rot="MR0"/>
<instance part="R30" gate="G$1" x="99.06" y="35.56" rot="MR0"/>
<instance part="R31" gate="G$1" x="99.06" y="30.48" rot="MR0"/>
<instance part="R32" gate="G$1" x="99.06" y="25.4" rot="MR0"/>
<instance part="R33" gate="G$1" x="99.06" y="20.32" rot="MR0"/>
<instance part="LED12" gate="G$1" x="101.6" y="66.04" rot="MR270"/>
<instance part="LED13" gate="G$1" x="101.6" y="60.96" rot="MR270"/>
<instance part="LED14" gate="G$1" x="101.6" y="55.88" rot="MR270"/>
<instance part="LED15" gate="G$1" x="101.6" y="50.8" rot="MR270"/>
<instance part="LED16" gate="G$1" x="101.6" y="45.72" rot="MR270"/>
<instance part="LED17" gate="G$1" x="101.6" y="40.64" rot="MR270"/>
<instance part="LED18" gate="G$1" x="101.6" y="35.56" rot="MR270"/>
<instance part="LED19" gate="G$1" x="101.6" y="30.48" rot="MR270"/>
<instance part="LED20" gate="G$1" x="101.6" y="25.4" rot="MR270"/>
<instance part="LED21" gate="G$1" x="101.6" y="20.32" rot="MR270"/>
<instance part="GND25" gate="1" x="109.22" y="10.16" rot="MR0"/>
<instance part="U$3" gate="G$1" x="144.78" y="180.34"/>
<instance part="U$4" gate="G$1" x="177.8" y="180.34"/>
<instance part="U$5" gate="G$1" x="210.82" y="180.34"/>
<instance part="U$6" gate="G$1" x="215.9" y="73.66"/>
<instance part="U$7" gate="G$1" x="182.88" y="73.66"/>
<instance part="U$8" gate="G$1" x="149.86" y="73.66"/>
<instance part="GND28" gate="1" x="144.78" y="78.74"/>
<instance part="GND29" gate="1" x="177.8" y="78.74"/>
<instance part="GND30" gate="1" x="210.82" y="78.74"/>
<instance part="GND31" gate="1" x="205.74" y="185.42"/>
<instance part="GND33" gate="1" x="139.7" y="185.42"/>
<instance part="CON4" gate="G$1" x="358.14" y="228.6" rot="MR180"/>
<instance part="R35" gate="G$1" x="124.46" y="106.68" smashed="yes">
<attribute name="NAME" x="127.254" y="105.918" size="1.778" layer="95"/>
<attribute name="VALUE" x="121.92" y="106.934" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="165.1" y="185.42"/>
<instance part="GND32" gate="1" x="172.72" y="185.42"/>
<instance part="R6" gate="G$1" x="241.3" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="240.538" y="96.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="239.268" y="101.346" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R7" gate="G$1" x="243.84" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="243.078" y="96.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="241.808" y="101.092" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R8" gate="G$1" x="246.38" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="245.618" y="96.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="244.348" y="101.092" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R9" gate="G$1" x="248.92" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="248.158" y="96.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="246.888" y="101.092" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R36" gate="G$1" x="251.46" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="250.698" y="96.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="249.428" y="101.092" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R37" gate="G$1" x="254" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="253.238" y="96.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="251.968" y="101.092" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R38" gate="G$1" x="256.54" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="255.778" y="96.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="254.508" y="101.092" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R39" gate="G$1" x="259.08" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="258.318" y="96.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="257.048" y="101.092" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R40" gate="G$1" x="261.62" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="260.858" y="96.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="259.588" y="101.092" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C2" gate="G$1" x="358.14" y="81.28" rot="MR0"/>
<instance part="C12" gate="G$1" x="350.52" y="86.36" rot="MR0"/>
<instance part="PE3" gate="M" x="365.76" y="27.94"/>
<instance part="OC23" gate="G$1" x="276.86" y="68.58" rot="MR0"/>
<instance part="R41" gate="G" x="289.56" y="81.28" rot="R90"/>
<instance part="D2" gate="G$1" x="297.18" y="71.12" rot="R90"/>
<instance part="LED23" gate="G$1" x="289.56" y="71.12" rot="MR0"/>
<instance part="GND1" gate="1" x="264.16" y="60.96"/>
</instances>
<busses>
<bus name="BUS:RXD,TXD,MISO-DIRY,MOSI-DIRX,SCK-DIRZ,/RST,X1,X2,Y1,Y2,Z1,Z2,STEPX,STEPY,STEPZ,DIRX,DIRY,DIRZ,DOOR1,DOOR2,LASER1,LASER2,CHILLER,LASERDIS,DOOR,AIR,AUX1,XT1,XT2,STEPEN">
<segment>
<wire x1="22.86" y1="254" x2="60.96" y2="254" width="0.762" layer="92"/>
<wire x1="60.96" y1="254" x2="63.5" y2="251.46" width="0.762" layer="92"/>
<wire x1="63.5" y1="251.46" x2="63.5" y2="17.78" width="0.762" layer="92"/>
<label x="23.368" y="255.016" size="1.778" layer="95"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="CON3" gate="G$1" pin="15"/>
<wire x1="345.44" y1="99.06" x2="345.44" y2="96.52" width="0.1524" layer="91"/>
<wire x1="345.44" y1="96.52" x2="345.44" y2="93.98" width="0.1524" layer="91"/>
<wire x1="345.44" y1="99.06" x2="347.98" y2="99.06" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="8"/>
<wire x1="345.44" y1="96.52" x2="347.98" y2="96.52" width="0.1524" layer="91"/>
<junction x="345.44" y="96.52"/>
</segment>
<segment>
<pinref part="IC13" gate="G$1" pin="2B4"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="111.76" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<wire x1="114.3" y1="99.06" x2="114.3" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC13" gate="G$1" pin="2B5"/>
<wire x1="114.3" y1="96.52" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<wire x1="114.3" y1="93.98" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="91.44" x2="114.3" y2="88.9" width="0.1524" layer="91"/>
<wire x1="114.3" y1="88.9" x2="114.3" y2="86.36" width="0.1524" layer="91"/>
<wire x1="111.76" y1="96.52" x2="114.3" y2="96.52" width="0.1524" layer="91"/>
<junction x="114.3" y="96.52"/>
<pinref part="IC13" gate="G$1" pin="2B6"/>
<wire x1="111.76" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<junction x="114.3" y="93.98"/>
<pinref part="IC13" gate="G$1" pin="2B7"/>
<wire x1="111.76" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<junction x="114.3" y="91.44"/>
<pinref part="IC13" gate="G$1" pin="2B8"/>
<wire x1="111.76" y1="88.9" x2="114.3" y2="88.9" width="0.1524" layer="91"/>
<junction x="114.3" y="88.9"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="2A5"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="96.52" y1="215.9" x2="93.98" y2="215.9" width="0.1524" layer="91"/>
<wire x1="93.98" y1="215.9" x2="93.98" y2="213.36" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="2A6"/>
<wire x1="93.98" y1="213.36" x2="93.98" y2="210.82" width="0.1524" layer="91"/>
<wire x1="93.98" y1="210.82" x2="93.98" y2="208.28" width="0.1524" layer="91"/>
<wire x1="93.98" y1="208.28" x2="93.98" y2="203.2" width="0.1524" layer="91"/>
<wire x1="93.98" y1="203.2" x2="93.98" y2="198.12" width="0.1524" layer="91"/>
<wire x1="93.98" y1="198.12" x2="93.98" y2="193.04" width="0.1524" layer="91"/>
<wire x1="96.52" y1="213.36" x2="93.98" y2="213.36" width="0.1524" layer="91"/>
<junction x="93.98" y="213.36"/>
<pinref part="IC12" gate="G$1" pin="2A7"/>
<wire x1="93.98" y1="210.82" x2="96.52" y2="210.82" width="0.1524" layer="91"/>
<junction x="93.98" y="210.82"/>
<pinref part="IC12" gate="G$1" pin="2A8"/>
<wire x1="96.52" y1="208.28" x2="93.98" y2="208.28" width="0.1524" layer="91"/>
<junction x="93.98" y="208.28"/>
<pinref part="IC12" gate="G$1" pin="1G"/>
<wire x1="96.52" y1="203.2" x2="93.98" y2="203.2" width="0.1524" layer="91"/>
<junction x="93.98" y="203.2"/>
<pinref part="IC12" gate="G$1" pin="2G"/>
<wire x1="96.52" y1="198.12" x2="93.98" y2="198.12" width="0.1524" layer="91"/>
<junction x="93.98" y="198.12"/>
</segment>
<segment>
<pinref part="IC13" gate="G$1" pin="1DIR"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="83.82" y1="81.28" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
<wire x1="81.28" y1="81.28" x2="81.28" y2="78.74" width="0.1524" layer="91"/>
<pinref part="IC13" gate="G$1" pin="2DIR"/>
<wire x1="81.28" y1="78.74" x2="81.28" y2="76.2" width="0.1524" layer="91"/>
<wire x1="81.28" y1="76.2" x2="81.28" y2="73.66" width="0.1524" layer="91"/>
<wire x1="83.82" y1="76.2" x2="81.28" y2="76.2" width="0.1524" layer="91"/>
<junction x="81.28" y="76.2"/>
<pinref part="IC13" gate="G$1" pin="1G"/>
<wire x1="83.82" y1="83.82" x2="81.28" y2="83.82" width="0.1524" layer="91"/>
<wire x1="81.28" y1="83.82" x2="81.28" y2="81.28" width="0.1524" layer="91"/>
<junction x="81.28" y="81.28"/>
<pinref part="IC13" gate="G$1" pin="2G"/>
<wire x1="83.82" y1="78.74" x2="81.28" y2="78.74" width="0.1524" layer="91"/>
<junction x="81.28" y="78.74"/>
</segment>
<segment>
<pinref part="GND21" gate="1" pin="GND"/>
<pinref part="IC12" gate="P" pin="GND@1"/>
<wire x1="193.04" y1="10.16" x2="193.04" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC12" gate="P" pin="GND@2"/>
<wire x1="193.04" y1="15.24" x2="198.12" y2="15.24" width="0.1524" layer="91"/>
<junction x="193.04" y="15.24"/>
<pinref part="IC12" gate="P" pin="GND@3"/>
<wire x1="198.12" y1="15.24" x2="203.2" y2="15.24" width="0.1524" layer="91"/>
<junction x="198.12" y="15.24"/>
<pinref part="IC12" gate="P" pin="GND@4"/>
<wire x1="203.2" y1="15.24" x2="208.28" y2="15.24" width="0.1524" layer="91"/>
<junction x="203.2" y="15.24"/>
<pinref part="IC12" gate="P" pin="GND@5"/>
<wire x1="208.28" y1="15.24" x2="213.36" y2="15.24" width="0.1524" layer="91"/>
<junction x="208.28" y="15.24"/>
<pinref part="IC12" gate="P" pin="GND@6"/>
<wire x1="213.36" y1="15.24" x2="218.44" y2="15.24" width="0.1524" layer="91"/>
<junction x="213.36" y="15.24"/>
<pinref part="IC12" gate="P" pin="GND@7"/>
<wire x1="218.44" y1="15.24" x2="223.52" y2="15.24" width="0.1524" layer="91"/>
<junction x="218.44" y="15.24"/>
<pinref part="IC12" gate="P" pin="GND@8"/>
<wire x1="223.52" y1="15.24" x2="228.6" y2="15.24" width="0.1524" layer="91"/>
<junction x="223.52" y="15.24"/>
<pinref part="IC13" gate="P" pin="GND@1"/>
<wire x1="228.6" y1="15.24" x2="238.76" y2="15.24" width="0.1524" layer="91"/>
<junction x="228.6" y="15.24"/>
<pinref part="IC13" gate="P" pin="GND@2"/>
<wire x1="238.76" y1="15.24" x2="243.84" y2="15.24" width="0.1524" layer="91"/>
<junction x="238.76" y="15.24"/>
<pinref part="IC13" gate="P" pin="GND@3"/>
<wire x1="243.84" y1="15.24" x2="248.92" y2="15.24" width="0.1524" layer="91"/>
<junction x="243.84" y="15.24"/>
<pinref part="IC13" gate="P" pin="GND@4"/>
<wire x1="248.92" y1="15.24" x2="254" y2="15.24" width="0.1524" layer="91"/>
<junction x="248.92" y="15.24"/>
<pinref part="IC13" gate="P" pin="GND@5"/>
<wire x1="254" y1="15.24" x2="259.08" y2="15.24" width="0.1524" layer="91"/>
<junction x="254" y="15.24"/>
<pinref part="IC13" gate="P" pin="GND@6"/>
<wire x1="259.08" y1="15.24" x2="264.16" y2="15.24" width="0.1524" layer="91"/>
<junction x="259.08" y="15.24"/>
<pinref part="IC13" gate="P" pin="GND@7"/>
<wire x1="264.16" y1="15.24" x2="269.24" y2="15.24" width="0.1524" layer="91"/>
<junction x="264.16" y="15.24"/>
<pinref part="IC13" gate="P" pin="GND@8"/>
<wire x1="269.24" y1="15.24" x2="274.32" y2="15.24" width="0.1524" layer="91"/>
<junction x="269.24" y="15.24"/>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="264.16" y1="20.32" x2="264.16" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="259.08" y1="15.24" x2="259.08" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="254" y1="20.32" x2="254" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="248.92" y1="15.24" x2="248.92" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="218.44" y1="20.32" x2="218.44" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="213.36" y1="15.24" x2="213.36" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="208.28" y1="20.32" x2="208.28" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="203.2" y1="15.24" x2="203.2" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SJ1" gate="G$1" pin="3"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<wire x1="27.94" y1="66.04" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="25.4" y1="66.04" x2="25.4" y2="60.96" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="25.4" y1="60.96" x2="25.4" y2="55.88" width="0.1524" layer="91"/>
<wire x1="25.4" y1="55.88" x2="25.4" y2="50.8" width="0.1524" layer="91"/>
<wire x1="25.4" y1="50.8" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="25.4" y1="45.72" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="40.64" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<wire x1="25.4" y1="35.56" x2="25.4" y2="30.48" width="0.1524" layer="91"/>
<wire x1="25.4" y1="30.48" x2="25.4" y2="25.4" width="0.1524" layer="91"/>
<wire x1="25.4" y1="25.4" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
<wire x1="25.4" y1="20.32" x2="25.4" y2="15.24" width="0.1524" layer="91"/>
<wire x1="25.4" y1="15.24" x2="25.4" y2="12.7" width="0.1524" layer="91"/>
<wire x1="25.4" y1="60.96" x2="27.94" y2="60.96" width="0.1524" layer="91"/>
<junction x="25.4" y="60.96"/>
<pinref part="LED3" gate="G$1" pin="C"/>
<wire x1="27.94" y1="55.88" x2="25.4" y2="55.88" width="0.1524" layer="91"/>
<junction x="25.4" y="55.88"/>
<pinref part="LED4" gate="G$1" pin="C"/>
<wire x1="25.4" y1="50.8" x2="27.94" y2="50.8" width="0.1524" layer="91"/>
<junction x="25.4" y="50.8"/>
<pinref part="LED5" gate="G$1" pin="C"/>
<wire x1="27.94" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<junction x="25.4" y="45.72"/>
<pinref part="LED6" gate="G$1" pin="C"/>
<wire x1="25.4" y1="40.64" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<junction x="25.4" y="40.64"/>
<pinref part="LED7" gate="G$1" pin="C"/>
<wire x1="27.94" y1="35.56" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<junction x="25.4" y="35.56"/>
<pinref part="LED8" gate="G$1" pin="C"/>
<wire x1="25.4" y1="30.48" x2="27.94" y2="30.48" width="0.1524" layer="91"/>
<junction x="25.4" y="30.48"/>
<pinref part="LED9" gate="G$1" pin="C"/>
<wire x1="27.94" y1="25.4" x2="25.4" y2="25.4" width="0.1524" layer="91"/>
<junction x="25.4" y="25.4"/>
<pinref part="LED10" gate="G$1" pin="C"/>
<wire x1="25.4" y1="20.32" x2="27.94" y2="20.32" width="0.1524" layer="91"/>
<junction x="25.4" y="20.32"/>
<pinref part="LED11" gate="G$1" pin="C"/>
<wire x1="27.94" y1="15.24" x2="25.4" y2="15.24" width="0.1524" layer="91"/>
<junction x="25.4" y="15.24"/>
</segment>
<segment>
<pinref part="LED12" gate="G$1" pin="C"/>
<wire x1="106.68" y1="66.04" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="109.22" y1="66.04" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
<pinref part="LED13" gate="G$1" pin="C"/>
<wire x1="109.22" y1="60.96" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<wire x1="109.22" y1="55.88" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<wire x1="109.22" y1="50.8" x2="109.22" y2="45.72" width="0.1524" layer="91"/>
<wire x1="109.22" y1="45.72" x2="109.22" y2="40.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="40.64" x2="109.22" y2="35.56" width="0.1524" layer="91"/>
<wire x1="109.22" y1="35.56" x2="109.22" y2="30.48" width="0.1524" layer="91"/>
<wire x1="109.22" y1="30.48" x2="109.22" y2="25.4" width="0.1524" layer="91"/>
<wire x1="109.22" y1="25.4" x2="109.22" y2="20.32" width="0.1524" layer="91"/>
<wire x1="109.22" y1="20.32" x2="109.22" y2="12.7" width="0.1524" layer="91"/>
<wire x1="109.22" y1="60.96" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<junction x="109.22" y="60.96"/>
<pinref part="LED14" gate="G$1" pin="C"/>
<wire x1="106.68" y1="55.88" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<junction x="109.22" y="55.88"/>
<pinref part="LED15" gate="G$1" pin="C"/>
<wire x1="109.22" y1="50.8" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<junction x="109.22" y="50.8"/>
<pinref part="LED16" gate="G$1" pin="C"/>
<wire x1="106.68" y1="45.72" x2="109.22" y2="45.72" width="0.1524" layer="91"/>
<junction x="109.22" y="45.72"/>
<pinref part="LED17" gate="G$1" pin="C"/>
<wire x1="109.22" y1="40.64" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<junction x="109.22" y="40.64"/>
<pinref part="LED18" gate="G$1" pin="C"/>
<wire x1="106.68" y1="35.56" x2="109.22" y2="35.56" width="0.1524" layer="91"/>
<junction x="109.22" y="35.56"/>
<pinref part="LED19" gate="G$1" pin="C"/>
<wire x1="109.22" y1="30.48" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<junction x="109.22" y="30.48"/>
<pinref part="LED20" gate="G$1" pin="C"/>
<wire x1="106.68" y1="25.4" x2="109.22" y2="25.4" width="0.1524" layer="91"/>
<junction x="109.22" y="25.4"/>
<pinref part="LED21" gate="G$1" pin="C"/>
<wire x1="109.22" y1="20.32" x2="106.68" y2="20.32" width="0.1524" layer="91"/>
<junction x="109.22" y="20.32"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="U$4" gate="G$1" pin="A@2"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<pinref part="U$5" gate="G$1" pin="A@2"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="U$6" gate="G$1" pin="A@2"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="U$7" gate="G$1" pin="A@2"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="U$8" gate="G$1" pin="A@2"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="A@1"/>
<pinref part="GND28" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="A@1"/>
<pinref part="GND29" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="A@1"/>
<pinref part="GND30" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="A@1"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="A@1"/>
<pinref part="GND33" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CON4" gate="G$1" pin="2"/>
<wire x1="350.52" y1="215.9" x2="347.98" y2="215.9" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="347.98" y1="215.9" x2="347.98" y2="213.36" width="0.1524" layer="91"/>
<pinref part="CON4" gate="G$1" pin="9"/>
<wire x1="347.98" y1="213.36" x2="347.98" y2="210.82" width="0.1524" layer="91"/>
<wire x1="347.98" y1="210.82" x2="347.98" y2="208.28" width="0.1524" layer="91"/>
<wire x1="347.98" y1="213.36" x2="350.52" y2="213.36" width="0.1524" layer="91"/>
<junction x="347.98" y="213.36"/>
<pinref part="CON4" gate="G$1" pin="1"/>
<wire x1="350.52" y1="210.82" x2="347.98" y2="210.82" width="0.1524" layer="91"/>
<junction x="347.98" y="210.82"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="A@2"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="A@1"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="OC23" gate="G$1" pin="EMIT"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="266.7" y1="66.04" x2="264.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="264.16" y1="66.04" x2="264.16" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ESTEPX" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="345.44" y1="198.12" x2="332.74" y2="198.12" width="0.1524" layer="91"/>
<label x="342.646" y="198.628" size="1.4224" layer="95" rot="MR0"/>
<pinref part="CON4" gate="G$1" pin="14"/>
<pinref part="L1" gate="A" pin="2"/>
<wire x1="350.52" y1="238.76" x2="332.74" y2="238.76" width="0.1524" layer="91"/>
<label x="261.62" y="238.76" size="1.4224" layer="95"/>
<wire x1="332.74" y1="238.76" x2="259.08" y2="238.76" width="0.1524" layer="91"/>
<wire x1="332.74" y1="198.12" x2="332.74" y2="238.76" width="0.1524" layer="91"/>
<junction x="332.74" y="238.76"/>
</segment>
</net>
<net name="EDIRX" class="0">
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="353.06" y1="182.88" x2="325.12" y2="182.88" width="0.1524" layer="91"/>
<label x="342.646" y="183.388" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L2" gate="B" pin="2"/>
<pinref part="CON4" gate="G$1" pin="5"/>
<wire x1="259.08" y1="231.14" x2="325.12" y2="231.14" width="0.1524" layer="91"/>
<label x="261.62" y="231.14" size="1.4224" layer="95"/>
<wire x1="325.12" y1="231.14" x2="350.52" y2="231.14" width="0.1524" layer="91"/>
<wire x1="325.12" y1="182.88" x2="325.12" y2="231.14" width="0.1524" layer="91"/>
<junction x="325.12" y="231.14"/>
</segment>
</net>
<net name="ESTEPEN" class="0">
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="345.44" y1="167.64" x2="317.5" y2="167.64" width="0.1524" layer="91"/>
<label x="342.646" y="168.148" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L3" gate="B" pin="2"/>
<pinref part="CON4" gate="G$1" pin="3"/>
<wire x1="259.08" y1="220.98" x2="317.5" y2="220.98" width="0.1524" layer="91"/>
<label x="261.62" y="220.98" size="1.4224" layer="95"/>
<wire x1="317.5" y1="220.98" x2="350.52" y2="220.98" width="0.1524" layer="91"/>
<wire x1="317.5" y1="167.64" x2="317.5" y2="220.98" width="0.1524" layer="91"/>
<junction x="317.5" y="220.98"/>
</segment>
</net>
<net name="ESTEPY" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="353.06" y1="193.04" x2="330.2" y2="193.04" width="0.1524" layer="91"/>
<label x="342.646" y="193.548" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L2" gate="D" pin="2"/>
<pinref part="CON4" gate="G$1" pin="6"/>
<wire x1="259.08" y1="236.22" x2="330.2" y2="236.22" width="0.1524" layer="91"/>
<label x="261.62" y="236.22" size="1.4224" layer="95"/>
<wire x1="330.2" y1="236.22" x2="350.52" y2="236.22" width="0.1524" layer="91"/>
<wire x1="330.2" y1="193.04" x2="330.2" y2="236.22" width="0.1524" layer="91"/>
<junction x="330.2" y="236.22"/>
</segment>
</net>
<net name="EDIRY" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="345.44" y1="177.8" x2="322.58" y2="177.8" width="0.1524" layer="91"/>
<label x="342.646" y="178.308" size="1.4224" layer="95" rot="MR0"/>
<pinref part="CON4" gate="G$1" pin="12"/>
<pinref part="L2" gate="A" pin="2"/>
<wire x1="350.52" y1="228.6" x2="322.58" y2="228.6" width="0.1524" layer="91"/>
<label x="261.62" y="228.6" size="1.4224" layer="95"/>
<wire x1="322.58" y1="228.6" x2="259.08" y2="228.6" width="0.1524" layer="91"/>
<wire x1="322.58" y1="177.8" x2="322.58" y2="228.6" width="0.1524" layer="91"/>
<junction x="322.58" y="228.6"/>
</segment>
</net>
<net name="EDIRZ" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="353.06" y1="172.72" x2="320.04" y2="172.72" width="0.1524" layer="91"/>
<label x="342.646" y="173.228" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L3" gate="D" pin="2"/>
<pinref part="CON4" gate="G$1" pin="4"/>
<wire x1="259.08" y1="226.06" x2="320.04" y2="226.06" width="0.1524" layer="91"/>
<label x="261.62" y="226.06" size="1.4224" layer="95"/>
<wire x1="320.04" y1="226.06" x2="350.52" y2="226.06" width="0.1524" layer="91"/>
<wire x1="320.04" y1="172.72" x2="320.04" y2="226.06" width="0.1524" layer="91"/>
<junction x="320.04" y="226.06"/>
</segment>
</net>
<net name="ELASER1" class="0">
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="353.06" y1="162.56" x2="314.96" y2="162.56" width="0.1524" layer="91"/>
<label x="342.646" y="163.068" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L1" gate="D" pin="2"/>
<pinref part="CON4" gate="G$1" pin="8"/>
<wire x1="259.08" y1="246.38" x2="314.96" y2="246.38" width="0.1524" layer="91"/>
<label x="261.62" y="246.38" size="1.4224" layer="95"/>
<wire x1="314.96" y1="246.38" x2="350.52" y2="246.38" width="0.1524" layer="91"/>
<wire x1="314.96" y1="162.56" x2="314.96" y2="246.38" width="0.1524" layer="91"/>
<junction x="314.96" y="246.38"/>
</segment>
</net>
<net name="ELASER2" class="0">
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<label x="342.646" y="157.988" size="1.4224" layer="95" rot="MR0"/>
<pinref part="CON4" gate="G$1" pin="15"/>
<pinref part="L1" gate="C" pin="2"/>
<wire x1="350.52" y1="243.84" x2="312.42" y2="243.84" width="0.1524" layer="91"/>
<label x="261.62" y="243.84" size="1.4224" layer="95"/>
<wire x1="312.42" y1="243.84" x2="259.08" y2="243.84" width="0.1524" layer="91"/>
<wire x1="345.44" y1="157.48" x2="312.42" y2="157.48" width="0.1524" layer="91"/>
<wire x1="312.42" y1="157.48" x2="312.42" y2="243.84" width="0.1524" layer="91"/>
<junction x="312.42" y="243.84"/>
</segment>
</net>
<net name="ELASERDIS" class="0">
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="353.06" y1="152.4" x2="309.88" y2="152.4" width="0.1524" layer="91"/>
<label x="342.646" y="152.908" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L3" gate="C" pin="2"/>
<pinref part="CON4" gate="G$1" pin="11"/>
<wire x1="259.08" y1="223.52" x2="309.88" y2="223.52" width="0.1524" layer="91"/>
<label x="261.62" y="223.52" size="1.4224" layer="95"/>
<wire x1="309.88" y1="223.52" x2="350.52" y2="223.52" width="0.1524" layer="91"/>
<wire x1="309.88" y1="152.4" x2="309.88" y2="223.52" width="0.1524" layer="91"/>
<junction x="309.88" y="223.52"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="2DIR"/>
<pinref part="P+14" gate="1" pin="+5V"/>
<wire x1="96.52" y1="195.58" x2="88.9" y2="195.58" width="0.1524" layer="91"/>
<wire x1="88.9" y1="195.58" x2="88.9" y2="200.66" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="1DIR"/>
<wire x1="88.9" y1="200.66" x2="88.9" y2="203.2" width="0.1524" layer="91"/>
<wire x1="96.52" y1="200.66" x2="88.9" y2="200.66" width="0.1524" layer="91"/>
<junction x="88.9" y="200.66"/>
</segment>
<segment>
<pinref part="IC12" gate="P" pin="VCC@1"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="203.2" y1="35.56" x2="203.2" y2="30.48" width="0.1524" layer="91"/>
<pinref part="P+15" gate="1" pin="+5V"/>
<wire x1="203.2" y1="40.64" x2="203.2" y2="35.56" width="0.1524" layer="91"/>
<junction x="203.2" y="35.56"/>
<pinref part="IC12" gate="P" pin="VCC@2"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="208.28" y1="35.56" x2="208.28" y2="30.48" width="0.1524" layer="91"/>
<wire x1="203.2" y1="35.56" x2="208.28" y2="35.56" width="0.1524" layer="91"/>
<junction x="208.28" y="35.56"/>
<pinref part="C40" gate="G$1" pin="2"/>
<pinref part="IC12" gate="P" pin="VCC@3"/>
<wire x1="213.36" y1="30.48" x2="213.36" y2="35.56" width="0.1524" layer="91"/>
<wire x1="208.28" y1="35.56" x2="213.36" y2="35.56" width="0.1524" layer="91"/>
<junction x="213.36" y="35.56"/>
<pinref part="IC12" gate="P" pin="VCC@4"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="218.44" y1="35.56" x2="218.44" y2="30.48" width="0.1524" layer="91"/>
<wire x1="213.36" y1="35.56" x2="218.44" y2="35.56" width="0.1524" layer="91"/>
<junction x="218.44" y="35.56"/>
<pinref part="C43" gate="G$1" pin="2"/>
<pinref part="IC13" gate="P" pin="VCC@1"/>
<wire x1="248.92" y1="30.48" x2="248.92" y2="35.56" width="0.1524" layer="91"/>
<wire x1="218.44" y1="35.56" x2="248.92" y2="35.56" width="0.1524" layer="91"/>
<junction x="248.92" y="35.56"/>
<pinref part="IC13" gate="P" pin="VCC@2"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="254" y1="35.56" x2="254" y2="30.48" width="0.1524" layer="91"/>
<wire x1="248.92" y1="35.56" x2="254" y2="35.56" width="0.1524" layer="91"/>
<junction x="254" y="35.56"/>
<pinref part="C47" gate="G$1" pin="2"/>
<pinref part="IC13" gate="P" pin="VCC@3"/>
<wire x1="259.08" y1="30.48" x2="259.08" y2="35.56" width="0.1524" layer="91"/>
<wire x1="254" y1="35.56" x2="259.08" y2="35.56" width="0.1524" layer="91"/>
<junction x="259.08" y="35.56"/>
<pinref part="IC13" gate="P" pin="VCC@4"/>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="264.16" y1="35.56" x2="264.16" y2="30.48" width="0.1524" layer="91"/>
<wire x1="259.08" y1="35.56" x2="264.16" y2="35.56" width="0.1524" layer="91"/>
<junction x="264.16" y="35.56"/>
</segment>
<segment>
<pinref part="SJ1" gate="G$1" pin="1"/>
<pinref part="P+16" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="F1" gate="G$1" pin="1"/>
<pinref part="P+6" gate="1" pin="+5V"/>
<wire x1="256.54" y1="134.62" x2="254" y2="134.62" width="0.1524" layer="91"/>
<wire x1="254" y1="134.62" x2="254" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PE" class="0">
<segment>
<pinref part="PE4" gate="M" pin="PE"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="365.76" y1="147.32" x2="355.6" y2="147.32" width="0.1524" layer="91"/>
<wire x1="365.76" y1="147.32" x2="365.76" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="363.22" y1="142.24" x2="365.76" y2="142.24" width="0.1524" layer="91"/>
<junction x="365.76" y="142.24"/>
<wire x1="365.76" y1="142.24" x2="365.76" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="355.6" y1="198.12" x2="365.76" y2="198.12" width="0.1524" layer="91"/>
<wire x1="365.76" y1="198.12" x2="365.76" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="365.76" y1="193.04" x2="365.76" y2="187.96" width="0.1524" layer="91"/>
<wire x1="365.76" y1="187.96" x2="365.76" y2="182.88" width="0.1524" layer="91"/>
<wire x1="365.76" y1="182.88" x2="365.76" y2="177.8" width="0.1524" layer="91"/>
<wire x1="365.76" y1="177.8" x2="365.76" y2="172.72" width="0.1524" layer="91"/>
<wire x1="365.76" y1="172.72" x2="365.76" y2="167.64" width="0.1524" layer="91"/>
<wire x1="365.76" y1="167.64" x2="365.76" y2="162.56" width="0.1524" layer="91"/>
<wire x1="365.76" y1="162.56" x2="365.76" y2="157.48" width="0.1524" layer="91"/>
<wire x1="365.76" y1="157.48" x2="365.76" y2="152.4" width="0.1524" layer="91"/>
<wire x1="363.22" y1="193.04" x2="365.76" y2="193.04" width="0.1524" layer="91"/>
<junction x="365.76" y="193.04"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="365.76" y1="187.96" x2="355.6" y2="187.96" width="0.1524" layer="91"/>
<junction x="365.76" y="187.96"/>
<wire x1="360.68" y1="182.88" x2="363.22" y2="182.88" width="0.1524" layer="91"/>
<junction x="365.76" y="182.88"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="363.22" y1="182.88" x2="365.76" y2="182.88" width="0.1524" layer="91"/>
<wire x1="365.76" y1="177.8" x2="355.6" y2="177.8" width="0.1524" layer="91"/>
<junction x="365.76" y="177.8"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="363.22" y1="172.72" x2="365.76" y2="172.72" width="0.1524" layer="91"/>
<junction x="365.76" y="172.72"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="365.76" y1="167.64" x2="355.6" y2="167.64" width="0.1524" layer="91"/>
<junction x="365.76" y="167.64"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="365.76" y1="162.56" x2="363.22" y2="162.56" width="0.1524" layer="91"/>
<junction x="365.76" y="162.56"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="355.6" y1="157.48" x2="365.76" y2="157.48" width="0.1524" layer="91"/>
<junction x="365.76" y="157.48"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="365.76" y1="152.4" x2="363.22" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<junction x="363.22" y="182.88"/>
<junction x="365.76" y="152.4"/>
<wire x1="365.76" y1="152.4" x2="365.76" y2="147.32" width="0.1524" layer="91"/>
<junction x="365.76" y="147.32"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="C30" gate="G$1" pin="1"/>
<pinref part="C28" gate="G$1" pin="1"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="355.6" y1="76.2" x2="365.76" y2="76.2" width="0.1524" layer="91"/>
<wire x1="365.76" y1="76.2" x2="365.76" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="363.22" y1="71.12" x2="365.76" y2="71.12" width="0.1524" layer="91"/>
<junction x="365.76" y="71.12"/>
<wire x1="365.76" y1="71.12" x2="365.76" y2="66.04" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="355.6" y1="66.04" x2="365.76" y2="66.04" width="0.1524" layer="91"/>
<junction x="365.76" y="66.04"/>
<wire x1="365.76" y1="66.04" x2="365.76" y2="60.96" width="0.1524" layer="91"/>
<wire x1="365.76" y1="60.96" x2="363.22" y2="60.96" width="0.1524" layer="91"/>
<junction x="365.76" y="60.96"/>
<wire x1="365.76" y1="60.96" x2="365.76" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="355.6" y1="55.88" x2="365.76" y2="55.88" width="0.1524" layer="91"/>
<junction x="365.76" y="55.88"/>
<wire x1="365.76" y1="55.88" x2="365.76" y2="50.8" width="0.1524" layer="91"/>
<wire x1="365.76" y1="50.8" x2="363.22" y2="50.8" width="0.1524" layer="91"/>
<junction x="365.76" y="50.8"/>
<wire x1="365.76" y1="50.8" x2="365.76" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="355.6" y1="45.72" x2="365.76" y2="45.72" width="0.1524" layer="91"/>
<junction x="365.76" y="45.72"/>
<wire x1="365.76" y1="45.72" x2="365.76" y2="40.64" width="0.1524" layer="91"/>
<wire x1="365.76" y1="40.64" x2="363.22" y2="40.64" width="0.1524" layer="91"/>
<junction x="365.76" y="40.64"/>
<wire x1="365.76" y1="40.64" x2="365.76" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="355.6" y1="35.56" x2="365.76" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="363.22" y1="81.28" x2="365.76" y2="81.28" width="0.1524" layer="91"/>
<wire x1="365.76" y1="81.28" x2="365.76" y2="76.2" width="0.1524" layer="91"/>
<junction x="365.76" y="76.2"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="355.6" y1="86.36" x2="365.76" y2="86.36" width="0.1524" layer="91"/>
<wire x1="365.76" y1="86.36" x2="365.76" y2="81.28" width="0.1524" layer="91"/>
<junction x="365.76" y="81.28"/>
<pinref part="PE3" gate="M" pin="PE"/>
<wire x1="365.76" y1="30.48" x2="365.76" y2="35.56" width="0.1524" layer="91"/>
<junction x="365.76" y="35.56"/>
</segment>
</net>
<net name="E5V_1" class="0">
<segment>
<pinref part="L4" gate="D" pin="2"/>
<pinref part="CON3" gate="G$1" pin="1"/>
<wire x1="284.48" y1="132.08" x2="342.9" y2="132.08" width="0.1524" layer="91"/>
<label x="285.75" y="132.334" size="1.4224" layer="95"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="342.9" y1="132.08" x2="347.98" y2="132.08" width="0.1524" layer="91"/>
<wire x1="345.44" y1="86.36" x2="342.9" y2="86.36" width="0.1524" layer="91"/>
<wire x1="342.9" y1="86.36" x2="342.9" y2="132.08" width="0.1524" layer="91"/>
<junction x="342.9" y="132.08"/>
</segment>
</net>
<net name="E5V_2" class="0">
<segment>
<pinref part="L4" gate="C" pin="2"/>
<pinref part="CON3" gate="G$1" pin="9"/>
<wire x1="284.48" y1="129.54" x2="340.36" y2="129.54" width="0.1524" layer="91"/>
<label x="285.75" y="129.794" size="1.4224" layer="95"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="340.36" y1="129.54" x2="347.98" y2="129.54" width="0.1524" layer="91"/>
<wire x1="353.06" y1="81.28" x2="340.36" y2="81.28" width="0.1524" layer="91"/>
<wire x1="340.36" y1="81.28" x2="340.36" y2="129.54" width="0.1524" layer="91"/>
<junction x="340.36" y="129.54"/>
</segment>
</net>
<net name="ESTEPZ" class="0">
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="345.44" y1="187.96" x2="327.66" y2="187.96" width="0.1524" layer="91"/>
<label x="342.646" y="188.468" size="1.4224" layer="95" rot="MR0"/>
<pinref part="CON4" gate="G$1" pin="13"/>
<pinref part="L2" gate="C" pin="2"/>
<wire x1="350.52" y1="233.68" x2="327.66" y2="233.68" width="0.1524" layer="91"/>
<label x="261.62" y="233.68" size="1.4224" layer="95"/>
<wire x1="327.66" y1="233.68" x2="259.08" y2="233.68" width="0.1524" layer="91"/>
<wire x1="327.66" y1="187.96" x2="327.66" y2="233.68" width="0.1524" layer="91"/>
<junction x="327.66" y="233.68"/>
</segment>
</net>
<net name="EX1" class="0">
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="345.44" y1="76.2" x2="337.82" y2="76.2" width="0.1524" layer="91"/>
<label x="342.9" y="76.2" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L4" gate="B" pin="2"/>
<pinref part="CON3" gate="G$1" pin="2"/>
<wire x1="284.48" y1="127" x2="337.82" y2="127" width="0.1524" layer="91"/>
<label x="285.75" y="127.254" size="1.4224" layer="95"/>
<wire x1="337.82" y1="127" x2="347.98" y2="127" width="0.1524" layer="91"/>
<wire x1="337.82" y1="76.2" x2="337.82" y2="127" width="0.1524" layer="91"/>
<junction x="337.82" y="127"/>
</segment>
</net>
<net name="EX2" class="0">
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="353.06" y1="71.12" x2="335.28" y2="71.12" width="0.1524" layer="91"/>
<label x="342.9" y="71.12" size="1.4224" layer="95" rot="MR0"/>
<pinref part="CON3" gate="G$1" pin="10"/>
<pinref part="L4" gate="A" pin="2"/>
<wire x1="347.98" y1="124.46" x2="335.28" y2="124.46" width="0.1524" layer="91"/>
<label x="285.75" y="124.714" size="1.4224" layer="95"/>
<wire x1="335.28" y1="124.46" x2="284.48" y2="124.46" width="0.1524" layer="91"/>
<wire x1="335.28" y1="71.12" x2="335.28" y2="124.46" width="0.1524" layer="91"/>
<junction x="335.28" y="124.46"/>
</segment>
</net>
<net name="EY1" class="0">
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="345.44" y1="66.04" x2="332.74" y2="66.04" width="0.1524" layer="91"/>
<label x="342.9" y="66.04" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L5" gate="D" pin="2"/>
<pinref part="CON3" gate="G$1" pin="3"/>
<wire x1="284.48" y1="121.92" x2="332.74" y2="121.92" width="0.1524" layer="91"/>
<label x="285.75" y="122.174" size="1.4224" layer="95"/>
<wire x1="332.74" y1="121.92" x2="347.98" y2="121.92" width="0.1524" layer="91"/>
<wire x1="332.74" y1="66.04" x2="332.74" y2="121.92" width="0.1524" layer="91"/>
<junction x="332.74" y="121.92"/>
</segment>
</net>
<net name="EY2" class="0">
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="353.06" y1="60.96" x2="330.2" y2="60.96" width="0.1524" layer="91"/>
<label x="342.9" y="60.96" size="1.4224" layer="95" rot="MR0"/>
<pinref part="CON3" gate="G$1" pin="11"/>
<pinref part="L5" gate="C" pin="2"/>
<wire x1="347.98" y1="119.38" x2="330.2" y2="119.38" width="0.1524" layer="91"/>
<label x="285.75" y="119.634" size="1.4224" layer="95"/>
<wire x1="330.2" y1="119.38" x2="284.48" y2="119.38" width="0.1524" layer="91"/>
<wire x1="330.2" y1="60.96" x2="330.2" y2="119.38" width="0.1524" layer="91"/>
<junction x="330.2" y="119.38"/>
</segment>
</net>
<net name="EZ1" class="0">
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<label x="342.9" y="55.88" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L5" gate="B" pin="2"/>
<pinref part="CON3" gate="G$1" pin="4"/>
<wire x1="284.48" y1="116.84" x2="327.66" y2="116.84" width="0.1524" layer="91"/>
<label x="285.75" y="117.094" size="1.4224" layer="95"/>
<wire x1="327.66" y1="116.84" x2="347.98" y2="116.84" width="0.1524" layer="91"/>
<wire x1="345.44" y1="55.88" x2="327.66" y2="55.88" width="0.1524" layer="91"/>
<wire x1="327.66" y1="55.88" x2="327.66" y2="116.84" width="0.1524" layer="91"/>
<junction x="327.66" y="116.84"/>
</segment>
</net>
<net name="EZ2" class="0">
<segment>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="353.06" y1="50.8" x2="325.12" y2="50.8" width="0.1524" layer="91"/>
<label x="342.9" y="50.8" size="1.4224" layer="95" rot="MR0"/>
<pinref part="CON3" gate="G$1" pin="12"/>
<pinref part="L5" gate="A" pin="2"/>
<wire x1="347.98" y1="114.3" x2="325.12" y2="114.3" width="0.1524" layer="91"/>
<label x="285.75" y="114.554" size="1.4224" layer="95"/>
<wire x1="325.12" y1="114.3" x2="284.48" y2="114.3" width="0.1524" layer="91"/>
<wire x1="325.12" y1="50.8" x2="325.12" y2="114.3" width="0.1524" layer="91"/>
<junction x="325.12" y="114.3"/>
</segment>
</net>
<net name="EDOOR1" class="0">
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<label x="342.9" y="45.72" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L6" gate="D" pin="2"/>
<pinref part="CON3" gate="G$1" pin="5"/>
<wire x1="284.48" y1="111.76" x2="322.58" y2="111.76" width="0.1524" layer="91"/>
<label x="285.75" y="112.014" size="1.4224" layer="95"/>
<wire x1="322.58" y1="111.76" x2="347.98" y2="111.76" width="0.1524" layer="91"/>
<wire x1="345.44" y1="45.72" x2="322.58" y2="45.72" width="0.1524" layer="91"/>
<wire x1="322.58" y1="45.72" x2="322.58" y2="111.76" width="0.1524" layer="91"/>
<junction x="322.58" y="111.76"/>
</segment>
</net>
<net name="X1" class="0">
<segment>
<wire x1="68.58" y1="127" x2="66.04" y2="129.54" width="0.1524" layer="91"/>
<label x="71.12" y="127.508" size="1.4224" layer="95"/>
<pinref part="IC13" gate="G$1" pin="1A1"/>
<wire x1="68.58" y1="127" x2="83.82" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X2" class="0">
<segment>
<wire x1="68.58" y1="124.46" x2="66.04" y2="127" width="0.1524" layer="91"/>
<label x="71.12" y="124.968" size="1.4224" layer="95"/>
<pinref part="IC13" gate="G$1" pin="1A2"/>
<wire x1="68.58" y1="124.46" x2="83.82" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Y1" class="0">
<segment>
<wire x1="68.58" y1="121.92" x2="66.04" y2="124.46" width="0.1524" layer="91"/>
<label x="71.12" y="122.428" size="1.4224" layer="95"/>
<pinref part="IC13" gate="G$1" pin="1A3"/>
<wire x1="68.58" y1="121.92" x2="83.82" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Y2" class="0">
<segment>
<wire x1="68.58" y1="119.38" x2="66.04" y2="121.92" width="0.1524" layer="91"/>
<label x="71.12" y="119.888" size="1.4224" layer="95"/>
<pinref part="IC13" gate="G$1" pin="1A4"/>
<wire x1="68.58" y1="119.38" x2="83.82" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Z1" class="0">
<segment>
<wire x1="68.58" y1="116.84" x2="66.04" y2="119.38" width="0.1524" layer="91"/>
<label x="71.12" y="117.348" size="1.4224" layer="95"/>
<pinref part="IC13" gate="G$1" pin="1A5"/>
<wire x1="68.58" y1="116.84" x2="83.82" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Z2" class="0">
<segment>
<wire x1="68.58" y1="114.3" x2="66.04" y2="116.84" width="0.1524" layer="91"/>
<label x="71.12" y="114.808" size="1.4224" layer="95"/>
<pinref part="IC13" gate="G$1" pin="1A6"/>
<wire x1="68.58" y1="114.3" x2="83.82" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CHILLER" class="0">
<segment>
<wire x1="68.58" y1="106.68" x2="66.04" y2="109.22" width="0.1524" layer="91"/>
<label x="71.12" y="107.188" size="1.4224" layer="95"/>
<pinref part="IC13" gate="G$1" pin="2A1"/>
<wire x1="68.58" y1="106.68" x2="83.82" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AUX1" class="0">
<segment>
<wire x1="66.04" y1="241.3" x2="63.5" y2="243.84" width="0.1524" layer="91"/>
<label x="68.58" y="241.3" size="1.4224" layer="95"/>
<pinref part="IC12" gate="G$1" pin="1A3"/>
<wire x1="66.04" y1="241.3" x2="96.52" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DOOR1" class="0">
<segment>
<wire x1="68.58" y1="111.76" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<label x="71.12" y="112.268" size="1.4224" layer="95"/>
<pinref part="IC13" gate="G$1" pin="1A7"/>
<wire x1="68.58" y1="111.76" x2="83.82" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DOOR2" class="0">
<segment>
<wire x1="66.04" y1="111.76" x2="68.58" y2="109.22" width="0.1524" layer="91"/>
<label x="71.12" y="109.474" size="1.4224" layer="95"/>
<pinref part="IC13" gate="G$1" pin="1A8"/>
<wire x1="68.58" y1="109.22" x2="83.82" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EDOOR2" class="0">
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="353.06" y1="40.64" x2="320.04" y2="40.64" width="0.1524" layer="91"/>
<label x="342.9" y="40.64" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L6" gate="C" pin="2"/>
<pinref part="CON3" gate="G$1" pin="13"/>
<wire x1="284.48" y1="109.22" x2="320.04" y2="109.22" width="0.1524" layer="91"/>
<label x="285.75" y="109.474" size="1.4224" layer="95"/>
<wire x1="320.04" y1="109.22" x2="347.98" y2="109.22" width="0.1524" layer="91"/>
<wire x1="320.04" y1="40.64" x2="320.04" y2="109.22" width="0.1524" layer="91"/>
<junction x="320.04" y="109.22"/>
</segment>
</net>
<net name="ECHILLER" class="0">
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="345.44" y1="35.56" x2="317.5" y2="35.56" width="0.1524" layer="91"/>
<label x="342.9" y="35.56" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L6" gate="B" pin="2"/>
<pinref part="CON3" gate="G$1" pin="6"/>
<wire x1="284.48" y1="106.68" x2="317.5" y2="106.68" width="0.1524" layer="91"/>
<label x="285.75" y="106.934" size="1.4224" layer="95"/>
<wire x1="317.5" y1="106.68" x2="347.98" y2="106.68" width="0.1524" layer="91"/>
<wire x1="317.5" y1="35.56" x2="317.5" y2="106.68" width="0.1524" layer="91"/>
<junction x="317.5" y="106.68"/>
</segment>
</net>
<net name="EAIR" class="0">
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<label x="342.9" y="147.32" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L3" gate="A" pin="2"/>
<pinref part="CON4" gate="G$1" pin="10"/>
<wire x1="259.08" y1="218.44" x2="307.34" y2="218.44" width="0.1524" layer="91"/>
<label x="261.62" y="218.44" size="1.4224" layer="95"/>
<wire x1="307.34" y1="218.44" x2="350.52" y2="218.44" width="0.1524" layer="91"/>
<wire x1="345.44" y1="147.32" x2="307.34" y2="147.32" width="0.1524" layer="91"/>
<wire x1="307.34" y1="147.32" x2="307.34" y2="218.44" width="0.1524" layer="91"/>
<junction x="307.34" y="218.44"/>
</segment>
</net>
<net name="EAUX1" class="0">
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<label x="342.9" y="142.24" size="1.4224" layer="95" rot="MR0"/>
<pinref part="L1" gate="B" pin="2"/>
<pinref part="CON4" gate="G$1" pin="7"/>
<wire x1="259.08" y1="241.3" x2="304.8" y2="241.3" width="0.1524" layer="91"/>
<label x="261.62" y="241.3" size="1.4224" layer="95"/>
<wire x1="304.8" y1="241.3" x2="350.52" y2="241.3" width="0.1524" layer="91"/>
<wire x1="353.06" y1="142.24" x2="304.8" y2="142.24" width="0.1524" layer="91"/>
<wire x1="304.8" y1="142.24" x2="304.8" y2="241.3" width="0.1524" layer="91"/>
<junction x="304.8" y="241.3"/>
</segment>
</net>
<net name="X1_E" class="0">
<segment>
<pinref part="R4" gate="D" pin="2"/>
<pinref part="L4" gate="B" pin="1"/>
<wire x1="129.54" y1="127" x2="236.22" y2="127" width="0.1524" layer="91"/>
<label x="263.652" y="127.254" size="1.4224" layer="95"/>
<wire x1="236.22" y1="127" x2="241.3" y2="127" width="0.1524" layer="91"/>
<wire x1="241.3" y1="127" x2="274.32" y2="127" width="0.1524" layer="91"/>
<wire x1="241.3" y1="99.06" x2="241.3" y2="127" width="0.1524" layer="91"/>
<junction x="241.3" y="127"/>
<pinref part="U$6" gate="G$1" pin="C4"/>
<wire x1="236.22" y1="86.36" x2="236.22" y2="127" width="0.1524" layer="91"/>
<junction x="236.22" y="127"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="45.72" y1="66.04" x2="60.96" y2="66.04" width="0.1524" layer="91"/>
<wire x1="60.96" y1="66.04" x2="63.5" y2="68.58" width="0.1524" layer="91"/>
<label x="48.26" y="66.548" size="1.4224" layer="95"/>
</segment>
</net>
<net name="X2_E" class="0">
<segment>
<pinref part="L4" gate="A" pin="1"/>
<pinref part="R4" gate="C" pin="2"/>
<wire x1="274.32" y1="124.46" x2="243.84" y2="124.46" width="0.1524" layer="91"/>
<label x="263.652" y="124.714" size="1.4224" layer="95"/>
<wire x1="243.84" y1="124.46" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
<wire x1="210.82" y1="124.46" x2="137.16" y2="124.46" width="0.1524" layer="91"/>
<wire x1="243.84" y1="99.06" x2="243.84" y2="124.46" width="0.1524" layer="91"/>
<junction x="243.84" y="124.46"/>
<pinref part="U$6" gate="G$1" pin="C1"/>
<wire x1="210.82" y1="86.36" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
<junction x="210.82" y="124.46"/>
<pinref part="R7" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="45.72" y1="60.96" x2="60.96" y2="60.96" width="0.1524" layer="91"/>
<wire x1="60.96" y1="60.96" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<label x="48.26" y="61.468" size="1.4224" layer="95"/>
</segment>
</net>
<net name="Y1_E" class="0">
<segment>
<pinref part="R4" gate="B" pin="2"/>
<pinref part="L5" gate="D" pin="1"/>
<wire x1="129.54" y1="121.92" x2="175.26" y2="121.92" width="0.1524" layer="91"/>
<label x="263.652" y="122.174" size="1.4224" layer="95"/>
<wire x1="175.26" y1="121.92" x2="246.38" y2="121.92" width="0.1524" layer="91"/>
<wire x1="246.38" y1="121.92" x2="274.32" y2="121.92" width="0.1524" layer="91"/>
<wire x1="246.38" y1="99.06" x2="246.38" y2="121.92" width="0.1524" layer="91"/>
<junction x="246.38" y="121.92"/>
<pinref part="U$7" gate="G$1" pin="C2"/>
<wire x1="177.8" y1="76.2" x2="175.26" y2="76.2" width="0.1524" layer="91"/>
<wire x1="175.26" y1="76.2" x2="175.26" y2="121.92" width="0.1524" layer="91"/>
<junction x="175.26" y="121.92"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="45.72" y1="55.88" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
<wire x1="60.96" y1="55.88" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<label x="48.26" y="56.388" size="1.4224" layer="95"/>
</segment>
</net>
<net name="Y2_E" class="0">
<segment>
<pinref part="L5" gate="C" pin="1"/>
<pinref part="R4" gate="A" pin="2"/>
<wire x1="274.32" y1="119.38" x2="248.92" y2="119.38" width="0.1524" layer="91"/>
<label x="263.652" y="119.634" size="1.4224" layer="95"/>
<wire x1="248.92" y1="119.38" x2="205.74" y2="119.38" width="0.1524" layer="91"/>
<wire x1="205.74" y1="119.38" x2="137.16" y2="119.38" width="0.1524" layer="91"/>
<wire x1="248.92" y1="99.06" x2="248.92" y2="119.38" width="0.1524" layer="91"/>
<junction x="248.92" y="119.38"/>
<pinref part="U$7" gate="G$1" pin="C3"/>
<wire x1="203.2" y1="76.2" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<wire x1="205.74" y1="76.2" x2="205.74" y2="119.38" width="0.1524" layer="91"/>
<junction x="205.74" y="119.38"/>
<pinref part="R9" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="45.72" y1="50.8" x2="60.96" y2="50.8" width="0.1524" layer="91"/>
<wire x1="60.96" y1="50.8" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<label x="48.26" y="51.308" size="1.4224" layer="95"/>
</segment>
</net>
<net name="Z1_E" class="0">
<segment>
<pinref part="R5" gate="D" pin="2"/>
<pinref part="L5" gate="B" pin="1"/>
<label x="263.652" y="117.094" size="1.4224" layer="95"/>
<wire x1="129.54" y1="116.84" x2="203.2" y2="116.84" width="0.1524" layer="91"/>
<wire x1="203.2" y1="116.84" x2="251.46" y2="116.84" width="0.1524" layer="91"/>
<wire x1="251.46" y1="116.84" x2="274.32" y2="116.84" width="0.1524" layer="91"/>
<wire x1="251.46" y1="99.06" x2="251.46" y2="116.84" width="0.1524" layer="91"/>
<junction x="251.46" y="116.84"/>
<wire x1="203.2" y1="86.36" x2="203.2" y2="116.84" width="0.1524" layer="91"/>
<junction x="203.2" y="116.84"/>
<pinref part="U$7" gate="G$1" pin="C4"/>
<pinref part="R36" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="45.72" y1="45.72" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<wire x1="60.96" y1="45.72" x2="63.5" y2="48.26" width="0.1524" layer="91"/>
<label x="48.26" y="46.228" size="1.4224" layer="95"/>
</segment>
</net>
<net name="Z2_E" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="45.72" y1="40.64" x2="60.96" y2="40.64" width="0.1524" layer="91"/>
<wire x1="60.96" y1="40.64" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<label x="48.26" y="41.148" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="L5" gate="A" pin="1"/>
<pinref part="R5" gate="C" pin="2"/>
<wire x1="274.32" y1="114.3" x2="254" y2="114.3" width="0.1524" layer="91"/>
<label x="263.652" y="114.554" size="1.4224" layer="95"/>
<wire x1="254" y1="114.3" x2="177.8" y2="114.3" width="0.1524" layer="91"/>
<wire x1="177.8" y1="114.3" x2="137.16" y2="114.3" width="0.1524" layer="91"/>
<wire x1="254" y1="99.06" x2="254" y2="114.3" width="0.1524" layer="91"/>
<junction x="254" y="114.3"/>
<pinref part="U$7" gate="G$1" pin="C1"/>
<wire x1="177.8" y1="86.36" x2="177.8" y2="114.3" width="0.1524" layer="91"/>
<junction x="177.8" y="114.3"/>
<pinref part="R37" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DOOR1_E" class="0">
<segment>
<pinref part="R5" gate="B" pin="2"/>
<pinref part="L6" gate="D" pin="1"/>
<label x="263.652" y="112.014" size="1.4224" layer="95"/>
<wire x1="129.54" y1="111.76" x2="142.24" y2="111.76" width="0.1524" layer="91"/>
<wire x1="142.24" y1="111.76" x2="256.54" y2="111.76" width="0.1524" layer="91"/>
<wire x1="256.54" y1="111.76" x2="274.32" y2="111.76" width="0.1524" layer="91"/>
<wire x1="256.54" y1="99.06" x2="256.54" y2="111.76" width="0.1524" layer="91"/>
<junction x="256.54" y="111.76"/>
<pinref part="U$8" gate="G$1" pin="C2"/>
<wire x1="144.78" y1="76.2" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="142.24" y1="76.2" x2="142.24" y2="111.76" width="0.1524" layer="91"/>
<junction x="142.24" y="111.76"/>
<pinref part="R38" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="45.72" y1="35.56" x2="60.96" y2="35.56" width="0.1524" layer="91"/>
<wire x1="60.96" y1="35.56" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<label x="48.26" y="36.068" size="1.4224" layer="95"/>
</segment>
</net>
<net name="DOOR2_E" class="0">
<segment>
<pinref part="L6" gate="C" pin="1"/>
<pinref part="R5" gate="A" pin="2"/>
<wire x1="274.32" y1="109.22" x2="259.08" y2="109.22" width="0.1524" layer="91"/>
<label x="263.652" y="109.474" size="1.4224" layer="95"/>
<wire x1="259.08" y1="109.22" x2="172.72" y2="109.22" width="0.1524" layer="91"/>
<wire x1="172.72" y1="109.22" x2="137.16" y2="109.22" width="0.1524" layer="91"/>
<wire x1="259.08" y1="99.06" x2="259.08" y2="109.22" width="0.1524" layer="91"/>
<junction x="259.08" y="109.22"/>
<pinref part="U$8" gate="G$1" pin="C3"/>
<wire x1="170.18" y1="76.2" x2="172.72" y2="76.2" width="0.1524" layer="91"/>
<wire x1="172.72" y1="76.2" x2="172.72" y2="109.22" width="0.1524" layer="91"/>
<junction x="172.72" y="109.22"/>
<pinref part="R39" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="45.72" y1="30.48" x2="60.96" y2="30.48" width="0.1524" layer="91"/>
<wire x1="60.96" y1="30.48" x2="63.5" y2="33.02" width="0.1524" layer="91"/>
<label x="48.26" y="30.988" size="1.4224" layer="95"/>
</segment>
</net>
<net name="CHILLER_E" class="0">
<segment>
<pinref part="L6" gate="B" pin="1"/>
<label x="263.652" y="106.934" size="1.4224" layer="95"/>
<wire x1="129.54" y1="106.68" x2="170.18" y2="106.68" width="0.1524" layer="91"/>
<wire x1="170.18" y1="106.68" x2="261.62" y2="106.68" width="0.1524" layer="91"/>
<wire x1="261.62" y1="106.68" x2="274.32" y2="106.68" width="0.1524" layer="91"/>
<wire x1="261.62" y1="99.06" x2="261.62" y2="106.68" width="0.1524" layer="91"/>
<junction x="261.62" y="106.68"/>
<pinref part="U$8" gate="G$1" pin="C4"/>
<wire x1="170.18" y1="86.36" x2="170.18" y2="106.68" width="0.1524" layer="91"/>
<junction x="170.18" y="106.68"/>
<pinref part="R35" gate="G$1" pin="2"/>
<pinref part="R40" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="45.72" y1="25.4" x2="60.96" y2="25.4" width="0.1524" layer="91"/>
<wire x1="60.96" y1="25.4" x2="63.5" y2="27.94" width="0.1524" layer="91"/>
<label x="48.26" y="25.908" size="1.4224" layer="95"/>
</segment>
</net>
<net name="AIR_E" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="45.72" y1="20.32" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
<wire x1="60.96" y1="20.32" x2="63.5" y2="22.86" width="0.1524" layer="91"/>
<label x="48.26" y="20.828" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="2B4"/>
<pinref part="L3" gate="A" pin="1"/>
<label x="236.22" y="218.44" size="1.4224" layer="95"/>
<wire x1="124.46" y1="218.44" x2="139.7" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="C1"/>
<wire x1="139.7" y1="218.44" x2="248.92" y2="218.44" width="0.1524" layer="91"/>
<wire x1="139.7" y1="193.04" x2="139.7" y2="218.44" width="0.1524" layer="91"/>
<junction x="139.7" y="218.44"/>
</segment>
</net>
<net name="STEPX_E" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="88.9" y1="66.04" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="66.04" y1="66.04" x2="63.5" y2="68.58" width="0.1524" layer="91"/>
<label x="68.58" y="66.294" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="1B4"/>
<pinref part="L1" gate="A" pin="1"/>
<wire x1="124.46" y1="238.76" x2="205.74" y2="238.76" width="0.1524" layer="91"/>
<label x="236.22" y="238.76" size="1.4224" layer="95"/>
<pinref part="U$5" gate="G$1" pin="C1"/>
<wire x1="205.74" y1="238.76" x2="248.92" y2="238.76" width="0.1524" layer="91"/>
<wire x1="205.74" y1="193.04" x2="205.74" y2="238.76" width="0.1524" layer="91"/>
<junction x="205.74" y="238.76"/>
</segment>
</net>
<net name="DIRX_E" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="88.9" y1="60.96" x2="66.04" y2="60.96" width="0.1524" layer="91"/>
<wire x1="66.04" y1="60.96" x2="63.5" y2="63.5" width="0.1524" layer="91"/>
<label x="68.58" y="61.214" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="1B7"/>
<pinref part="L2" gate="B" pin="1"/>
<label x="236.22" y="231.14" size="1.4224" layer="95"/>
<wire x1="124.46" y1="231.14" x2="198.12" y2="231.14" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="C4"/>
<wire x1="198.12" y1="231.14" x2="248.92" y2="231.14" width="0.1524" layer="91"/>
<wire x1="198.12" y1="193.04" x2="198.12" y2="231.14" width="0.1524" layer="91"/>
<junction x="198.12" y="231.14"/>
</segment>
</net>
<net name="STEPY_E" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="88.9" y1="55.88" x2="66.04" y2="55.88" width="0.1524" layer="91"/>
<wire x1="66.04" y1="55.88" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<label x="68.58" y="56.134" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="1B5"/>
<pinref part="L2" gate="D" pin="1"/>
<wire x1="124.46" y1="236.22" x2="170.18" y2="236.22" width="0.1524" layer="91"/>
<label x="236.22" y="236.22" size="1.4224" layer="95"/>
<pinref part="U$4" gate="G$1" pin="C2"/>
<wire x1="170.18" y1="236.22" x2="248.92" y2="236.22" width="0.1524" layer="91"/>
<wire x1="172.72" y1="182.88" x2="170.18" y2="182.88" width="0.1524" layer="91"/>
<wire x1="170.18" y1="182.88" x2="170.18" y2="236.22" width="0.1524" layer="91"/>
<junction x="170.18" y="236.22"/>
</segment>
</net>
<net name="DIRY_E" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="88.9" y1="50.8" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
<wire x1="66.04" y1="50.8" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<label x="68.58" y="51.054" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="L2" gate="A" pin="1"/>
<pinref part="IC12" gate="G$1" pin="1B8"/>
<wire x1="248.92" y1="228.6" x2="172.72" y2="228.6" width="0.1524" layer="91"/>
<label x="236.22" y="228.6" size="1.4224" layer="95"/>
<pinref part="U$4" gate="G$1" pin="C1"/>
<wire x1="172.72" y1="228.6" x2="124.46" y2="228.6" width="0.1524" layer="91"/>
<wire x1="172.72" y1="193.04" x2="172.72" y2="228.6" width="0.1524" layer="91"/>
<junction x="172.72" y="228.6"/>
</segment>
</net>
<net name="STEPZ_E" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="88.9" y1="45.72" x2="66.04" y2="45.72" width="0.1524" layer="91"/>
<wire x1="66.04" y1="45.72" x2="63.5" y2="48.26" width="0.1524" layer="91"/>
<label x="68.58" y="45.974" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="L2" gate="C" pin="1"/>
<pinref part="IC12" gate="G$1" pin="1B6"/>
<label x="236.22" y="233.68" size="1.4224" layer="95"/>
<wire x1="248.92" y1="233.68" x2="200.66" y2="233.68" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="C3"/>
<wire x1="200.66" y1="233.68" x2="124.46" y2="233.68" width="0.1524" layer="91"/>
<wire x1="198.12" y1="182.88" x2="200.66" y2="182.88" width="0.1524" layer="91"/>
<wire x1="200.66" y1="182.88" x2="200.66" y2="233.68" width="0.1524" layer="91"/>
<junction x="200.66" y="233.68"/>
</segment>
</net>
<net name="DIRZ_E" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="88.9" y1="40.64" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="40.64" x2="63.5" y2="43.18" width="0.1524" layer="91"/>
<label x="68.58" y="40.894" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="2B1"/>
<pinref part="L3" gate="D" pin="1"/>
<label x="236.22" y="226.06" size="1.4224" layer="95"/>
<wire x1="124.46" y1="226.06" x2="137.16" y2="226.06" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="C2"/>
<wire x1="137.16" y1="226.06" x2="248.92" y2="226.06" width="0.1524" layer="91"/>
<wire x1="139.7" y1="182.88" x2="137.16" y2="182.88" width="0.1524" layer="91"/>
<wire x1="137.16" y1="182.88" x2="137.16" y2="226.06" width="0.1524" layer="91"/>
<junction x="137.16" y="226.06"/>
</segment>
</net>
<net name="LASER1_E" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="88.9" y1="30.48" x2="66.04" y2="30.48" width="0.1524" layer="91"/>
<wire x1="66.04" y1="30.48" x2="63.5" y2="33.02" width="0.1524" layer="91"/>
<label x="68.58" y="30.734" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="1B1"/>
<pinref part="L1" gate="D" pin="1"/>
<wire x1="124.46" y1="246.38" x2="203.2" y2="246.38" width="0.1524" layer="91"/>
<label x="236.22" y="246.38" size="1.4224" layer="95"/>
<pinref part="U$5" gate="G$1" pin="C2"/>
<wire x1="203.2" y1="246.38" x2="248.92" y2="246.38" width="0.1524" layer="91"/>
<wire x1="205.74" y1="182.88" x2="203.2" y2="182.88" width="0.1524" layer="91"/>
<wire x1="203.2" y1="182.88" x2="203.2" y2="246.38" width="0.1524" layer="91"/>
<junction x="203.2" y="246.38"/>
</segment>
</net>
<net name="LASER2_E" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="88.9" y1="25.4" x2="66.04" y2="25.4" width="0.1524" layer="91"/>
<wire x1="66.04" y1="25.4" x2="63.5" y2="27.94" width="0.1524" layer="91"/>
<label x="68.58" y="25.654" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="1B2"/>
<pinref part="L1" gate="C" pin="1"/>
<wire x1="124.46" y1="243.84" x2="233.68" y2="243.84" width="0.1524" layer="91"/>
<label x="236.22" y="243.84" size="1.4224" layer="95"/>
<pinref part="U$5" gate="G$1" pin="C3"/>
<wire x1="233.68" y1="243.84" x2="248.92" y2="243.84" width="0.1524" layer="91"/>
<wire x1="231.14" y1="182.88" x2="233.68" y2="182.88" width="0.1524" layer="91"/>
<wire x1="233.68" y1="182.88" x2="233.68" y2="243.84" width="0.1524" layer="91"/>
<junction x="233.68" y="243.84"/>
</segment>
</net>
<net name="LASERDIS_E" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="88.9" y1="20.32" x2="66.04" y2="20.32" width="0.1524" layer="91"/>
<wire x1="66.04" y1="20.32" x2="63.5" y2="22.86" width="0.1524" layer="91"/>
<label x="68.58" y="20.574" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="2B2"/>
<pinref part="L3" gate="C" pin="1"/>
<label x="236.22" y="223.52" size="1.4224" layer="95"/>
<wire x1="124.46" y1="223.52" x2="167.64" y2="223.52" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="C3"/>
<wire x1="167.64" y1="223.52" x2="248.92" y2="223.52" width="0.1524" layer="91"/>
<wire x1="165.1" y1="182.88" x2="167.64" y2="182.88" width="0.1524" layer="91"/>
<wire x1="167.64" y1="182.88" x2="167.64" y2="223.52" width="0.1524" layer="91"/>
<junction x="167.64" y="223.52"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R4" gate="D" pin="1"/>
<pinref part="IC13" gate="G$1" pin="1B1"/>
<wire x1="119.38" y1="127" x2="111.76" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="IC13" gate="G$1" pin="1B2"/>
<pinref part="R4" gate="C" pin="1"/>
<wire x1="111.76" y1="124.46" x2="127" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R4" gate="B" pin="1"/>
<pinref part="IC13" gate="G$1" pin="1B3"/>
<wire x1="119.38" y1="121.92" x2="111.76" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="IC13" gate="G$1" pin="1B4"/>
<pinref part="R4" gate="A" pin="1"/>
<wire x1="111.76" y1="119.38" x2="127" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R5" gate="D" pin="1"/>
<pinref part="IC13" gate="G$1" pin="1B5"/>
<wire x1="119.38" y1="116.84" x2="111.76" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="IC13" gate="G$1" pin="1B6"/>
<pinref part="R5" gate="C" pin="1"/>
<wire x1="111.76" y1="114.3" x2="127" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R5" gate="B" pin="1"/>
<pinref part="IC13" gate="G$1" pin="1B7"/>
<wire x1="119.38" y1="111.76" x2="111.76" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="IC13" gate="G$1" pin="1B8"/>
<pinref part="R5" gate="A" pin="1"/>
<wire x1="111.76" y1="109.22" x2="127" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="IC13" gate="G$1" pin="2B1"/>
<wire x1="119.38" y1="106.68" x2="111.76" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="L4" gate="C" pin="1"/>
<pinref part="L4" gate="D" pin="1"/>
<wire x1="274.32" y1="129.54" x2="274.32" y2="132.08" width="0.1524" layer="91"/>
<wire x1="274.32" y1="132.08" x2="274.32" y2="134.62" width="0.1524" layer="91"/>
<junction x="274.32" y="132.08"/>
<pinref part="F1" gate="G$1" pin="2"/>
<wire x1="274.32" y1="134.62" x2="271.78" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="LED1" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="LED2" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="LED3" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="LED5" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="LED6" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="LED7" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="LED8" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="LED9" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="LED10" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="LED11" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="LED4" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="LED12" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="LED13" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="LED14" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<pinref part="LED16" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="LED17" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<pinref part="LED18" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="LED19" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<pinref part="LED20" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="LED21" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<pinref part="LED15" gate="G$1" pin="A"/>
</segment>
</net>
<net name="STEPEN_E" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="88.9" y1="35.56" x2="66.04" y2="35.56" width="0.1524" layer="91"/>
<wire x1="66.04" y1="35.56" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<label x="68.58" y="35.814" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="2B3"/>
<pinref part="L3" gate="B" pin="1"/>
<label x="236.22" y="220.98" size="1.4224" layer="95"/>
<wire x1="124.46" y1="220.98" x2="165.1" y2="220.98" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="C4"/>
<wire x1="165.1" y1="220.98" x2="248.92" y2="220.98" width="0.1524" layer="91"/>
<wire x1="165.1" y1="193.04" x2="165.1" y2="220.98" width="0.1524" layer="91"/>
<junction x="165.1" y="220.98"/>
</segment>
</net>
<net name="AUX1_E" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="45.72" y1="15.24" x2="60.96" y2="15.24" width="0.1524" layer="91"/>
<wire x1="60.96" y1="15.24" x2="63.5" y2="17.78" width="0.1524" layer="91"/>
<label x="48.26" y="15.748" size="1.4224" layer="95"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="1B3"/>
<pinref part="L1" gate="B" pin="1"/>
<wire x1="124.46" y1="241.3" x2="231.14" y2="241.3" width="0.1524" layer="91"/>
<label x="236.22" y="241.3" size="1.4224" layer="95"/>
<pinref part="U$5" gate="G$1" pin="C4"/>
<wire x1="231.14" y1="241.3" x2="248.92" y2="241.3" width="0.1524" layer="91"/>
<wire x1="231.14" y1="193.04" x2="231.14" y2="241.3" width="0.1524" layer="91"/>
<junction x="231.14" y="241.3"/>
</segment>
</net>
<net name="LASER1" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="1A1"/>
<wire x1="96.52" y1="246.38" x2="66.04" y2="246.38" width="0.1524" layer="91"/>
<wire x1="66.04" y1="246.38" x2="63.5" y2="248.92" width="0.1524" layer="91"/>
<label x="68.58" y="246.38" size="1.4224" layer="95"/>
</segment>
</net>
<net name="LASER2" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="1A2"/>
<wire x1="96.52" y1="243.84" x2="66.04" y2="243.84" width="0.1524" layer="91"/>
<wire x1="66.04" y1="243.84" x2="63.5" y2="246.38" width="0.1524" layer="91"/>
<label x="68.58" y="243.84" size="1.4224" layer="95"/>
</segment>
</net>
<net name="STEPX" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="1A4"/>
<wire x1="96.52" y1="238.76" x2="66.04" y2="238.76" width="0.1524" layer="91"/>
<wire x1="66.04" y1="238.76" x2="63.5" y2="241.3" width="0.1524" layer="91"/>
<label x="68.58" y="238.76" size="1.4224" layer="95"/>
</segment>
</net>
<net name="STEPY" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="1A5"/>
<wire x1="96.52" y1="236.22" x2="66.04" y2="236.22" width="0.1524" layer="91"/>
<wire x1="66.04" y1="236.22" x2="63.5" y2="238.76" width="0.1524" layer="91"/>
<label x="68.58" y="236.22" size="1.4224" layer="95"/>
</segment>
</net>
<net name="STEPZ" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="1A6"/>
<wire x1="96.52" y1="233.68" x2="66.04" y2="233.68" width="0.1524" layer="91"/>
<wire x1="66.04" y1="233.68" x2="63.5" y2="236.22" width="0.1524" layer="91"/>
<label x="68.58" y="233.68" size="1.4224" layer="95"/>
</segment>
</net>
<net name="MOSI-DIRX" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="1A7"/>
<wire x1="96.52" y1="231.14" x2="66.04" y2="231.14" width="0.1524" layer="91"/>
<wire x1="66.04" y1="231.14" x2="63.5" y2="233.68" width="0.1524" layer="91"/>
<label x="68.58" y="231.14" size="1.4224" layer="95"/>
</segment>
</net>
<net name="MISO-DIRY" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="1A8"/>
<wire x1="96.52" y1="228.6" x2="66.04" y2="228.6" width="0.1524" layer="91"/>
<wire x1="66.04" y1="228.6" x2="63.5" y2="231.14" width="0.1524" layer="91"/>
<label x="68.58" y="228.6" size="1.4224" layer="95"/>
</segment>
</net>
<net name="SCK-DIRZ" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="2A1"/>
<wire x1="96.52" y1="226.06" x2="66.04" y2="226.06" width="0.1524" layer="91"/>
<wire x1="66.04" y1="226.06" x2="63.5" y2="228.6" width="0.1524" layer="91"/>
<label x="68.58" y="226.06" size="1.4224" layer="95"/>
</segment>
</net>
<net name="LASERDIS" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="2A2"/>
<wire x1="96.52" y1="223.52" x2="66.04" y2="223.52" width="0.1524" layer="91"/>
<wire x1="66.04" y1="223.52" x2="63.5" y2="226.06" width="0.1524" layer="91"/>
<label x="68.58" y="223.52" size="1.4224" layer="95"/>
</segment>
</net>
<net name="STEPEN" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="2A3"/>
<wire x1="96.52" y1="220.98" x2="66.04" y2="220.98" width="0.1524" layer="91"/>
<wire x1="66.04" y1="220.98" x2="63.5" y2="223.52" width="0.1524" layer="91"/>
<label x="68.58" y="220.98" size="1.4224" layer="95"/>
</segment>
</net>
<net name="AIR" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="2A4"/>
<wire x1="96.52" y1="218.44" x2="66.04" y2="218.44" width="0.1524" layer="91"/>
<wire x1="66.04" y1="218.44" x2="63.5" y2="220.98" width="0.1524" layer="91"/>
<label x="68.58" y="218.44" size="1.4224" layer="95"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="SJ1" gate="G$1" pin="2"/>
<wire x1="264.16" y1="88.9" x2="261.62" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="261.62" y1="88.9" x2="259.08" y2="88.9" width="0.1524" layer="91"/>
<wire x1="259.08" y1="88.9" x2="256.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="256.54" y1="88.9" x2="254" y2="88.9" width="0.1524" layer="91"/>
<wire x1="254" y1="88.9" x2="251.46" y2="88.9" width="0.1524" layer="91"/>
<wire x1="251.46" y1="88.9" x2="248.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="248.92" y1="88.9" x2="246.38" y2="88.9" width="0.1524" layer="91"/>
<wire x1="246.38" y1="88.9" x2="243.84" y2="88.9" width="0.1524" layer="91"/>
<wire x1="243.84" y1="88.9" x2="241.3" y2="88.9" width="0.1524" layer="91"/>
<junction x="243.84" y="88.9"/>
<pinref part="R8" gate="G$1" pin="2"/>
<junction x="246.38" y="88.9"/>
<pinref part="R9" gate="G$1" pin="2"/>
<junction x="248.92" y="88.9"/>
<pinref part="R36" gate="G$1" pin="2"/>
<junction x="251.46" y="88.9"/>
<pinref part="R37" gate="G$1" pin="2"/>
<junction x="254" y="88.9"/>
<pinref part="R38" gate="G$1" pin="2"/>
<junction x="256.54" y="88.9"/>
<pinref part="R39" gate="G$1" pin="2"/>
<junction x="259.08" y="88.9"/>
<pinref part="R40" gate="G$1" pin="2"/>
<junction x="261.62" y="88.9"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="OC23" gate="G$1" pin="C"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="284.48" y1="66.04" x2="289.56" y2="66.04" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="14"/>
<wire x1="289.56" y1="66.04" x2="297.18" y2="66.04" width="0.1524" layer="91"/>
<wire x1="347.98" y1="104.14" x2="314.96" y2="104.14" width="0.1524" layer="91"/>
<wire x1="314.96" y1="104.14" x2="314.96" y2="66.04" width="0.1524" layer="91"/>
<wire x1="314.96" y1="66.04" x2="297.18" y2="66.04" width="0.1524" layer="91"/>
<junction x="297.18" y="66.04"/>
<pinref part="LED23" gate="G$1" pin="C"/>
<junction x="289.56" y="66.04"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="OC23" gate="G$1" pin="A"/>
<wire x1="284.48" y1="71.12" x2="284.48" y2="73.66" width="0.1524" layer="91"/>
<pinref part="LED23" gate="G$1" pin="A"/>
<wire x1="284.48" y1="73.66" x2="289.56" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R41" gate="G" pin="1"/>
<wire x1="289.56" y1="73.66" x2="289.56" y2="76.2" width="0.1524" layer="91"/>
<junction x="289.56" y="73.66"/>
<pinref part="D2" gate="G$1" pin="K"/>
<wire x1="289.56" y1="76.2" x2="289.56" y2="81.28" width="0.1524" layer="91"/>
<wire x1="297.18" y1="76.2" x2="289.56" y2="76.2" width="0.1524" layer="91"/>
<junction x="289.56" y="76.2"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R41" gate="G" pin="2"/>
<wire x1="289.56" y1="101.6" x2="289.56" y2="91.44" width="0.1524" layer="91"/>
<pinref part="L6" gate="A" pin="1"/>
</segment>
</net>
<net name="/RST" class="0">
<segment>
<pinref part="OC23" gate="G$1" pin="COL"/>
<wire x1="266.7" y1="71.12" x2="256.54" y2="71.12" width="0.1524" layer="91"/>
<label x="256.54" y="71.12" size="1.4224" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="CON3" gate="G$1" pin="7"/>
<pinref part="L6" gate="A" pin="2"/>
<wire x1="347.98" y1="101.6" x2="299.72" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
